# Kvizosfera

Project developed by students at [Faculty of Electrical Engineering and Computing](https://www.fer.unizg.hr/en).

## About

- TODO

## Development information

### Frontend

To run frontend part of project, you will need to have **NPM** installed.

To check if NPM installed run: `npm -v` command which should output installed version of NPM.

Before you execute any of listed command you will **need to position** yourself into `Kvizosfera/IzvorniKod/frontend` folder!

Running app for the first time (or every time after some new package is installed) requires to install all packages locally. To install packages locally run: `npm install`. If you are not sure if any new package is installed, you can run this command every time, NPM is smart enough to not install already installed packages.

To start the app execute command: `npm start`. Do not close console after executing this command since it will stop the app.

You can check if the app is working by going to URL: _http://localhost:3000/_

**TLDR:**

```bash
# Kvizosfera/IzvorniKod/frontend
# installs required packages (required only first time)
npm install
# starts app
npm start
```

## Backend

To run backend part of app you will need **Docker-compose** installed. To check if _docker-compose_ is installed run: `docker-compose -v` and you should see version of _docker-compose_ as output.

Before you execute any of listed command you will **need to position** yourself into `Kvizosfera/IzvorniKod/api` folder!

Before you execute any of the following commands, you will need to create external volume for docker since our containers will be deleted (to save memory) every time we shut them down. When they are deleted, we do not want to lose any of the data that was stored inside database container so we will created something that is called _docker volume_ which basically means that docker will reserve some space on our computer for the created _volume_ and our database data will be put there. To create _docker volume_ execute: `docker volume create postgres_database`.

It's **important** to **create volume before** we go on and execute other commands because they wont work if the volume named _postgres_database_ doesn't exist.

When you are starting backend for the first time you will need to execute: `docker-compose build`. This command will build all necessary docker containers.

After docker containers are built, you can start them by executing: `docker-compose up -d`.

You can check if docker containers are started by typing: `docker-compose ps`. This command will list all active containers on your computer.

You can check if the app is working by going to URL: _http://localhost:8000/_

**TLDR:**

```bash
# Kvizosfera/IzvorniKod/api
# creates volume
docker volume create postgres_database
# build required containers (required only first time)
docker-compose build
# starts backend
docker-compose up -d
```
