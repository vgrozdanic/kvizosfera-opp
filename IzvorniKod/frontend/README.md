# Frontend

To run frontend part of project,  you will need to have **NPM** installed.

To check if NPM installed run: `npm -v` command which should output installed version of NPM.

Running app for the first time (or every time after some new package is installed) requires to install all packages locally. To install packages locally run: `npm install`. If you are not sure if any new package is installed, you can run this command every time, NPM is smart enough to not install already installed packages.

To start the app execute command: `npm start`. Do not close console after executing this command since it will stop the app. 

You can check if the app is working by going to URL: *http://localhost:3000/*

### TLDR:

```bash
# installs required packages
npm install
# starts app
npm start
```

