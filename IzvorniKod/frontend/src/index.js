
import React from 'react';
import ReactDOM from 'react-dom';
import { createBrowserHistory } from 'history';
import {
  Router,
  Route,
  Switch,
} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';
import Login from './layouts/Login';
import Home from './layouts/Home';
import Register from './layouts/Register';
import CreateQuizEvent from './layouts/CreateQuizEvent/CreateQuizEvent';
import MyProfile from './layouts/MyProfile/MyProfile';
import createLeague from './layouts/createLeague/createLeague';
import QuizApplicationPage from './layouts/QuizApplicationPage/QuizApplicationPage';
import Profile from './layouts/Profile/Profile';
import CreateOrganizer from './layouts/CreateOrganizer/CreateOrganizer';
import organizerMember from './layouts/organizerMember/organizerMember';
import EditUserData from './layouts/EditUserData/EditUserData';
import Explore from './layouts/Explore/Explore'
import Messages from './layouts/Messages/Messages';
import { requireAuthentication } from './components/auth/Auth';
import Quiz from './layouts/quiz/Quiz';
import EditOrganizer from './layouts/EditOrganizer/EditOrganizer';
import AddTrialQuizQuestion from './layouts/AddTrialQuizQuestion/AddTrialQuizQuestion';
import AddEventQuizQuestion from './layouts/AddEventQuizQuestion/AddEventQuizQuestion';
import SolveTrialQuiz from './layouts/SolveTrialQuiz/SolveTrialQuiz';
import SolveEventQuiz from './layouts/SolveEventQuiz/SolveEventQuiz';
import AddQuizResults from './layouts/AddQuizResults/AddQuizResults';
import QuizMakerPage from './layouts/quizMaker/QuizMakerPage';
import ChangeUserRole from './layouts/ChangeUserRole/ChangeUserRole';
import LeaguePage from './layouts/LeaguePage/LeaguePage';

const hist = createBrowserHistory();

ReactDOM.render(
  // eslint-disable-next-line react/jsx-filename-extension
  <Router history={hist}>
    <Switch>
      <Route exact path="/explore" component={requireAuthentication(Explore)} />
      <Route exact path="/organize" component={requireAuthentication(CreateQuizEvent)} />
      <Route exact path="/login" component={Login} />
      <Route exact path="/register" component={Register} />
      <Route exact path="/organizer/:quizmakerID" component={QuizMakerPage} />
      <Route exact path="/league/:leagueId" component={LeaguePage} />
      <Route exact path="/create-league" component={requireAuthentication(createLeague)} />
      <Route exact path="/application-page/:quizId" component={requireAuthentication(QuizApplicationPage)} />
      <Route exact path="/profile/:userId" component={requireAuthentication(Profile)} />
      <Route exact path="/profile" component={requireAuthentication(MyProfile)} />
      <Route exact path="/create-organizer" component={requireAuthentication(CreateOrganizer)} />
      <Route exact path="/edit-user-data" component={requireAuthentication(EditUserData)} />
      <Route exact path="/add-member" component={requireAuthentication(organizerMember)} />
      <Route exact path="/messages" component={requireAuthentication(Messages)} />
      <Route exact path="/quiz/:quiz_id" component={requireAuthentication(Quiz)} />
      <Route exact path="/edit-organizer/:organizerId" component={requireAuthentication(EditOrganizer)} />
      <Route exact path="/add-trial-quiz-question/:organizerId" component={requireAuthentication(AddTrialQuizQuestion)} />
      <Route exact path="/add-event-quiz-question/:quizId" component={requireAuthentication(AddEventQuizQuestion)} />
      <Route exact path="/add-quiz-results/:quizId" component={requireAuthentication(AddQuizResults)} />
      <Route exact path="/solve-trial-quiz/:organizerId" component={requireAuthentication(SolveTrialQuiz)} />
      <Route exact path="/solve-event-quiz/:quizId" component={requireAuthentication(SolveEventQuiz)} />
      <Route exact path="/change-user-role" component={requireAuthentication(ChangeUserRole)} />
      <Route exact path="/home" component={requireAuthentication(Home)} />
      <Route path="/" component={requireAuthentication(Home)} />
    </Switch>
  </Router>,
  document.getElementById('root'),
);
