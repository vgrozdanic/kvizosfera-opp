import React from 'react';
import { animateScroll } from "react-scroll";
import { Toast, ToastBody, ToastHeader } from 'reactstrap';
import Navbar from '../../components/navbar/Navbar';
import APIService from '../../services/APIService';
import './messages.css';

const apiService = new APIService();

const dateOptions = {
    weekday: "long",
    year: "numeric",
    month: "long",
    day: "numeric"
}

const timeOptions = {
    hour12 : false,
    hour:  "2-digit",
    minute: "2-digit",
}

class Messages extends React.Component {

    constructor(props) {
        super(props);
    }

    state = {
        error: false,
        messagesParticipants: [],
        messagesInCurrentConversation: [],
        currentConversationUserId: -1,
    }

    scrollToBottom() {
        animateScroll.scrollToBottom({
          containerId: "conversation"
        });
    }

    componentDidMount() {
        apiService.getMessageParticipants()
        .then((response) => {
            this.setState({
                currentConversationUserId: response[0].id,
                messagesParticipants: response,
            })

            apiService.getConversationMessages(response[0].id)
            .then((response) => {
                this.setState({
                    messagesInCurrentConversation: response,
                })
            }).catch((error) => this.setState({
                error: true
            }));
        }).catch((error) => {
            this.setState({
            error: true
        })});

        this.scrollToBottom();
    }

    componentDidUpdate() {
        this.scrollToBottom();
    }

    send = async (event) => {
        event.preventDefault();
        apiService.
          sendMessage(this.state.currentConversationUserId, document.getElementById("msg-area").value)
          .then(() => {
                document.getElementById("msg-area").value = "";
                this.updateConversation(this.state.currentConversationUserId);
                this.updateParticipants();
          })
          .catch((error) => {
          });
      }

    updateConversation = userID => {
        this.setState(state => {
            const currentConversationUserId = userID;
            apiService.getConversationMessages(userID)
                .then((response) => {
                    this.setState({
                        messagesInCurrentConversation: response,
                    })
                }).catch((error) => this.setState({
                    error: true
                }));

            return {
                currentConversationUserId: currentConversationUserId,
            };
        });

    };

    updateParticipants = () => {
        apiService.getMessageParticipants()
        .then((response) => {
            this.setState({
                messagesParticipants: response,
            })
        }).catch((error) => {
            this.setState({
            error: true
        })});
    }

    render() {
        return (
            <div>
                <Navbar />
                <div class="container">
                    <div class="row">
                        <div class="col-4">
                            <div className="p-3 bg-secondary my-2 rounded border border-info scrollerParticipants">
                                {this.state.messagesParticipants &&
                                    this.state.messagesParticipants.map((user, index) => (
                                    user.id === this.state.currentConversationUserId ?
                                    <div className="card mb-2 bg-info border border-dark">
                                        <div className="card-body">
                                        <h5 className="card-title font-weight-bold">
                                            {user.username}
                                        </h5>
                                        <h6 class="card-subtitle text-white mb-2">{(new Date(user.msg_date).toLocaleDateString("hr-HR", dateOptions))} </h6>
                                        <h6 class="card-subtitle mb-2">{(new Date(user.msg_date).toLocaleTimeString("hr-HR", timeOptions))}</h6>
                                        <a onClick={() => this.updateConversation(user.id)} class="btn border-dark btn-primary btn-block stretched-link text-light" role="button">Odaberi</a>
                                        </div>
                                    </div>  
                                    :
                                    <div className="card mb-2">
                                        <div className="card-body">
                                        <h5 className="card-title font-weight-bold">
                                            {user.username}
                                        </h5>
                                        <h6 class="card-subtitle text-muted mb-2">{(new Date(user.msg_date).toLocaleDateString("hr-HR", dateOptions))} </h6>
                                        <h6 class="card-subtitle mb-2">{(new Date(user.msg_date).toLocaleTimeString("hr-HR", timeOptions))}</h6>
                                        <a onClick={() => this.updateConversation(user.id)} class="btn btn-primary btn-block stretched-link text-light" role="button">Odaberi</a>
                                        </div>
                                    </div>  
                                ))}
                            </div>
                        </div>
                        <div class="col-8">
                            <div class="container">
                                <div className="p-3 bg-light my-2 rounded border border-dark scroller" id="conversation">
                                    {this.state.messagesInCurrentConversation &&
                                        this.state.messagesInCurrentConversation.map((message, index) => (
                                            message.sender.id === this.state.currentConversationUserId ?
                                            <div class="d-flex justify-content-start">
                                                <Toast>
                                                <ToastHeader>
                                                    <strong class="mr-auto text-info">{message.sender.username}</strong>
                                                    <br />
                                                    <small class="text-muted">{(new Date(message.msg_date).toLocaleDateString("hr-HR", dateOptions))} {(new Date(message.msg_date).toLocaleTimeString("hr-HR", timeOptions))}</small>
                                                </ToastHeader>
                                                <ToastBody>
                                                    {message.message}
                                                </ToastBody>
                                                </Toast>
                                            </div>
                                            :
                                            <div class="d-flex justify-content-end">
                                                <Toast>
                                                <ToastHeader>
                                                    <strong class="mr-auto text-info">{message.sender.username}</strong>
                                                    <br />
                                                    <small class="text-muted">{(new Date(message.msg_date).toLocaleDateString("hr-HR", dateOptions))} {(new Date(message.msg_date).toLocaleTimeString("hr-HR", timeOptions))}</small>
                                                </ToastHeader>
                                                <ToastBody>
                                                    {message.message}
                                                </ToastBody>
                                                </Toast>
                                            </div>
                                    ))}
                                </div>
                            </div>
                            <div class="container">
                                <div className="p-3 bg-light my-2 rounded border border-dark">
                                    <form onSubmit={this.send}>
                                        <div class="row">
                                            <div class="col-sm">
                                                <textarea rows="3" cols="60" name="msg" id="msg-area"></textarea>
                                            </div>
                                            <div class="col-sm">
                                                <button className="btn btn-info mt-3" type="submit">Pošalji</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}

export default Messages;