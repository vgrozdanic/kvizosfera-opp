import React from 'react';
import Navbar from '../../components/navbar/Navbar';
import APIService from '../../services/APIService';

const apiService = new APIService();

class AddQuizResults extends React.Component {
    state ={
        error: undefined,
        capacity: undefined,
        quizName: undefined,
        counter: undefined,
        points: undefined,
        name: "",
    }

    handleChange = (event) => {
        this.setState({ [event.target.name]: event.target.value });
    };

    componentDidMount() {
        apiService
        .getQuizResults(this.props.match.params.quizId)
        .then((response) => {
            this.setState({ counter: response.length + 1, name: "", points: ""});
        }).catch(error => this.setState({ error: true }));

        apiService
        .getQuizData(this.props.match.params.quizId)
        .then((response) => {
         this.setState({
         quizName: response.name,
         capacity: response.capacity,
        });
        }).catch((error) => this.setState({ error: true }));
    }

    submitTeamRanking = async (event) => {
        event.preventDefault();
        apiService
        .submitQuizResults(
            this.props.match.params.quizId,
            this.state.counter,
            this.state.points,
            this.state.name,
        )
        .then(() => {
            this.setState(prevState => {
                return { name: "", points: "", counter: prevState.counter + 1 }
            });
        })
        .catch(error => this.setState({ error: true }));
    };

    render() {
        return (
          <div>
            <Navbar />
            <div className="text-center title">
              <h1>Dodaj rezultate za <a href={'/quiz/' + this.props.match.params.quizId}>{this.state.quizName}</a></h1>
            </div>
            <div className="container col-xs-4">
                <form
                        className="text-center border p-5"
                        onSubmit={this.submitTeamRanking}
                >
                    <div className="card mb-2">
                        <div className="card-body">
                        <li class="list-group-item list-group-item-primary py-2 font-weight-bold">Ostvareno mjesto: {this.state.counter}.</li>
                            <div className="row">
                                <h6 className="ml-1 mt-2 py-2">
                                    Ime ekipe:
                                </h6>
                                <input className="bg-light ml-1 mr-1 form-control" value={this.state.name} rows="2" cols="250" name="name" id="name_id" onChange={this.handleChange}/>
                            </div>
                            <div className="row">
                                <h6 className="ml-1 mt-2 py-2">
                                    Ostvareni bodovi:
                                </h6>
                                <input className="bg-light ml-1 mr-1 form-control" value={this.state.points} rows="2" cols="250" name="points" id="points_id" onChange={this.handleChange} />
                            </div>
                        </div>
                    </div>
                    {this.state.counter > this.state.capacity ?
                        <button className="btn btn-info btn-block my-4" disabled>
                            Maksimalan broj ekipa dodan
                        </button>
                        :
                        <button className="btn btn-info btn-block my-4" type="submit" >
                                Unesi!
                        </button>
                    }
                    {this.state.error && (
                        <p className="text-danger">Nije uspješno dodano.</p>
                    )}
                    {this.state.loggedIn && <p className="text-success">Uspješno dodano!</p>}
                    </form>
                </div>
        </div>
        );
    }
    }
export default AddQuizResults;
