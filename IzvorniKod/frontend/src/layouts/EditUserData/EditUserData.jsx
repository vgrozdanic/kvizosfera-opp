import React from 'react';
import Navbar from '../../components/navbar/Navbar';
import APIService from '../../services/APIService';

const apiService = new APIService();

class EditUserData extends React.Component {
    state={
       first_name: undefined,
       last_name: undefined,
       location: undefined,
       user_name: undefined,
       email: undefined,
       error: undefined,
       success: undefined,
    };

    handleChange = (event) => this.setState({ [event.target.name]: event.target.value });

    submitUserData = async (event) => {
      event.preventDefault();
      apiService
        .sendChangedUserData(
          this.state.first_name,
          this.state.last_name,
          this.state.location,
          this.state.email,
        )
        .then(() => {
          this.setState({ success: true });
        })
        .catch(() => this.setState({ error: true }));
    };

    componentDidMount() {
      if (this.props.match.params) {
      apiService
        .getCurrentProfileData()
        .then((response) => { this.setState({ 
          first_name: response.first_name,
          last_name: response.last_name,
          location: response.location,
          user_name: response.user.username,
          email: response.user.email,
        })
      })
      .catch((error) => this.setState({ error: true }));
  }
  }

  deleteUser = () => {
    apiService
      .deleteUser()
      .then((response) => {
        this.props.history.push("/login");
      })
      .catch(error => this.setState({ error: true }));
  };

  render() {
    return (
      <div>
        <Navbar />
        <div className="text-center">
          <br />
          <br />
            <h1>Uredi osobne podatke!</h1>
        </div>
        <div className="container col-md-5">
        <form className="text-center border p-5" onSubmit={this.submitUserData}>
            <div className="row">
              Ime:
              <input
                defaultValue={this.state.first_name}
                type="text"
                id="name_id"
                name="first_name"
                className="form-control mb-4"
                onChange={this.handleChange}
              />
              </div>
              <div className="row">
              Prezime:
              <input
                defaultValue={this.state.last_name}
                type="text"
                id="lastname_id"
                name="last_name"
                className="form-control mb-4"
                onChange={this.handleChange}
              />
              </div>
              <div className="row">
              Lokacija:
              <input
                defaultValue={this.state.location}
                type="text"
                id="location_id"
                name="location"
                className="form-control mb-4"
                onChange={this.handleChange}
              />
              </div>
              <div className="row">
              Email:
              <input
                defaultValue={this.state.email}
                type="text"
                id="email_id"
                name="email"
                className="form-control mb-4"
                onChange={this.handleChange}
              />
              </div>
              <button className="btn btn-primary btn-block mt-2" type="submit">Spremi</button>
            {this.state.error && (
              <p className="text-danger">
                Došlo je do pogreške prilikom pokušaja izmjene podataka
              </p>
            )}
            {this.state.success && (
              <p className="text-success">Podatci uspješno ažurirani!</p>
            )}
            </form>
            <button className="btn btn-danger btn-block mt-2 mb-2" onClick={this.deleteUser}>Obriši profil</button>
        </div>
      </div>
    );
  }
}

export default EditUserData;
