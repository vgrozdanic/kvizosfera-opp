import React from 'react';
import ProfilePicture from '../../assets/profile_picture.png';
import { faComments, faCalendar, faClock } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './myProfile.css';
import Navbar from '../../components/navbar/Navbar';
import APIService from '../../services/APIService';
import Following from '../../components/following/Following';
import Followers from '../../components/followers/Followers';

const apiService = new APIService();

const dateOptions = {
  weekday: "long",
  year: "numeric",
  month: "long",
  day: "numeric"
}

const timeOptions = {
  hour12 : false,
  hour:  "2-digit",
  minute: "2-digit",
}

class MyProfile extends React.Component {
  state = {
    error: false,
    mail: undefined,
    id: undefined,
    username: undefined,
    firstName: undefined,
    lastName: undefined,
    age: undefined,
    picture: undefined,
    location: undefined,
    upcomingQuizzes: [],
    bygoneQuizzes: [],
    followingUsers: [],
    followers: [],
    organizers: [],
  }

  componentDidMount() {
    apiService.getCurrentProfileData()
    .then((response) => {
      let calcAge = this.calculateAge(response.dob)
      this.setState({
        id: response.user.id,
        mail: response.user.email,
        username: response.user.username,
        firstName: response.first_name,
        lastName: response.last_name,
        age: calcAge,
        picture: response.picture,
        location: response.location,
      })

      apiService.getUserFollowers(response.user.id)
      .then((response) => {
        this.setState({
          followers: response
        })
      }).catch((error) => this.setState({ error: true }));

      apiService.getUserFollowing(response.user.id)
      .then((response) => {
        this.setState({
          followingUsers: response
        })
      }).catch((error) => this.setState({ error: true }));
    }).catch((error) => this.setState({
        error: true
    }));

    apiService.getCurrentUserUpcomingQuizzes()
      .then((response) => {
        this.setState({
          upcomingQuizzes: response
        })
    }).catch((error) => this.setState({ error: true }));

    apiService.getCurrentUserBygoneQuizzes()
      .then((response) => {
        this.setState(state => {
          const lastFiveBygoneQuizzes = response.filter((quiz, index) => index >= response.length - 5).reverse();
          return {
            bygoneQuizzes: lastFiveBygoneQuizzes,
          };
        });
    }).catch((error) => this.setState({ error: true }));

    apiService.getUserOrganizers()
    .then((response) => {
      this.setState({
        organizers: response
      })
  }).catch((error) => this.setState({ error: true }));
  }

  sendMessage() {
    // potrebno napisati funkciju koja omogućava slanje poruke ovom korisniku
  }

  calculateAge(birthdate) {
    const today = new Date();
    const birthDate = new Date(birthdate);
    let age = today.getFullYear() - birthDate.getFullYear();
    const m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
    return age;
  }

  render() {
    const isError = this.state.error;
    return (
      <div>
        <Navbar />
        {isError
          ? (
            <div>
              <h1 className="text-center">Došlo je do problema!</h1>
              <h2 className="text-center">Jedan od mogućih uzroka je da traženi korisnik ne postoji.</h2>
            </div>
          )
          : (
            <div className="container">
              <div className="row mt-3 mb-3">
                <span>
                <div className="col pl-2 pr-2">
                  <div style={{ display: 'inline-block' }}>
                    <img className="profilePic p-2" src={ProfilePicture} alt="ProfilePicture" />
                  </div>
                  <div style={{ display: 'inline-block' }} className="font-weight-bold p-2">{this.state.username}</div>
                  <div style={{ display: 'inline-block' }} className="ml-3">
                    <a href={'/edit-user-data'} class="btn btn-primary active" role="button">Uredi podatke</a>
                    <Following following={this.state.followingUsers} myProfile={true}/>
                    <Followers sex={this.state.sex} followers={this.state.followers} myProfile={true}/>
                  </div>
                  </div>
                </span>
              </div>
              <div className="row">
                <div className="col-3 pl-2 pr-2">
                  <div className="card">
                    <div className="card-body">
                      <h5 className="card-title">Info</h5>
                      <p className="card-text">E-mail: {this.state.mail}</p>
                      <p className="card-text">Ime i prezime: {this.state.firstName} {this.state.lastName}</p>
                      <p className="card-text">Dob: {this.state.age}</p>
                      <p className="card-text">Lokacija: {this.state.location}</p>
                      <h5 className="card-title">Povezani organizatori</h5>
                      {this.state.organizers &&
                        this.state.organizers.map((organizer, index) => (
                          <div><a href={"organizer/" + organizer.id}>{organizer.name}<br /></a></div>
                        ))}
                    </div>
                  </div>
                </div>
                <div className="col pl-2 pr-2">
                  <div className="card">
                    <div className="card-body">
                      <h5 className="card-title">Idući kvizovi</h5>
                      {this.state.upcomingQuizzes &&
                        this.state.upcomingQuizzes.sort(function(a, b) {
                          let dateA = a.quiz_event.event_date;
                          let dateB = b.quiz_event.event_date;
                          return dateA > dateB ? 1 : dateA < dateB ? -1 : 0;
                        }).map((quizEventEntry, index) => (
                          <div className="card mb-2">
                            <div className="card-body">
                              <h5 className="card-title">
                                <a href={'/quiz/' + quizEventEntry.quiz_event.id}> {quizEventEntry.quiz_event.name}</a>
                              </h5>
                              <h6 class="card-subtitle mb-2">
                                {quizEventEntry.quiz_event.league ?
                                  (<a href={'/league/' + quizEventEntry.quiz_event.league.id}> {quizEventEntry.quiz_event.league.name}</a>) :
                                  "Ne postoji liga"
                                }
                              </h6>
                              <h7 class="card-subtitle text-muted mb-2"><FontAwesomeIcon icon={faCalendar} className="mr-2" />{(new Date(quizEventEntry.quiz_event.event_date).toLocaleDateString("hr-HR", dateOptions))}</h7>
                              <br />
                              <h7 class="card-subtitle text-muted mb-2"><FontAwesomeIcon icon={faClock} className="mr-2" />{(new Date(quizEventEntry.quiz_event.event_date).toLocaleTimeString("hr-HR", timeOptions))}</h7>
                            </div>
                          </div>
                        ))}
                    </div>
                  </div>
                </div>
                <div className="col pl-2 pr-2">
                  <div className="card">
                    <div className="card-body">
                      <h5 className="card-title">Prošli kvizovi</h5>
                      {this.state.bygoneQuizzes &&
                        this.state.bygoneQuizzes.sort(function(a, b) {
                          let dateA = a.quiz_event.event_date;
                          let dateB = b.quiz_event.event_date;
                          return dateA > dateB ? -1 : dateA < dateB ? 1 : 0;
                        }).map((quizEventEntry, index) => (
                          <div className="card mb-2">
                            <div className="card-body">
                              <h5 className="card-title">
                                <a href={'/quiz/' + quizEventEntry.quiz_event.id}> {quizEventEntry.quiz_event.name}</a>
                              </h5>
                              <h6 class="card-subtitle mb-2">
                                {quizEventEntry.quiz_event.league ?
                                  (<a href={'/league/' + quizEventEntry.quiz_event.league.id}> {quizEventEntry.quiz_event.league.name}</a>) :
                                  "Ne postoji liga"
                                }
                              </h6>
                              <h7 class="card-subtitle text-muted mb-2"><FontAwesomeIcon icon={faCalendar} className="mr-2" />{(new Date(quizEventEntry.quiz_event.event_date).toLocaleDateString("hr-HR", dateOptions))}</h7>
                              <br />
                              <h7 class="card-subtitle text-muted mb-2"><FontAwesomeIcon icon={faClock} className="mr-2" />{(new Date(quizEventEntry.quiz_event.event_date).toLocaleTimeString("hr-HR", timeOptions))}</h7>
                            </div>
                          </div>
                        ))}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )}
      </div>
    );
  }
}

export default MyProfile;
