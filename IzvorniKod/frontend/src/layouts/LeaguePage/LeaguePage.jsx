import React from 'react';
import Navbar from '../../components/navbar/Navbar';
import APIService from '../../services/APIService';
import { faCalendar, faClock } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const apiService = new APIService();

const dateOptions = {
  weekday: "long",
  year: "numeric",
  month: "long",
  day: "numeric"
}

const timeOptions = {
  hour12 : false,
  hour:  "2-digit",
  minute: "2-digit",
}

class LeaguePage extends React.Component {
  state = {
    organizerID: undefined,
    organizerName: undefined,
    name: undefined,
    rules: undefined,
    concept: undefined,
    min_num_of_players: undefined,
    max_num_of_players: undefined,
    fee: undefined,
    location: undefined,
    creationDate: undefined,
    error: false,
    success: false,
    leagueUpcomingQuizzes: [],
    leagueBygoneQuizzes: [],
  }

  componentDidMount() {
      apiService.getLeagueData(this.props.match.params.leagueId)
      .then((response) => {
        this.setState({ 
            organizerID: response.organizer,
            name: response.name,
            rules: response.rules,
            concept: response.concept,
            min_num_of_players: response.min_num_of_players,
            max_num_of_players: response.max_num_of_players,
            fee: response.fee,
            location: response.location,
            creationDate: response.creationDate,
         });

         apiService.getOrganizerInfo(response.organizer)
         .then((response) => {
            this.setState({
              organizerName: response.name,
            });
         })
         .catch(error => this.setState({ error: true }));
      })
      .catch(error => this.setState({ error: true }));

      apiService
      .getQuizEvents()
      .then(response => {
        response.forEach(
          quizEvent => {
            let prevUpcomingQuizzes = this.state.leagueUpcomingQuizzes;
            if (quizEvent.league && quizEvent.league.id === parseInt(this.props.match.params.leagueId)) {
              this.setState({
                leagueUpcomingQuizzes: [...prevUpcomingQuizzes, quizEvent],
              })
            }
          }
        )
      })
      .catch(error => this.setState({ error: true }));

      apiService
      .getBygoneQuizEvents()
      .then(response => {
        response.forEach(
          quizEvent => {
            let prevBygoneQuizzes = this.state.leagueBygoneQuizzes;
            if (quizEvent.league && quizEvent.league.id === parseInt(this.props.match.params.leagueId)) {
              this.setState({
                leagueBygoneQuizzes: [...prevBygoneQuizzes, quizEvent],
              })
            }
          }
        )
      })
      .catch(error => this.setState({ error: true }));
  }

render() {
    return (
      <div>
        <Navbar />
            <div className="row ligaNaslov">{this.state.name}</div>
            <div className="container okvirHome">
              <div className="row">
                <div className="col-3 pl-2 pr-2">
                  <div className="card">
                    <div className="card-body">
                      <h5 className="card-title">Informacije</h5>
                      <p className="card-text">Naziv lige: <br/> {this.state.name}</p>
                      <p className="card-text">Organizator:<br/><a href={'/organizer/' + this.state.organizerID} className="card-text">{this.state.organizerName}</a></p>
                      <p className="card-text">Kotizacija:<br/> {this.state.fee} kn </p>
                      <p className="card-text">Minimalan broj igrača u ekipi: <br/>{this.state.min_num_of_players}</p>
                      <p className="card-text">Maksimalan broj igrača u ekipi: <br/>{this.state.max_num_of_players}</p>
                      <p className="card-text">Lokacija: <br/>{this.state.location}</p>
                      <p className="card-text">Pravila: <br/>{this.state.rules}</p>
                      <p className="card-text">Koncept: <br/>{this.state.concept}</p>
                    </div>
                  </div>
                </div>
                <div className="col pl-2 pr-2">
                  <div className="card">
                    <div className="card-body">
                      <h5 className="card-title">Idući kvizovi</h5>
                      {this.state.leagueUpcomingQuizzes &&
                        this.state.leagueUpcomingQuizzes.sort(function(a, b) {
                          let dateA = a.event_date;
                          let dateB = b.event_date;
                          return dateA > dateB ? 1 : dateA < dateB ? -1 : 0;
                        }).map((quizEventEntry, index) => (
                          <div className="card mb-2">
                            <div className="card-body">
                              <h5 className="card-title">
                                <a href={'/quiz/' + quizEventEntry.id}> {quizEventEntry.name}</a>
                              </h5>
                              <h6 class="card-subtitle mb-2">
                                {quizEventEntry.league ?
                                  (<a href={'/league/' + quizEventEntry.league.id}> {quizEventEntry.league.name}</a>) :
                                  "Ne postoji liga"
                                }
                              </h6>
                              <h7 class="card-subtitle text-muted mb-2"><FontAwesomeIcon icon={faCalendar} className="mr-2" />{(new Date(quizEventEntry.event_date).toLocaleDateString("hr-HR", dateOptions))}</h7>
                              <br />
                              <h7 class="card-subtitle text-muted mb-2"><FontAwesomeIcon icon={faClock} className="mr-2" />{(new Date(quizEventEntry.event_date).toLocaleTimeString("hr-HR", timeOptions))}</h7>
                            </div>
                          </div>
                        ))}
                    </div>
                  </div>
                </div>
                <div className="col pl-2 pr-2">
                <div className="card">
                    <div className="card-body">
                      <h5 className="card-title">Prošli kvizovi</h5>
                      {this.state.leagueBygoneQuizzes &&
                        this.state.leagueBygoneQuizzes.sort(function(a, b) {
                          let dateA = a.event_date;
                          let dateB = b.event_date;
                          return dateA > dateB ? -1 : dateA < dateB ? 1 : 0;
                        }).map((quizEventEntry, index) => (
                          <div className="card mb-2">
                            <div className="card-body">
                              <h5 className="card-title">
                                <a href={'/quiz/' + quizEventEntry.id}> {quizEventEntry.name}</a>
                              </h5>
                              <h6 class="card-subtitle mb-2">
                                {quizEventEntry.league ?
                                  (<a href={'/league/' + quizEventEntry.league.id}> {quizEventEntry.league.name}</a>) :
                                  "Ne postoji liga"
                                }
                              </h6>
                              <h7 class="card-subtitle text-muted mb-2"><FontAwesomeIcon icon={faCalendar} className="mr-2" />{(new Date(quizEventEntry.event_date).toLocaleDateString("hr-HR", dateOptions))}</h7>
                              <br />
                              <h7 class="card-subtitle text-muted mb-2"><FontAwesomeIcon icon={faClock} className="mr-2" />{(new Date(quizEventEntry.event_date).toLocaleTimeString("hr-HR", timeOptions))}</h7>
                            </div>
                          </div>
                        ))}
                    </div>
                  </div>
                </div>
              </div>
            </div>
      </div>
    );
  }
}

export default LeaguePage;