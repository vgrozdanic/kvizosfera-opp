import React from 'react';
import Navbar from '../../components/navbar/Navbar';
import APIService from '../../services/APIService';

const apiService = new APIService();

class OrganizerMember extends React.Component {
  state = {
    error: false,
    user: undefined,
    users: undefined,
    organizer: undefined,
    organizerList: undefined,
    success: false,
  };

  handleChange = (event) => this.setState({ [event.target.name]: event.target.value });

  submitForm = async (event) => {
    event.preventDefault();
    this.setState({ error: false, success: false });
    apiService
      .addMemberToOrganizer(this.state.user, this.state.organizer)
      .then(() => {
        this.setState({ success: true });
      })
      .catch((error) => this.setState({ error: true }));
  };

  componentDidMount() {
    apiService
      .getUserOrganizers()
      .then((response) => {
        // we set array we got in response to organizerList, but also we need
        // to initialize organizer value (since function handleChange will only be
        // called when we change value of select element) to cover the case when
        // user doesn't change value in select element
        this.setState({ organizerList: response, organizer: response[0].id });
      })
      .catch((error) => this.setState({ error: true }));

    apiService
      .getAllUsers()
      .then((response) => {
        this.setState({ users: response });

        apiService
          .getCurrentProfileData()
          .then((response) => {
            this.filter(response.user.id);
          })
          .catch((error) => this.setState({ error: true }));

      })
      .catch((error) => this.setState({ error: true }));
    
  }
  
  filter = userId => {
    this.setState(state => {
        const filteredUsers = state.users.filter((user, index) => userId !== user.user.id);

        return {
            users: filteredUsers,
        };
    });
  };

  render() {
    return (
      <div>
        <Navbar />
        <div className="container col-sm-4">
          <form
            className="text-center border border-light p-5"
            onSubmit={this.submitForm}
          >
            <p className="h4 mb-4">Dodaj korisnika kao člana organizatora</p>

            <div className="row">
              <select required className="form-control mb-4 marginaTop" id="user" name="user" onChange={this.handleChange}>
                <option disabled selected hidden>Odaberi korisnika</option>
                {this.state.users
                  && this.state.users.map((user, index) => (
                  <option value={user.user.username}>{user.user.username}</option>
                ))}
              </select>
            </div>
            <div className="row">
              <select required className="form-control mb-4 marginaTop" name="organizer" id="organizer" onChange={this.handleChange}>
                <option disabled selected hidden>Odaberi organizatora</option>
                {this.state.organizerList
                  && this.state.organizerList.map((organizer, index) => (
                    <option value={organizer.id}>{organizer.name}</option>
                  ))}
              </select>
            </div>

            {/* Add user button */}
            <button className="btn btn-info btn-block my-4" type="submit">
              Dodaj korisnika
            </button>

            {this.state.error && (
              <p className="text-danger">
                Došlo je do pogreške prilikom pokušaja dodavanja korisnika
              </p>
            )}
            {this.state.success && (
              <p className="text-success">Korisnik uspješno dodan!</p>
            )}
          </form>
        </div>
      </div>
    );
  }
}

export default OrganizerMember;
