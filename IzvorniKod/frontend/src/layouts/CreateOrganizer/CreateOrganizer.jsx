import React from 'react';
import Navbar from '../../components/navbar/Navbar';
import './createOrganizer.css';
import APIService from '../../services/APIService';

const apiService = new APIService();

class CreateOrganizer extends React.Component {
    state = {
        error: false,
        organizerName: undefined,
        rules: undefined,
        picture: undefined,
        location: undefined,
        created: false,
    };

    handleChange = (event) => this.setState({ [event.target.name]: event.target.value });

    submitInfo = async (event) => {
        event.preventDefault();
        this.setState({ error: false, created: false});
        apiService.createOrganizer(this.state.organizerName, document.getElementById("text-area").value, this.state.location)
        .then(() => {
            this.setState({ created: true });
            alert("Uspješno ste dodali organizatora!");
            this.props.history.push("/home");
        })
        .catch((error) => {
          this.setState({ error: true }
        )});
    }

    render() {
        return (
          <div>
          <Navbar />
          <div className="text-center mt-2">
              <h1>Kreiraj novog organizatora!</h1>
          </div>
          <div className="container col-xs-4">
              <form
                    className="text-center border p-5"
                    onSubmit={this.submitInfo}
                >
                  {
                  this.state.error ?
                    (<div class="alert alert-danger" role="alert">
                        Došlo je do pogreške. Molimo provjerite sva polja.
                      </div>) : (<div />)
                  }
                  <div className="row">
                    Ime organizatora:
                      <input
                            type="text"
                            id="defaultCreateOrganizerFormName"
                            name="organizerName"
                            className="form-control mb-4"
                            onChange={this.handleChange}
                        />
                    </div>

                  <div className="row">
                    Grad:
                      <input
                            type="text"
                            id="defaultCreateOrganizerFormCity"
                            name="location"
                            className="form-control mb-4"
                            onChange={this.handleChange}
                        />
                    </div>

                  <div className="row">
                    Pravila na Vašim kvizovima:
                      <textarea rows="10" cols="250" name="rules" id="text-area">

                      </textarea>
                  </div>

                  <button className="btn btn-info btn-block my-4" type="submit">
                        Kreiraj organizatora
                  </button>
                </form>
            </div>
        </div>
        );
    }
}

export default CreateOrganizer;
