import React from 'react';
import Navbar from '../components/navbar/Navbar';
import { faCalendar, faClock, faUsers } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import APIService from '../services/APIService';
import Octicon, {
    Search,
    Repo,
    Pin,
    ListOrdered,
    Unverified
} from "@primer/octicons-react";

const apiService = new APIService();

const dateOptions = {
  weekday: "long",
  year: "numeric",
  month: "long",
  day: "numeric"
}

const timeOptions = {
  hour12 : false,
  hour:  "2-digit",
  minute: "2-digit",
}

class Home extends React.Component {

  state = {
    error: false,
    upcomingQuizzes: [],
    upcomingQuizzesNumber: undefined
  }

  componentDidMount() {
    if (window.location.href.substr(-4) !== 'home' && window.localStorage.getItem('token') !== null) {
      window.location = '/home';
    }

    apiService.getCurrentUserUpcomingQuizzes()
      .then((response) => {
        this.setState({
          upcomingQuizzes: response,
          upcomingQuizzesNumber: response.length
        })
    }).catch((error) => this.setState({ error: true }));
  }

  render() {
    return (
      <div>
        <Navbar />
        <div className="container okvirHome">
         <div className="card aktivnost float-right">
                    <div className="card-body">
                      <h5 className="card-title naslovNadolazecih rounded">Nadolazeći kvizovi:</h5>
                      {this.state.upcomingQuizzesNumber ? this.state.upcomingQuizzes &&
                        this.state.upcomingQuizzes.sort(function(a, b) {
                          let dateA = a.quiz_event.event_date;
                          let dateB = b.quiz_event.event_date;
                          return dateA > dateB ? 1 : dateA < dateB ? -1 : 0;
                        }).map((quizEventEntry, index) => (
                          <div className="card mb-2">
                            <div className="card-body">
                              <h5 className="card-title">
                                <a href={'/quiz/' + quizEventEntry.quiz_event.id}> {quizEventEntry.quiz_event.name}</a>
                              </h5>
                              <h6 className="card-subtitle mb-2">
                                {quizEventEntry.quiz_event.league ?
                                  (<a href={'/league/' + quizEventEntry.quiz_event.league.id}> {quizEventEntry.quiz_event.league.name}</a>) :
                                  "Ne postoji liga"
                                }
                              </h6>
                              <h7 className="card-subtitle text-muted mb-2"><FontAwesomeIcon icon={faCalendar} className="mr-2" />{(new Date(quizEventEntry.quiz_event.event_date).toLocaleDateString("hr-HR", dateOptions))}</h7>
                              <br />
                              <h7 className="card-subtitle text-muted mb-2"><FontAwesomeIcon icon={faClock} className="mr-2" />{(new Date(quizEventEntry.quiz_event.event_date).toLocaleTimeString("hr-HR", timeOptions))}</h7>
                            </div>
                          </div>
                        )): <div>
                              <p className="text-white font-weight-bold">Niste prijavljeni ni na jedan kviz. Kvizove možete pronaći na stranici "Pronađi kviz"
                                                                          te se u par koraka prijaviti na odabrani.</p>
                              <button className="btn btn-outline naslovNadolazecih font-weight-bold" onClick={() => {this.props.history.push("/explore")}}>
                                Pronađi kviz
                              </button>
                            </div>}
                  </div>
          </div>
          <div className="float-left gumbiHome">
          <a type="button" class="btn btn-primary btn-lg gumb" href="/create-league">
            <div className="gumbTekst">Napravi <br /> ligu</div>
          <Octicon size="extra-large" icon={ListOrdered} />
          </a>
          <a type="button" class="btn btn-primary btn-lg gumb" href="/organize">
            <div className="gumbTekst">Organiziraj<br /> kviz</div>
          <Octicon size="extra-large" icon={Unverified} />
          </a>
          <a type="button" class="btn btn-primary btn-lg gumb" href="/create-organizer">
            <div className="gumbTekst">Registriraj organizatora</div>
            <div className="mt-5">
              <FontAwesomeIcon className="mt-4 fa-6x" icon={faUsers}/>
            </div>
          {/* <Octicon size="extra-large" icon={Pin} /> */}
          </a>
          </div>
        </div>
      </div>
    );
  }
}

export default Home;
