import React from 'react';
import Navbar from '../../components/navbar/Navbar';
import APIService from '../../services/APIService';
import QuizResult from '../../components/quizResult/QuizResult';

const apiService = new APIService();

const insert = (array, index, newItem) => [
    ...array.slice(0, index),
    newItem,
    ...array.slice(index + 1)
]

class SolveTrialQuiz extends React.Component {

    state = {
        questions: [],
        answers: [],
    }

    componentDidMount() {
        apiService
        .getTrialQuizQuestions(this.props.match.params.organizerId)
        .then((response) => {
            this.setState({ questions: response, answers: new Array(response.length).fill("") });
        }).catch(error => this.setState({ error: true }));
    }

    handleChange = (event) => {
        this.setState({ answers: insert(this.state.answers, [event.target.name] - 1, event.target.value) });
    };

    calculateScore = () => {
        let currentScore = 0;
        this.state.questions.map((question, index) => {
            if (this.state.answers[index] !== undefined && question.answer.toUpperCase() === this.state.answers[index].toUpperCase()) {
                currentScore++;
            }
        })
        return currentScore;
    }

    render() {
        return (
        <div>
            <Navbar />
            {this.state.questions.length === 0 ?
                <div>
                    Za ovog organizatora ne postoji probni kviz!
                </div>
                :
                <div>
                    <div className="text-center mb-2 mt-2">
                        <h1>Riješi probni kviz</h1>
                    </div>
                    <div className="container col-xs-4">
                        <div className="card mb-2">
                            {this.state.questions &&
                                this.state.questions.map((question, index) => (
                                    <div className="card-body">
                                        <li class="list-group-item text-center text-info list-group-item-light py-2 font-weight-bold">
                                            <h5 className="text-dark mt-2 py-2">
                                                {question.question_number}. {question.question}
                                            </h5>
                                            <input
                                                    type="text"
                                                    id={'answer' + question.question_number}
                                                    name={question.question_number}
                                                    value={this.state.answer}
                                                    className="text-light text-center form-control mb-4 bg-info ml-2 mr-2 form-control"
                                                    onChange={this.handleChange}
                                            />
                                        </li>
                                    </div>
                            ))}
                            <QuizResult score={this.calculateScore()} questions={this.state.questions.length}/>
                        </div>
                    </div>
                </div>
            }
        </div>
        );
    }
}

export default SolveTrialQuiz;
