
import React from 'react';
import Navbar from '../../components/navbar/Navbar';
import APIService from '../../services/APIService';

const apiService = new APIService();

class createLeague extends React.Component {
  state = {
    organizer: undefined,
    name: undefined,
    rules: undefined,
    concept: undefined,
    min_num_of_players: undefined,
    max_num_of_players: undefined,
    fee: undefined,
    location: undefined,
    organizerList: undefined,
    error: undefined,
    success: undefined,
    invalidNumber: undefined,
    invalidFeeType: undefined,
    invalidNumOfPlayersType: undefined,
  };

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
      invalidNumber: undefined,
      invalidFeeType: undefined,
      invalidNumOfPlayersType: undefined,
    });
  };

  submitCreateLeague = async (event) => {
    event.preventDefault();
    let isError = false;
    if (parseInt(this.state.max_num_of_players) < parseInt(this.state.min_num_of_players)) {
      this.setState( { invalidNumber: true } );
      isError=true;
    }
    if (isNaN(this.state.min_num_of_players) || isNaN(this.state.max_num_of_players)) {
      this.setState({ invalidNumOfPlayersType: true });
      isError=true;
    }
    if (isNaN(this.state.fee)) {
      this.setState({ invalidFeeType: true });
      isError=true;
    }
    if(isError) return;
    apiService
      .createLeague(
        this.state.organizer,
        this.state.name,
        this.state.rules,
        this.state.concept,
        this.state.min_num_of_players,
        this.state.max_num_of_players,
        this.state.fee,
        this.state.location,
      )
      .then(() => {
        this.setState({ success: true });
        alert('Liga uspješno kreirana');
        this.props.history.push('/home');
      })
      .catch(() => {
        this.setState({ error: true });
      })
  };

  componentDidMount() {
    apiService
      .getUserOrganizers()
      .then((response) => {
        // we set array we got in response to organizerList, but also we need
        // to initialize organizer value (since function handleChange will only be
        // called when we change value of select element) to cover the case when
        // user doesn't change value in select element
        this.setState({ organizerList: response, organizer: response[0].id });
      })
      .catch((error) => this.setState({ error: true }));
  }

  render() {
    return (
      <div>
        <Navbar />
        <p className="h3 my-4 text-center">Kreiraj ligu</p>
        <div className="container col-md-5">
          <form
            className="text-center border border-light p-5"
            onSubmit={this.submitCreateLeague}>
            <div className="row">
              Odaberi organizatora
              <div className="col">
                <select defaultValue="DEFAULT" className="form-control mb-4" name="organizer" onChange={this.handleChange}>
                  {this.state.organizerList
                  && this.state.organizerList.map((organizer, index) => (
                    <option value={organizer.id}>{organizer.name}</option>
                  ))}
                  <option value="DEFAULT" hidden>Odaberi...</option>
                </select>
              </div>
            </div>

            <div className="row">
              Naziv lige
              <input
                type="text"
                id="defaultLeagueName"
                name="name"
                className="form-control mb-4"
                onChange={this.handleChange}
              />
            </div>

            <div className="row">
              Pravila
              <textarea className="form-control mb-4" rows="3" cols="250" name="rules" id="text-area" onChange={this.handleChange} />
            </div>

            <div className="row">
              Koncept
              <textarea className="form-control mb-4" rows="3" cols="250" name="concept" id="text-area" onChange={this.handleChange} />
            </div>

            <div className="row mb-1">
              Broj igrača
            </div>

            <div className="row">
              Od
              <div className="col">
                <input
                  type="text"
                  id="defaultMinNumOfPlayers"
                  name="min_num_of_players"
                  className="form-control mb-4"
                  style={
                  (this.state.invalidNumber || this.state.invalidNumOfPlayersType) && { borderColor: 'red' }
                  }
                  onChange={this.handleChange}
                />
              </div>
              Do
              <div className="col">
                <input
                  type="text"
                  id="defaultMaxNumOfPlayers"
                  name="max_num_of_players"
                  className="form-control mb-4"
                  style={
                  (this.state.invalidNumber || this.state.invalidNumOfPlayersType) && { borderColor: 'red' }
                  }
                  onChange={this.handleChange}
                />
              </div>
            </div>

            {this.state.invalidNumber && (
              <p className="text-danger">Maksimalan broj igrača je manji od minimalnog!</p>
            )}

            {this.state.invalidNumOfPlayersType && (
              <p className="text-danger">Pogrešan unos.</p>
            )}

            <div className="row">
             Iznos kotizacije(u kunama)
              <input
                type="text"
                id="defaultFee"
                name="fee"
                className="form-control mb-4"
                style={
                  this.state.invalidFeeType && { borderColor: 'red' }
                }
                onChange={this.handleChange}
              />
            </div>

            {this.state.invalidFeeType  && (
              <p className="text-danger">Kotizacija treba biti cjelobrojan iznos.</p>
            )}

            <div className="row">
              Lokacija
              <input
                type="text"
                id="defaultLocation"
                name="location"
                className="form-control mb-4"
                onChange={this.handleChange}
              />
            </div>

            <button className="btn btn-info btn-block my-4" type="submit">
              Potvrdi
            </button>

            { this.state.error && (<p className="text-danger">Pogreška pri kreiranju lige.Popunite ispravno sva polja.</p>)}
            {this.state.success && (
            <p className="text-success text-center">Liga je uspješno kreirana!</p>
            )}
          </form>
        </div>
      </div>
    );
  }
}

export default createLeague;
