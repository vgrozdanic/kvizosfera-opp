import React from 'react';
import Navbar from '../../components/navbar/Navbar';
import APIService from '../../services/APIService';        
import './addTrialQuizQuestion.css';                                                                                             

const apiService = new APIService();

class AddTrialQuizQuestion extends React.Component {  

    state = {
        error: false,
        answer: "",
        counter: undefined,
        organizerName: undefined,
        organizers: [],
        organizerIds: [],
        amIOrganizer: false,
    }
    
    componentDidMount() {

        apiService.getUserOrganizers()
        .then((response) => {
          this.setState({
            organizers: response
          })
  
          this.getOrganizerIds();
          this.checkIfIAmOrganizer();
        })
        .catch(error => this.setState({ error: true }));

        apiService
        .getTrialQuizQuestions(this.props.match.params.organizerId)
        .then((response) => {
            this.setState({ counter: response.length + 1, answer: "" });
        }).catch(error => this.setState({ error: true }));

        apiService
        .getOrganizerInfo(this.props.match.params.organizerId)
        .then((response) => {
            this.setState({ organizerName: response.name });
        }).catch(error => this.setState({ error: true }));

    }

    handleChange = (event) => {
        this.setState({ [event.target.name]: event.target.value });
    };

    submit = async (event) => {
        event.preventDefault();
        apiService
        .submitTrialQuizQuestion(this.props.match.params.organizerId, this.state.counter, document.getElementById("question").value, this.state.answer)
        .then(() => {
            this.setState(prevState => { 
                return { answer: "", counter: prevState.counter + 1 }
            });
            document.getElementById("question").value = "";
        })
        .catch(error => this.setState({ error: true }));
    };

    getOrganizerIds = () => {
        this.setState(state => {
          const organizerIds = [];
          state.organizers.map((organizer, index) => {
            organizerIds.push(organizer.id);
          })
    
          return {
            organizerIds: organizerIds
          };
        })
      };

    checkIfIAmOrganizer = () => {
        if (this.state.organizerIds.includes(parseInt(this.props.match.params.organizerId))) {
          this.setState({ amIOrganizer: true });
        }
    };

    render() {
        return (
        <div>
            <Navbar />
            {!this.state.amIOrganizer ?
                <>
                    Nemate pravo dodavati pitanja za ovog organizatora.
                </>
                :
                <>
                    <div className="text-center title">
                        <h1>Dodaj pitanje za probni kviz za <a href={`/organizer/${this.props.match.params.organizerId}`}>{this.state.organizerName}</a>!</h1>
                    </div>
                    <div className="container col-xs-4">
                        <form 
                                className="text-center border p-5" 
                                onSubmit={this.submit}
                        >
                            <div className="card mb-2">
                                <div className="card-body">
                                <li class="list-group-item list-group-item-primary py-2 font-weight-bold">Redni broj pitanja: {this.state.counter}.</li>
                                    <div className="row">
                                        <h6 className="ml-1 mt-2 py-2">
                                            Tekst pitanja
                                        </h6>
                                        <textarea className="bg-light ml-1 mr-1 form-control" rows="2" cols="250" name="rules" id="question"></textarea>
                                    </div>

                                    <div className="row">
                                        <h6 className="ml-1 mt-2 py-2">
                                            Točan odgovor
                                        </h6>
                                        <input 
                                                type="text"
                                                id="defaultCreateOrganizerFormCity"
                                                name="answer"
                                                value={this.state.answer}
                                                className="text-dark form-control mb-4 bg-light ml-2 mr-2 form-control"
                                                onChange={this.handleChange}
                                        />
                                    </div>
                                </div>
                            </div>
                            <button className="btn btn-info btn-block" type="submit">
                                    Dodaj pitanje!
                            </button>
                            <hr />
                            <a
                                href={"/organizer/" + this.props.match.params.organizerId}
                                class="btn btn-primary btn-block"
                                role="button"
                            >
                                Završi dodavanje pitanja
                            </a>
                            {this.state.error && (
                                <p className="text-danger">Pitanje nije uspješno dodano.</p>
                            )}
                            </form>
                    </div>
                </>
            }
        </div>
        );
    }
}

export default AddTrialQuizQuestion;