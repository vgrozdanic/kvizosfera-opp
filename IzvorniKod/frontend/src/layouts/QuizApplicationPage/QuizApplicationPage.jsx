import React from "react";
import Navbar from "../../components/navbar/Navbar";
import _ from 'lodash';
import APIService from "../../services/APIService";

const apiService = new APIService();

class QuizApplicationPage extends React.Component {
  state = {
    league: undefined,
    user: undefined,
    organizer: undefined,
    name: undefined,
    team_name: undefined,
    event_date: undefined,
    location: undefined,
    capacity: undefined,
    concept: undefined,
    fee: undefined,
    rules: undefined,
    prize: undefined,
    min_num_of_players: undefined,
    max_num_of_players: undefined,
    num_of_team_members: undefined,
    quiz_event: undefined,
    error: undefined,
    success: undefined,
    organizers: [],
    organizerIds: [],
    amIOrganizer: false,
  };

  handleChange = event =>
    this.setState({ [event.target.name]: event.target.value });

  submitApplication = async event => {
    event.preventDefault();
    apiService
      .sendApplication(
        this.state.team_name,
        this.state.num_of_team_members,
        this.state.quiz_event
      )
      .then(() => {
        this.setState({ success: true });
        window.location.reload(false);
      })
      .catch(() => this.setState({ error: true }));
      alert("Uspješno ste se prijavili na kviz");
      this.props.history.push("/home")
  };

  componentDidMount() {
    if (this.props.match.params) {
      apiService
        .getQuizData(this.props.match.params.quizId)
        .then(response => {
          this.setState({
            quiz_event: this.props.match.params.quizId,
            league: response.league,
            user: response.user,
            organizer: response.organizer,
            name: response.name,
            event_date: response.event_date,
            location: response.location,
            capacity: response.capacity,
            concept: response.concept,
            fee: response.fee,
            rules: response.rules,
            prize: response.prize,
            min_num_of_players: response.min_num_of_players,
            max_num_of_players: response.max_num_of_players,
            has_quiz_entry: response.has_quiz_entry,
          });
        })
        .catch(error => this.setState({ error: true }));

      apiService.getUserOrganizers()
        .then((response) => {
          this.setState({
            organizers: response
          })

          this.getOrganizerIds();
          this.checkIfIAmOrganizer();
        })
        .catch(error => this.setState({ error: true }));
    }
  }

  getOrganizerIds = () => {
    this.setState(state => {
      const organizerIds = [];
      state.organizers.map((organizer, index) => {
        organizerIds.push(organizer.id);
      })

      return {
        organizerIds: organizerIds
      };
    })
  };

  checkIfIAmOrganizer = () => {
    if (this.state.organizerIds.includes(this.state.organizer)) {
      this.setState({ amIOrganizer: true });
    }
  };

  render() {
    const date = new Date(this.state.event_date);
    return (
      <div>
        <Navbar />
        {this.state.amIOrganizer ?
          <div>
            Vi ste organizator ovog kviza!
          </div>
          :
          <>
            {this.state.has_quiz_entry ?
              <div className="container col-md-5">
                <div className="alert alert-danger mt-3" role="alert">
                  Već ste prijavljeni na ovaj kviz.
                </div>
              </div>
              :
              <div className="text-center">
              {date < new Date() ? (
                <div className="container col-sm-5 text-center border border-light">
                <h2 className="text-danger">Kviz je završen.Nije moguće prijaviti se!</h2>
                <div className="text-center">
                  <a href={`/quiz/${this.state.quiz_event}`} className="btn btn-outline-primary mb-2" role="button">Povratak</a>
                </div>
                </div>
                ) :(
              <div>
                <br />
                <br />
                <h1>Prijavi se na kviz <a href={'/quiz/' + this.props.match.params.quizId}>{this.state.name}</a>!</h1>
              <div className="container col-sm-5">
                <form
                  className="text-center border p-5"
                  onSubmit={this.submitApplication}
                >
                  <div className="row">
                    Naziv ekipe:
                    <input
                      type="text"
                      id="team_id"
                      name="team_name"
                      className="form-control mb-4"
                      onChange={this.handleChange}
                    />
                  </div>
                  <div className="row">
                    Broj članova: (od {this.state.min_num_of_players} do {this.state.max_num_of_players})
                    <select className="form-control mb-2" name="num_of_team_members" onChange={this.handleChange}>
                    { _.range(this.state.min_num_of_players, this.state.max_num_of_players+1).map(value => <option key={value} value={value}>{value}</option>) }
                    <option selected hidden>Odaberi</option>
                    </select>
                  </div>
                  <button className="btn btn-primary" type="submit">
                    Prijava
                  </button>
                  {this.state.error && (
                    <p className="text-danger">
                      Došlo je do pogreške prilikom pokušaja prijave na kviz
                    </p>
                  )}
                  {this.state.success && (
                    <p className="text-success">Prijava uspješno poslana!</p>
                  )}
                </form>
              </div>
            </div>
            )}
            </div>
            }
          </>
        }
      </div>
    )
}
}

export default QuizApplicationPage;
