import React from 'react';
import Navbar from '../../components/navbar/Navbar';
import '../../components/MySyles/PetarStilovi.css';
import APIService from '../../services/APIService';

const apiService = new APIService();

class EditOrganizer extends React.Component {
  state = {
    organizerName: undefined,
    organizerLocation: undefined,
    organizerPicture: undefined,
    organizerId: undefined,
    sucess: false,
    error: false,
    deleteAccount: false,
  };


  componentDidMount() {
    if (this.props.match.params) {
      apiService
        // eslint-disable-next-line react/prop-types
        .getOrganizerData(this.props.match.params.organizerId)
        .then((response) => {
          this.setState({ organizerName: response.name,
            organizerId: this.props.match.params.organizerId,
            organizerLocation: response.location });
        });
    }
  }

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };


  submitEditOrganizer = async (event) => {
    event.preventDefault();
    this.state.sucess = false;
    this.state.error = false;
    apiService
      .editOrganizer(
        this.state.organizerName,
        this.state.organizerId,
        this.state.organizerLocation,
       // this.state.organizerPicture,
      )
      .then(() => {
        // eslint-disable-next-line no-alert
        alert('Vaši podatci su uspješno pormijenjeni!');
        this.setState({ success: true });
        // this.props.history.push('/login');
      })
      // eslint-disable-next-line no-alert
      .catch(() => {
        this.setState({ error: true });
        alert('Popunite ispravno sva polja! Hvala.'); });
  };

  deleteOrganizer = () => {
    apiService
      .deleteOrganizer(this.state.organizerId)
      .then((response) => {
        this.props.history.push("/home");
      })
      .then(() => {
        // eslint-disable-next-line no-alert
        alert('Profil organizatora obrisan!');
        this.setState({ success: true });
        // this.props.history.push('/login');
      })
      .catch(error => this.setState({ error: true }));
  };


  render() {
    return (
      <div>
        <Navbar />
        {/* Naslov */}
        <div className="text-center naslov">
          Uredi podatke organizatora!
        </div>
        <div className="container col-md-5">
          <form className="text-center border p-5 font17" onSubmit={this.submitEditOrganizer}>
            {/* Ime organizatora */}
            <div className="row">
            Ime:
              <input
                defaultValue={this.state.organizerName}
                type="text"
                id="organizerNameId"
                name="organizerName"
                className="form-control mb-4 marginaTop"
                onChange={this.handleChange}
              />
            </div>
            {/* Lokacija */}
            <div className="row">
            Lokacija:
              <input
                defaultValue={this.state.organizerLocation}
                type="text"
                id="organizerLocationId"
                name="organizerLocation"
                className="form-control mb-4 marginaTop"
                onChange={this.handleChange}
              />
            </div>
            {/**
            *{/* Slika
            <div className="row">
            Slika:
              <input
                type="text"
                id="organizerNameId"
                name="organizerName"
                className="form-control mb-4 marginaTop"
                onChange={this.handleChange}
              />
            </div>
            */ }

            {/*  Tipka potvrdi */}
            <button className="btn btn-info btn-block my-4" type="submit">
              Potvrdi!
            </button>
          </form>
          <button className="btn btn-danger btn-block mt-2 mb-2" onClick={this.deleteOrganizer}>Obriši profil organizatora</button>
        </div>
      </div>
    );
  }
}

export default EditOrganizer;
