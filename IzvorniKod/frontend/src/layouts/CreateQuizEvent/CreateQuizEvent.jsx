import React from 'react';
import Navbar from '../../components/navbar/Navbar';
import '../../components/MySyles/PetarStilovi.css';
import APIService from '../../services/APIService';

const apiService = new APIService();

const dateOptions = {
  weekday: "long",
  year: "numeric",
  month: "long",
  day: "numeric",
  hour12 : false,
  hour:  "2-digit",
  minute: "2-digit",
}

class CreateQuizEvent extends React.Component {
  state = {
    quizName: undefined,
    quizOrganizer: undefined,
    quizCategory: undefined,
    quizLeague: undefined,
    quizHour: undefined,
    quizMinutes: undefined,
    quizDay: undefined,
    quizMonth: undefined,
    quizYear: undefined,
    quizLocation: undefined,
    quizCapacity: undefined,
    quizRangeOfPlayersMin: undefined,
    quizRangeOfPlayersMax: undefined,
    quizConcept: 'Koncept kviza nije eksplicitno naveden.',
    quizRules: 'Pravila kviza nisu eksplicitno navedena.',
    quizPrize: 'Nagrade nisu eksplicitno navedene.',
    quizRegistrationFee: undefined,
    categoryList: ["Kviz općeg znanja", "Sportski kviz", "Glazbeni kviz", "Filmski kviz", "Ostalo"],
    organizerList: undefined,
    leagueList: undefined,
    sucess: undefined,
    error: undefined,
    checkLeague: false,
    noOrganizersError: undefined,
    invalidNumber: undefined,
    errors: {
      name_undefined: false,
      organizer_undefined: false,
      time_not_correct: false,
      category_undefined: false,
      location_undefined: false,
      capacity_not_correct: false,
      num_of_players_not_correct: false,
      fee_not_correct: false,
    }
  };


  componentDidMount() {
    apiService
      .getUserOrganizers()
      .then((response) => {
        // we set array we got in response to organizerList, but also we need
        // to initialize organizer value (since function handleChange will only be
        // called when we change value of select element) to cover the case when
        // user doesn't change value in select element
        response[0] ?
          (this.setState({ organizerList: response})) :
          (this.setState({ noOrganizersError: true }))
      });
  }

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleChangeOrganizer = (event) => {
    // eslint-disable-next-line func-names
    this.setState({ [event.target.name]: event.target.value }, function () {
      this.doCheckLeague()
        .then(() => { this.getLeagues(); });
    });

    apiService.getOrganizerInfo(event.target.value)
      .then((response) => {
        this.setState({
          quizLocation: response.location,
        });

        document.getElementById("quizLocationId").value = response.location;
        document.getElementById("quizRangeOfPlayersMinId").value = "";
        document.getElementById("quizRangeOfPlayersMaxId").value = "";
        document.getElementById("quizConceptId").value = "";
        document.getElementById("quizRegistrationFeeId").value = "";
        document.getElementById("quizRulesId").value = "";
      })
      .catch(() => this.setState({ error: true }));
  };

  handleChangeLeague = (event) => {
    event.preventDefault();
    var value = event.target.value;
    const leagueList = this.state.leagueList;
    if (value !== 'bez_lige') {
      apiService.getLeagueData(event.target.value)
        .then((response) => {
          this.setState({
            quizLocation: response.location,
            quizRangeOfPlayersMin: response.min_num_of_players,
            quizRangeOfPlayersMax: response.max_num_of_players,
            quizConcept: response.concept,
            quizRegistrationFee: response.fee,
            quizRules: response.rules,
          });

          document.getElementById("quizLocationId").value = response.location;
          document.getElementById("quizRangeOfPlayersMinId").value = response.min_num_of_players;
          document.getElementById("quizRangeOfPlayersMaxId").value = response.max_num_of_players;
          document.getElementById("quizConceptId").value = response.concept;
          document.getElementById("quizRegistrationFeeId").value = response.fee;
          document.getElementById("quizRulesId").value = response.rules;
        })
        .catch(() => this.setState({ error: true }));
    } else {
      document.getElementById("quizLocationId").value = "";
      document.getElementById("quizRangeOfPlayersMinId").value = "";
      document.getElementById("quizRangeOfPlayersMaxId").value = "";
      document.getElementById("quizConceptId").value = "";
      document.getElementById("quizRegistrationFeeId").value = "";
      document.getElementById("quizRulesId").value = "";
    };
    this.setState({ quizLeague: value});
  }

  getLeagues = async () => {
    apiService.getOrganizerLeagues(this.state.quizOrganizer)
      .then((response) => {
        this.setState({ leagueList: response, quizLeague: response[0].id });

        apiService.getLeagueData(response[0].id)
        .then((response) => {
          this.setState({
            quizLocation: response.location,
            quizRangeOfPlayersMin: response.min_num_of_players,
            quizRangeOfPlayersMax: response.max_num_of_players,
            quizConcept: response.concept,
            quizRegistrationFee: response.fee,
            quizRules: response.rules,
          });

          document.getElementById("quizLocationId").value = response.location;
          document.getElementById("quizRangeOfPlayersMinId").value = response.min_num_of_players;
          document.getElementById("quizRangeOfPlayersMaxId").value = response.max_num_of_players;
          document.getElementById("quizConceptId").value = response.concept;
          document.getElementById("quizRegistrationFeeId").value = response.fee;
          document.getElementById("quizRulesId").value = response.rules;
        })
        .catch(() => this.setState({ error: true }));
      })
      .catch(() => this.setState({ error: true }));
  }

  doCheckLeague = async () => {
    apiService.getOrganizerLeagues(this.state.quizOrganizer)
      .then((response) => {
        if (response[0] !== undefined) this.state.checkLeague = true;
        if (response[0] === undefined) this.state.checkLeague = false;
      });
  }

  submitCreateQuizEvent = async (event) => {
    event.preventDefault();
    let er = false;
    var kapacitet = false;
    var ime = false;
    var organizator = false;
    var vrijeme = false;
    var kategorija = false;
    var raspon = false;
    var lokacija = false;
    var kotizacija = false;
    this.setState({ errors: { 
    capacity_not_correct: false,
    name_undefined: false,
    organizer_undefined: false,
    time_not_correct: false,
    category_undefined: false,
    num_of_players_not_correct: false,
    location_undefined: false,
    fee_not_correct: false } 
  });
  if(this.state.quizName === undefined){
    ime = true;
    er = true;
  }
  if(this.state.quizOrganizer === undefined
    || this.state.quizOrganizer === 'Odaberite...'){
    organizator = true;
    er = true;
  }
  if(this.state.quizHour === undefined
    || this.state.quizMinutes === undefined 
    || this.state.quizDay === undefined 
    || this.state.quizMonth === undefined 
    || this.state.quizYear === undefined
    || (this.state.quizHour > 23) 
    || (this.state.quizHour < 0)
    || (this.state.quizMinutes > 59)
    || (this.state.quizMinutes < 0)    
    || (this.state.quizDay > 31)
    || (this.state.quizDay < 1)
    || (this.state.quizMonth < 1)
    || (this.state.quizMonth > 12)
    || (this.state.quizYear < 1000)
    || (this.state.quizYear > 9999)
    || !isFinite(this.state.quizHour)
    || !isFinite(this.state.quizMinutes)
    || !isFinite(this.state.quizDay)
    || !isFinite(this.state.quizYear)
    || !isFinite(this.state.quizMonth)
    ){
      vrijeme = true;
    er = true;
  }
  if(this.state.quizCategory === undefined){
    kategorija = true;
    er = true;
  }
  if(this.state.quizLocation === undefined){
    lokacija = true;
    er = true;
  }
  if(this.state.quizCapacity === undefined
    || !isFinite(this.state.quizCapacity)
    || (this.state.quizCapacity < 0)
    ){
     kapacitet = true;
    er = true;
  }
  if(this.state.quizRangeOfPlayersMin === undefined
    || this.state.quizRangeOfPlayersMax === undefined
    || !isFinite(this.state.quizRangeOfPlayersMin)
    || !isFinite(this.state.quizRangeOfPlayersMax)
    || (this.state.quizRangeOfPlayersMax < this.state.quizRangeOfPlayersMin)
    || (0 > this.state.quizRangeOfPlayersMin)
    ){
     raspon = true;
    er = true;
  }
  if(this.state.quizRegistrationFee === undefined
    || !isFinite(this.state.quizRegistrationFee)
    || (this.state.quizRegistrationFee < 0)
    ){
    kotizacija = true;
    er = true;
  }
  var date = new Date();
  const provjeriDatum = new Date(parseInt(this.state.quizYear, 10), parseInt(this.state.quizMonth, 10) - 1, parseInt(this.state.quizDay, 10)-1, parseInt(this.state.quizHour, 10), parseInt(this.state.quizMinutes, 10), 0, 0);
  console.log(date);
  console.log(provjeriDatum);
  if(provjeriDatum <= date){
    vrijeme = true;
    er = true;
  }

  this.setState({ errors: { 
    capacity_not_correct: kapacitet,
    name_undefined: ime,
    organizer_undefined: organizator,
    time_not_correct: vrijeme,
    category_undefined: kategorija,
    num_of_players_not_correct: raspon,
    location_undefined: lokacija,
    fee_not_correct: kotizacija } 
  });

  if(er) return;

    var leagueID;
    if (document.getElementById("quizLeagueId")){
      leagueID = document.getElementById("quizLeagueId").value;
      if (leagueID == 'bez_lige'){
        leagueID = undefined;
      }
    } else {
      leagueID = undefined;
    }
    if (parseInt(this.state.quizRangeOfPlayersMax) < parseInt(this.state.quizRangeOfPlayersMin)) {
      this.setState( { invalidNumber: true } );
      return;
    }
    this.state.sucess = false;
    this.state.error = false;
    const quizDate = new Date(parseInt(this.state.quizYear, 10), parseInt(this.state.quizMonth, 10) - 1, parseInt(this.state.quizDay, 10), parseInt(this.state.quizHour, 10), parseInt(this.state.quizMinutes, 10), 0, 0);
    apiService
      .createQuizEvent(
        this.state.quizName,
        this.state.quizOrganizer,
        leagueID,
        quizDate,
        this.state.quizLocation,
        this.state.quizCapacity,
        this.state.quizRangeOfPlayersMin,
        this.state.quizRangeOfPlayersMax,
        this.state.quizRegistrationFee,
        this.state.quizPrize,
        this.state.quizConcept,
        this.state.quizRules,
        this.state.quizCategory,
      )
      .then(() => {
        // eslint-disable-next-line no-alert
        alert('Vaš kviz je uspješno registriran!');
        window.location="/home";
      })
      // eslint-disable-next-line no-alert
      .catch(() => {
        alert('Popunite ispravno sva polja! Hvala.'); });
  };


  render() {
    return (
      <div>
        <Navbar />
          <>
          {/* Naslov */}
          <div className="text-center naslov">
            Organiziraj kviz!
          </div>
          <div className="container col-md-5">
            {
            this.state.noOrganizersError ?
              (<div className="">
                <div className="alert alert-danger mt-3" role="alert">
                  Nema registriranih organizatora! Molimo vas dodajte organizatora.
                </div>
                <div className="d-flex justify-content-center">
                <button className="btn btn-outline-primary" onClick={() => {this.props.history.push("/create-organizer")}}>
                  Dodaj organizatora
                </button>
                </div>
              </div>) :
            (<form className="text-center border p-5 font17" onSubmit={this.submitCreateQuizEvent}>
              {/* Ime kviza */}
              <div className="row">
              Ime kviza:
                <input
                  type="text"
                  id="quizNameId"
                  name="quizName"
                  className="form-control mb-4 marginaTop"
                  onChange={this.handleChange}
                />
              </div>
              {this.state.errors.name_undefined && (
              <p className="text-danger">Molimo unesite ime kviza.</p>
            )}
              {/* Organizator */}
              <div className="row">
              Poveži s kvizovima određenog organizatora:
                <select
                  required
                  className="form-control mb-4 marginaTop"
                  id="quizOrganizerId"
                  name="quizOrganizer"
                  onChange={this.handleChangeOrganizer}
                >
                  <option disabled selected hidden>Odaberite...</option>
                  {this.state.organizerList
                    // eslint-disable-next-line no-unused-vars
                    && this.state.organizerList.map((quizOrganizer, index) => (
                      <option value={quizOrganizer.id}>{quizOrganizer.name}</option>
                    ))}
                </select>

              </div>
              {this.state.errors.organizer_undefined && (
              <p className="text-danger">Molimo odaberite organizatora kviza.</p>
            )}
              {/* Liga */}
              {(this.state.checkLeague === true)
              && (
              <div className="row">
              Poveži s ligom odabranog organizatora:
                <select className="form-control mb-4 marginaTop" id="quizLeagueId" name="quizLeague" onChange={this.handleChangeLeague}>
                  <option value="bez_lige">Bez lige</option>
                  {this.state.leagueList
                    // eslint-disable-next-line no-unused-vars
                    && this.state.leagueList.map((quizLeague, index) => (
                      <option value={quizLeague.id}>{quizLeague.name}</option>
                    ))}
                </select>
              </div>
              )}
              {/* Datum održavanja kviza */}
              <div className="container">
                <div className="row">
                  <div className="col-sm">
                    Sati:
                    <input
                      type="text"
                      id="quizHourId"
                      name="quizHour"
                      className="form-control mb-4 marginaTop"
                      onChange={this.handleChange}
                    />
                  </div>
                  <div className="col-sm">
                    Minute:
                    <input
                      type="text"
                      id="quizMinutesId"
                      name="quizMinutes"
                      className="form-control mb-4 marginaTop"
                      onChange={this.handleChange}
                    />
                  </div>
                  <div className="col-sm">
                    Dan
                    <input
                      type="text"
                      id="quizDayId"
                      name="quizDay"
                      className="form-control mb-4 marginaTop"
                      onChange={this.handleChange}
                    />
                  </div>
                  <div className="col-sm">
                    Mjesec
                    <input
                      type="text"
                      id="quizMonthId"
                      name="quizMonth"
                      className="form-control mb-4 marginaTop"
                      onChange={this.handleChange}
                    />
                  </div>
                  <div className="col-sm">
                    Godina
                    <input
                      type="text"
                      id="quizYearId"
                      name="quizYear"
                      className="form-control mb-4 marginaTop"
                      onChange={this.handleChange}
                    />
                  </div>
                </div>
              </div>
              {this.state.errors.time_not_correct && (
              <p className="text-danger" id="timeId">Molimo unesite ispravno vrijeme kviza. Kviz se može prijaviti od sutrašnjeg dana pa na dalje.</p>
            )}
              {/* Kategorija */}
              <div className="row">
              Odaberi kategoriju:
                <select
                  required
                  className="form-control mb-4 marginaTop"
                  id="quizCategory"
                  name="quizCategory"
                  onChange={this.handleChange}
                >
                  <option disabled selected hidden>Odaberite...</option>
                  {this.state.categoryList
                    // eslint-disable-next-line no-unused-vars
                    && this.state.categoryList.map((category, index) => (
                      <option value={index + 1}>{category}</option>
                    ))}
                </select>

              </div>
              {this.state.errors.category_undefined && (
              <p className="text-danger">Molimo odaberite kategoriju kviza.</p>
            )}
              {/* Lokacija */}
              <div className="row">
            Lokacija:
                <input
                  type="text"
                  id="quizLocationId"
                  name="quizLocation"
                  className="form-control mb-4 marginaTop"
                  onChange={this.handleChange}
                />
              </div>
              {this.state.errors.location_undefined && (
              <p className="text-danger">Molimo unesite lokaciju održavanja kviza.</p>
            )}
              {/* Kapacitet */}
              <div className="row">
            Kapacitet:
                <input
                  type="text"
                  id="quizCapacityId"
                  name="quizCapacity"
                  className="form-control mb-4 marginaTop"
                  onChange={this.handleChange}
                />
              </div>
              {this.state.errors.capacity_not_correct && (
              <p className="text-danger"id="capId">Molimo unesite ispravan kapacitet kviza.</p>
            )}
              {/* Raspon članova ekipe */}
              <div className="container">
                <div className="row">
                  <div className="col-sm">
                  Minimalan br. članova ekipe:
                    <input
                      type="text"
                      id="quizRangeOfPlayersMinId"
                      name="quizRangeOfPlayersMin"
                      className="form-control mb-4 marginaTop"
                      onChange={this.handleChange}
                      style={
                        (this.state.invalidNumber ) && { borderColor: 'red' }
                        }
                    />
                  </div>
                  <div className="col-sm">
                  Maksimalan br. članova ekipe:
                    <input
                      type="text"
                      id="quizRangeOfPlayersMaxId"
                      name="quizRangeOfPlayersMax"
                      className="form-control mb-4 marginaTop"
                      onChange={this.handleChange}
                      style={
                        (this.state.invalidNumber ) && { borderColor: 'red' }
                        }
                    />
                    {this.state.invalidNumber && (<p className="text-danger">Maksimalan broj igrača je manji od minimalnog!</p>)}
                  </div>
                </div>
              </div>
              {this.state.errors.num_of_players_not_correct && (
              <p className="text-danger" id="maxId">Molimo unesite ispravan raspon igrača.</p>
            )}
              {/* Tema
              <div className="podaci" id="quizTema">
              Tematika kviza:
                <select className="podaciInput">
                  <option>Opće znanje</option>
                  <option>Sport</option>
                  <option>Film</option>
                  <option>Glazba</option>
                  <option>Likovna umjetnost</option>
                  <option>Politika</option>
                  <option>Zemljopis</option>
                  <option>Književnost</option>
                  <option>Ostalo</option>
                </select>
              </div>
              */}
              {/* Kotizacija */}
              <div className="row">
            Kotizacija (u kunama):
                <input
                  type="text"
                  id="quizRegistrationFeeId"
                  name="quizRegistrationFee"
                  className="form-control mb-4 marginaTop"
                  onChange={this.handleChange}
                />
              </div>
              {this.state.errors.fee_not_correct && (
              <p className="text-danger" id="cotizationError">Molimo unesite ispravan iznos kotizacije.</p>
            )}
              {/* Nagrade */}
              <div className="row">
            Nagrade:
                <input
                  type="text"
                  id="quizPrizeId"
                  name="quizPrize"
                  className="form-control mb-4 marginaTop"
                  enabled
                  onChange={this.handleChange}
                />
              </div>
              {/* Koncept */}
              <div className="row">
              Koncept kviza:
                <textarea
                  rows="3"
                  id="quizConceptId"
                  name="quizConcept"
                  className="form-control mb-4 marginaTop"
                  onChange={this.handleChange}
                />
              </div>
              {/* Pravila */}
              <div className="row">
              Pravila kviza:
                <textarea
                  rows="3"
                  id="quizRulesId"
                  name="quizRules"
                  className="form-control mb-4 marginaTop"
                  onChange={this.handleChange}
                />
              </div>
              {/*  Tipka potvrdi */}
              <button className="btn btn-info btn-block my-4" type="submit">
                Potvrdi!
              </button>
            </form>)
          }
          </div>
          </>
      </div>
    );
  }
}

export default CreateQuizEvent;
