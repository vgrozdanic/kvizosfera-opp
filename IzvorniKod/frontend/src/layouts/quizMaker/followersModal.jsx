import React from 'react';
import { Button, Modal } from 'react-bootstrap/';

class FollowersModal extends React.Component {
  render() {
    return (
      <div>
        <Modal show={this.props.show} onHide={this.props.handleClose} centered>
          <Modal.Header closeButton>
            <Modal.Title>Pratitelji organizatora</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.props.followers
              && this.props.followers.map((follower, index) => (
                <div key={follower.user.id}><a href={`../../profile/${follower.user.id}`}>{follower.user.username}</a></div>
              ))}
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.props.handleClose}>Povratak</Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default FollowersModal;
