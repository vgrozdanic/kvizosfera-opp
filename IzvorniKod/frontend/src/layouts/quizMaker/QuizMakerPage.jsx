import React from 'react';
import { faComments, faImages, faCalendar, faClock } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Navbar from '../../components/navbar/Navbar';
import FollowersModal from './followersModal';
import MessagesModal from './messagesModal';
import QuizmakerPicture from '../../assets/profile_picture.png';
import APIService from '../../services/APIService';
import '../MyProfile/myProfile.css'
import OrganizerRating from '../../components/rating/OrganizerRating';
import { DropdownButton, Dropdown } from 'react-bootstrap'
import OrganizerComments from '../../components/comments/OrganizerComments';

const apiService = new APIService();

const dateOptions = {
  weekday: "long",
  year: "numeric",
  month: "long",
  day: "numeric"
}

const timeOptions = {
  hour12 : false,
  hour:  "2-digit",
  minute: "2-digit",
}

class QuizMakerPage extends React.Component {
  state = {
    showFollowers: false,
    showMessages: false,
    error: false,
    success: false,
    pastQuizes: undefined,
    futureQuizes: undefined,
    name: undefined,
    location: undefined,
    info: undefined,
    members: [],
    leagues: undefined,
    organizerID: undefined,
    followers: undefined,
    followersNumber: undefined,
    following: undefined,
    memberFlag: undefined,
  }

  componentDidMount() {
    const { quizmakerID } = this.props.match.params;
    this.setState({ organizerID: quizmakerID, members: [] });
    apiService
      .getOrganizerInfo(quizmakerID)
      .then((response) => {
        this.setState({ name: response.name, location: response.location, info: response.info, following: response.following_organizer, memberFlag: response.is_organizer});
        apiService
          .getUserInfo(response.user)
          .then((response) => {
            this.setState({
              members: [response].concat(this.state.members),
            })
          })
          .catch((error) => this.setState({ error: true }));
      })
      .catch((error) => this.setState({ error: true }));
    apiService
      .getOrganizerMembers(quizmakerID)
      .then((response) => {
        response.map((member, index) => (
          apiService
            .getUserInfo(member.user)
            .then((resp) => {
              this.setState((prevState) => ({
                members: [...prevState.members, resp]
              }));
            })
            .catch((error) => this.setState({ error: true }))
        ));
      })
      .catch((error) => this.setState({ error: true }));
    apiService
      .getOrganizerFollowers(quizmakerID)
      .then((response) => {
        this.setState({ followers: response, followersNumber: response.length });
      })
      .catch((error) => this.setState({ error: true }));
    apiService
      .getOrganizerPastQuizes(quizmakerID)
      .then((response) => {
        this.setState({ pastQuizes: response });
      })
      .catch((error) => this.setState({ error: true }));
    apiService
      .getOrganizerFutureQuizes(quizmakerID)
      .then((response) => {
        this.setState({ success: true });
        this.setState({ futureQuizes: response });
      })
      .catch((error) => this.setState({ error: true }));
    apiService
      .getOrganizerLeagues(quizmakerID)
      .then((response) => {
        this.setState({ success: true });
        this.setState({ leagues: response });
      })
      .catch((error) => this.setState({ error: true }));

  }

  closeFollowers = async (event) => {
    this.setState({ showFollowers: false });
  }

  openFollowers = async (event) => {
    event.preventDefault();
    this.setState({ showFollowers: true });
  }

  closeMessages = async (event, userID) => {
    this.setState({ showMessages: false });
  }

  openMessages = async (event) => {
    event.preventDefault();
    this.setState({ showMessages: event.target.attributes[0].value });
  }

  followOrganizer = async (event) => {
    event.preventDefault();
    this.setState({ error: false });
    apiService
      .followOrganizer(this.state.organizerID)
      .then(() => {
        this.setState({ following: true });
      })
      .catch((error) => this.setState({ error: true }));
    let followers = apiService
      .getOrganizerFollowers(this.state.organizerID)
      .then((response) => {
        this.setState({ followers: response, followersNumber: response.length });
      })
      .catch((error) => this.setState({ error: true }));
    await followers;
    this.componentDidMount();
  };

  send = async (event) => {
    event.preventDefault();
    apiService.
      sendMessage(event.target.attributes[1].value, document.getElementById("msg-area").value)
      .then(() => {
            document.getElementById("msg-area").value = "";
      })
      .catch((error) => {
      });
  }

  unfollowOrganizer = async (event) => {
    event.preventDefault();
    this.setState({ error: false });
    apiService
      .unfollowOrganizer(this.state.organizerID)
      .then(() => {
        this.setState({ following: true });
      })
      .catch((error) => this.setState({ error: true }));
    let followers = apiService
      .getOrganizerFollowers(this.state.organizerID)
      .then((response) => {
        this.setState({ followers: response, followersNumber: response.length });
      })
      .catch((error) => this.setState({ error: true }));
    await followers;
    this.componentDidMount();
  };

  render() {
    return (
      <div>
        <div>
          <Navbar />
        </div>
        <div className="container">
          <div className="row mt-3 mb-3">
            <div className="col pl-2 pr-2">
              <div style={{ display: 'inline-block' }}>
                <img src={QuizmakerPicture} alt="Quizmaker" className="profilePic"/>
              </div>
              <div style={{ display: 'inline-block' }} className="ml-3 font-weight-bold">{this.state.name}</div>
              <div style={{ display: 'inline-block' }} className="ml-3">
                {!this.state.following ?
                      <button type="button" className="btn btn-outline-primary ml-2" onClick={this.followOrganizer}>Prati</button>
                      :
                      <button type="button" className="btn btn-outline-primary ml-2" onClick={this.unfollowOrganizer}>Prestani pratiti</button>
                }
              </div>
              <div style={{ display: 'inline-block' }} className="ml-2">
                <button type="button" className="btn btn-outline-primary" onClick={this.openFollowers} data-toggle="modal" data-target="#exampleModalCenter">Pratitelja: {this.state.followersNumber}</button>
              </div>
            </div>
            <div className="col d-flex align-items-center justify-content-end pl-2 pr-2">
              <span className="">
                <Dropdown>
                  <Dropdown.Toggle variant="outline-primary" id="dropdown-basic">
                    <span>Poruka</span>
                    <FontAwesomeIcon icon={faComments} className="ml-2" />
                  </Dropdown.Toggle>
                  <Dropdown.Menu>
                    {this.state.members && this.state.members.map((member, index) => (
                      <div>
                        <Dropdown.Item onClick={this.openMessages} value={member.user.id}>{`${member.first_name} ${member.last_name}`}</Dropdown.Item>
                        <MessagesModal username={member.user.username} id={member.user.id}
                                      show={this.state.showMessages == member.user.id ? true : false}
                                      handleClose={this.closeMessages} send={this.send}/>
                      </div>
                    ))}
                  </Dropdown.Menu>
                </Dropdown>
              </span>
              <span className="ml-3">
                <button type="button" className="btn btn-outline-primary" onClick={() => {}} data-toggle="modal" data-target="#exampleModalCenter">
                  <span>Galerija</span>
                  <FontAwesomeIcon icon={faImages} onClick={this.open} className="ml-2" />
                </button>
              </span>
            </div>
          </div>
          <div className="row">
            <div className="col-3 pl-2 pr-2">
              <div className="card">
                <div className="card-body">
                  <h4 className="card-title font-weight-bold">Info</h4>
                  <p className="card-text">{this.state.info}</p>
                  <h5 className="card-title">Članovi</h5>
                  {this.state.members
                    && this.state.members.map((member, index) => (
                      <div><a href={`../profile/${member.user.id}`}>{`${member.first_name} ${member.last_name}`}</a></div>
                    ))}
                  <h5 className="card-title mt-3">Lige</h5>
                    {this.state.leagues
                      && this.state.leagues.map((league, index) => (
                      <div><a href={`../league/${league.id}`}>{league.name}</a></div>
                    ))}
                  <h5 className="card-title mt-3">Lokacija</h5>
                  <p className="card-text">{this.state.location}</p>
                </div>
              </div>
              <div className="card mt-2">
                <div className="card-body text-center">
                  <a href={'/solve-trial-quiz/' + this.state.organizerID} class="btn btn-primary" role="button">Riješi probni kviz</a>
                  {this.state.memberFlag ?
                    <>
                      <hr />
                      <a href={'/edit-organizer/' + this.state.organizerID} class="btn btn-danger text-white active" role="button">Uredi podatke</a>
                      <a href={'/add-trial-quiz-question/' + this.state.organizerID} class="btn btn-primary mt-2" role="button">Dodaj pitanje za probni kviz</a>
                      <a href={'/create-league'} class="btn btn-primary mt-2" role="button">Dodaj novu ligu</a>
                      <a href={'/add-member'} class="btn btn-primary mt-2" role="button">Dodaj novog člana</a>
                    </>
                    :
                    <></>
                  }
                </div>
              </div>
              {!this.state.memberFlag ?
                <OrganizerRating organizer_id={this.props.match.params.quizmakerID}/>
                :
                <></>
              }
            </div>
            <div className="col pl-2 pr-2">
              <div className="card">
                <div className="card-body">
                  <h5 className="card-title">Idući kvizovi</h5>
                  {this.state.futureQuizes &&
                    this.state.futureQuizes.sort(function(a, b) {
                      let dateA = a.event_date;
                      let dateB = b.event_date;
                      return dateA > dateB ? 1 : dateA < dateB ? -1 : 0;
                    }).map((quizEventEntry, index) => (
                      <div className="card mb-2">
                        <div className="card-body">
                          <h5 className="card-title">
                            <a href={'/quiz/' + quizEventEntry.id}> {quizEventEntry.name}</a>
                          </h5>
                          <h6 class="card-subtitle mb-2">
                            {quizEventEntry.league ? (
                              <a href={"/league/" + quizEventEntry.league.id}>
                                <h4 class="card-title mt-2">
                                  {quizEventEntry.league.name}
                                </h4>
                              </a>
                            ) : (
                              <h4>Ne postoji liga</h4>
                            )}
                          </h6>
                          <h7 class="card-subtitle text-muted mb-2"><FontAwesomeIcon icon={faCalendar} className="mr-2" />{(new Date(quizEventEntry.event_date).toLocaleDateString("hr-HR", dateOptions))}</h7>
                          <br />
                          <h7 class="card-subtitle text-muted mb-2"><FontAwesomeIcon icon={faClock} className="mr-2" />{(new Date(quizEventEntry.event_date).toLocaleTimeString("hr-HR", timeOptions))}</h7>
                        </div>
                      </div>
                    ))}
                </div>
              </div>
            </div>
            <div className="col pl-2 pr-2">
              <div className="card">
                <div className="card-body">
                  <h5 className="card-title">Prošli kvizovi</h5>
                  {this.state.pastQuizes &&
                    this.state.pastQuizes.sort(function(a, b) {
                      let dateA = a.event_date;
                      let dateB = b.event_date;
                      return dateA > dateB ? -1 : dateA < dateB ? 1 : 0;
                    }).map((quizEventEntry, index) => (
                      <div className="card mb-2">
                        <div className="card-body">
                          <h5 className="card-title">
                            <a href={'/quiz/' + quizEventEntry.id}> {quizEventEntry.name}</a>
                          </h5>
                          <h6 class="card-subtitle mb-2">
                            {quizEventEntry.league ? (
                              <a href={"/league/" + quizEventEntry.league.id}>
                                <h4 class="card-title mt-2">
                                  {quizEventEntry.league.name}
                                </h4>
                              </a>
                            ) : (
                              <h4>Ne postoji liga</h4>
                            )}
                          </h6>
                          <h7 class="card-subtitle text-muted mb-2"><FontAwesomeIcon icon={faCalendar} className="mr-2" />{(new Date(quizEventEntry.event_date).toLocaleDateString("hr-HR", dateOptions))}</h7>
                          <br />
                          <h7 class="card-subtitle text-muted mb-2"><FontAwesomeIcon icon={faClock} className="mr-2" />{(new Date(quizEventEntry.event_date).toLocaleTimeString("hr-HR", timeOptions))}</h7>
                        </div>
                      </div>
                    ))}
                </div>
              </div>
            </div>
          </div>
          <OrganizerComments quizmakerID={this.props.match.params.quizmakerID} />
        </div>
        <FollowersModal
          show={this.state.showFollowers}
          handleClose={this.closeFollowers}
          followers={this.state.followers}
        />
      </div>
    );
  }
}

export default QuizMakerPage;
