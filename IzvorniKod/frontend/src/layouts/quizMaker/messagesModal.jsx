import React from 'react';
import { Button, Modal } from 'react-bootstrap/';

class MessagesModal extends React.Component {

  render() {
    return (
      <div>
        <Modal show={this.props.show} onHide={this.props.handleClose} centered>
          <Modal.Header closeButton>
            <Modal.Title>Pošalji poruku korisniku {this.props.username}</Modal.Title>
          </Modal.Header>
          <form className="text-center border p-5" onSubmit={this.props.send} value={this.props.id}>
              <Modal.Body>
                  <div className="row">
                      Tekst poruke:
                      <textarea rows="10" cols="150" name="msg" id="msg-area"></textarea>
                  </div>
              </Modal.Body>
              <Modal.Footer>
                  <Button color="secondary" onClick={this.props.handleClose}>Odustani</Button>
                  <Button color="primary" type="submit">Pošalji</Button>
              </Modal.Footer>
          </form>
          </Modal>
      </div>
    );
  }
}

export default MessagesModal;
