import React from "react";
import APIService from "../services/APIService";

const apiService = new APIService();

class Register extends React.Component {
  state = {
    username: undefined,
    password: undefined,
    email: undefined,
    repeat_password: undefined,
    first_name: undefined,
    last_name: undefined,
    sex: undefined,
    location: undefined,
    dob_day: undefined,
    dob_month: undefined,
    dob_year: undefined,
    error: undefined,
    errors: {
      noUsername: undefined,
      noEmail: undefined,
      noFirstName: undefined,
      noLastName: undefined,
      noDOB: undefined,
      noLocation: undefined,
      noSex: undefined,
      different_password: undefined,
      DOB_in_future: undefined,
      wrong_password_format: undefined,
      anyError: undefined
    }
  };

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  submitRegister = async event => {
    event.preventDefault();
    var errors = {};
    var error = false;
    if (this.state.username === undefined) {
      errors.noUsername = true;
      error = true;
    }
    if (this.state.email === undefined) {
      errors.noEmail = true;
      error = true;
    }
    if (this.state.first_name === undefined) {
      errors.noFirstName = true;
      error = true;
    }
    if (this.state.last_name === undefined) {
      errors.noLastName = true;
      error = true;
    }
    if (this.state.sex === undefined) {
      errors.noSex = true;
      error = true;
    }
    if (this.state.location === undefined) {
      errors.noLocation = true;
      error = true;
    }
    if (this.state.password !== this.state.repeat_password) {
      errors.different_password = true;
      error = true;
    }
    if (/\d/.test(this.state.password) == false){
      errors.wrong_password_format = true;
      error = true;
    }
    if (/\D/.test(this.state.password) == false){
      errors.wrong_password_format = true;
      error = true;
    }
    if (this.state.password){
      if (this.state.password.length < 6) {
        errors.wrong_password_format = true;
        error = true;
      }
    }
    if (this.state.dob_year && this.state.dob_month && this.state.dob_day){
      if (this.state.dob_month >= 1 && this.state.dob_month <= 12 && this.state.dob_day >= 1 && this.state.dob_day <= 31){
        var today = new Date();
        var dobDate = new Date(parseInt(this.state.dob_year), parseInt(this.state.dob_month)-1, parseInt(this.state.dob_day))
        if (dobDate > today) {
          errors.DOB_in_future = true;
          error = true;
        }
      } else {
        errors.noDOB = true;
        error = true;
      }
    } else {
      errors.noDOB = true;
      error = true;
    }
    errors.anyError = error;
    this.setState({ errors: errors });
    if (error) {
      return
    }
    const dob = `${this.state.dob_year}-${this.state.dob_month}-${this.state.dob_day}`;
    apiService
      .register(
        this.state.email,
        this.state.username,
        this.state.password,
        this.state.first_name,
        this.state.last_name,
        dob,
        this.state.sex,
        this.state.location
      )
      .then(() => {
        alert("Succesfull registration");
        this.props.history.push("/login");
      })
      .catch(() => this.setState({ error: true }));
  };

  componentDidMount() {
    apiService.logout();
  }

  render() {
    return (
      <div>
        <div className="container text-center">
          <h1>Dobrodošli u Kvizosferu</h1>
        </div>
        <div className="container col-sm-4">
          {/* Default form Register */}
          <form
            className="text-center border border-light p-5"
            onSubmit={this.submitRegister}
          >
            <p className="h4 mb-4">Registracija</p>

            {this.state.errors.anyError && (
              <p className="text-danger" id="dateInFutureError">Molimo popunite sva polja</p>
            )}

            {/* Email */}
            <div className="row">
              E-mail
              <input
                type="text"
                id="defaultRegisterFormEmail"
                name="email"
                className="form-control mb-4"
                onChange={this.handleChange}
                style={
                  (this.state.errors.noEmail) && { borderColor: "red" }
                }
              />
            </div>

            {/* Username */}
            <div className="row">
              Korisničko ime
              <input
                type="text"
                id="defaultRegisterFormUsername"
                name="username"
                className="form-control mb-4"
                onChange={this.handleChange}
                style={
                  (this.state.errors.noUsername) && { borderColor: "red" }
                }
              />
            </div>

            {/* Password */}
            <div className="row">
              Lozinka
              <input
                type="password"
                id="defaultRegisterFormPassword"
                name="password"
                className="form-control mb-4"
                style={
                  (this.state.errors.different_password || this.state.errors.wrong_password_format) && { borderColor: "red" }
                }
                onChange={this.handleChange}
              />
            </div>

            {/* Confirm Password */}
            <div className="row">
              Ponovljena lozinka
              <input
                type="password"
                id="defaultRegisterFormConfirmPassword"
                name="repeat_password"
                className="form-control mb-4 "
                style={
                  (this.state.errors.different_password || this.state.errors.wrong_password_format) && { borderColor: "red" }
                }
                onChange={this.handleChange}
              />
            </div>

            {this.state.errors.different_password ? (
              <p className="text-danger" id="differentPasswordError">Lozinke se moraju podudarati</p>
            ) : this.state.errors.wrong_password_format ?
            (
              <p className="text-danger" id="wrongPasswordFormatError">Lozinke moraju sadržavati barem jedno slovo, jedan broj i imati barem 6 znakova</p>
            ) : (<></>)}

            {/* First Name */}
            <div className="row">
              Ime
              <input
                type="text"
                id="defaultRegisterFormFirstName"
                name="first_name"
                className="form-control mb-4"
                onChange={this.handleChange}
                style={
                  (this.state.errors.noFirstName) && { borderColor: "red" }
                }
              />
            </div>

            {/* Last Name */}
            <div className="row">
              Prezime
              <input
                type="text"
                id="defaultRegisterFormLastName"
                name="last_name"
                className="form-control mb-4"
                onChange={this.handleChange}
                style={
                  (this.state.errors.noLastName) && { borderColor: "red" }
                }
              />
            </div>

            {/* Birthday */}
            <div className="row">
              <div className="col">
                Dan
                <input
                  type="text"
                  id="defaultRegisterFormDayOfBirthDay"
                  name="dob_day"
                  className="form-control mb-4"
                  style={
                    (this.state.errors.DOB_in_future || this.state.errors.noDOB) && { borderColor: "red" }
                  }
                  onChange={this.handleChange}
                />
              </div>
              <div className="col">
                Mjesec
                <input
                  type="text"
                  id="defaultRegisterFormDayOfBirthMonth"
                  name="dob_month"
                  className="form-control mb-4"
                  style={
                    (this.state.errors.DOB_in_future || this.state.errors.noDOB) && { borderColor: "red" }
                  }
                  onChange={this.handleChange}
                />
              </div>
              <div className="col">
                Godina
                <input
                  type="text"
                  id="defaultRegisterFormDayOfBirthYear"
                  name="dob_year"
                  className="form-control mb-4"
                  style={
                    (this.state.errors.DOB_in_future || this.state.errors.noDOB) && { borderColor: "red" }
                  }
                  onChange={this.handleChange}
                />
              </div>
            </div>

            {this.state.errors.DOB_in_future && (
              <p className="text-danger" id="dateInFutureError">Uneseni datum je u budućnosti</p>
            )}

            {/* Location */}
            <div className="row">
              Grad
              <input
                type="text"
                id="defaultRegisterFormLocation"
                name="location"
                className="form-control mb-4"
                onChange={this.handleChange}
                style={
                  (this.state.errors.noLocation) && { borderColor: "red" }
                }
              />
            </div>

            {/* Sex */}
            <div className="row">
              <div className="radio col-lg">
                <label>
                  <input
                    type="radio"
                    name="sex"
                    onChange={this.handleChange}
                    value="M"
                  />{" "}
                  M
                </label>
              </div>
              <div className="radio col-lg">
                <label>
                  <input
                    type="radio"
                    name="sex"
                    onChange={this.handleChange}
                    value="F"
                  />{" "}
                  Ž
                </label>
              </div>
            </div>

            {/* Register button */}
            <button className="btn btn-info btn-block my-4" type="submit" id="submitButton">
              Registriraj se
            </button>

            {/* Register */}
            <p>
              Registrirani ste?
              <a href="/Login"> Prijavite se</a>
            </p>
            {this.state.error && (
              <p className="text-danger">Registracija neuspješna</p>
            )}
          </form>
        </div>
      </div>
    );
  }
}

export default Register;
