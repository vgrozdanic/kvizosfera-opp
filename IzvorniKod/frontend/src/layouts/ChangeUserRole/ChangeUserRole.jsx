import React from 'react';
import Navbar from '../../components/navbar/Navbar';
import APIService from '../../services/APIService';

const apiService = new APIService();

class ChangeUserRole extends React.Component {

    state = {
        error: false,
        currentUserRole: undefined,
        pickedUser: undefined,
        pickedRole: undefined,
        users: [],
        roles: ["Korisnik", "Moderator", "Administrator"],
    }

    handleChange = (event) => {
        this.setState({ [event.target.name]: event.target.value });
    };

    componentDidMount() {
      apiService
      .getCurrentProfileData()
      .then((response) => {
        this.setState({
            currentUserRole: response.role
        });
      })
      .catch((error) => this.setState({ error: true }));

      apiService
      .getAllUsers()
      .then((response) => {
        this.setState({ users: response });
      })
      .catch((error) => this.setState({ error: true }));
    }

    renderSwitch(param) {
        switch(param) {
            case 1:
                return "Korisnik";
            case 2:
                return "Moderator";
            case 3:
                return "Administrator";
        }
    }

    submit = async (event) => {
        event.preventDefault();
        apiService
        .changeUserRole(this.state.pickedUser, this.state.pickedRole)
        .then(() => {
            alert('Uloga uspješno promijenjena!');
            window.location.reload(false);
        })
        .catch(error => this.setState({ error: true }));
    };

    render() {
        return(
            <div>
                <Navbar />
                    {this.state.currentUserRole === 3 ?
                        <form
                            className="text-center p-5"
                            onSubmit={this.submit}
                        >
                            <div className="card mb-2">
                                <div className="card-body">
                                    <li class="list-group-item text-center text-info list-group-item-light py-2 font-weight-bold">
                                        <h6 className="text-dark mt-2 py-2">
                                            Dodijeli ulogu nekom korisniku
                                        </h6>
                                        <select required className="form-control mb-4 marginaTop" id="pickUser" name="pickedUser" onChange={this.handleChange}>
                                            <option disabled selected hidden>Odaberi korisnika</option>
                                            {this.state.users
                                                && this.state.users.map((user, index) => (
                                                <option value={user.user.id}>{user.user.username} ({this.renderSwitch(user.role)})</option>
                                            ))}
                                        </select>
                                        <select required className="form-control mb-4 marginaTop" id="pickRole" name="pickedRole" onChange={this.handleChange}>
                                            <option disabled selected hidden>Odaberi ulogu</option>
                                            {this.state.roles
                                                && this.state.roles.map((role, index) => (
                                                <option value={index + 1}>{role}</option>
                                            ))}
                                        </select>
                                    </li>
                                </div>
                                <button className="btn btn-info m-3" type="submit">
                                    Potvrdi!
                                </button>
                                {this.state.error && (
                                    <p className="text-danger">Uloga nije uspješno promijenjena!</p>
                                )}
                            </div>
                        </form>
                        :
                        <div className="m-3">
                            Nemate ovlasti za mijenjanje korisničkih uloga!
                        </div>
                    }
            </div>
        );
    }
}

export default ChangeUserRole;
