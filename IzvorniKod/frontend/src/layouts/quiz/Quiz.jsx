import React from "react";
import "./quiz.css";
import APIService from "../../services/APIService";
import Navbar from "../../components/navbar/Navbar";
import QuizComments from "../../components/comments/QuizComments";
import QuizRating from "../../components/rating/QuizRating";
import { ModalHeader } from "reactstrap";

const apiService = new APIService();

class Quiz extends React.Component {
  state = {
    error: false,
    organizer: undefined,
    name: undefined,
    date: undefined,
    location: undefined,
    maxCapacity: undefined,
    concept: "concept",
    fee: 10,
    rules: "rules",
    prize: "prize",
    leagueName: undefined,
    leagueId: undefined,
    organizerName: undefined,
    currentUserId: false,
    organizerMembers: [],
    min_num_of_players: undefined,
    max_num_of_players: undefined,
    has_quiz_entry: undefined,
    success: undefined,
    results: []
  };

  handleChange = event =>
    this.setState({ [event.target.name]: event.target.value });

  submitCheckout = async event => {
    event.preventDefault();
    apiService
      .deleteApplication(this.props.match.params.quiz_id)
      .then(() => {
        window.location.reload();
        this.setState({ success: true });
      })
      .catch(() => this.setState({ error: true }));
  };

  componentDidMount() {
    if (this.props.match.params) {
      apiService
        .getQuizData(this.props.match.params.quiz_id)
        .then(response => {
          this.setState({
            date: response.event_date,
            organizer: response.organizer,
            name: response.name,
            location: response.location,
            maxCapacity: response.capacity,
            concept: response.concept,
            fee: response.fee,
            rules: response.rules,
            prize: response.prize,
            min_num_of_players: response.min_num_of_players,
            max_num_of_players: response.max_num_of_players,
            leagueId: response.league,
            has_quiz_entry: response.has_quiz_entry
          });

          apiService
            .getLeagueInfo(response.league)
            .then(response => {
              this.setState({
                leagueName: response.name
              });
            })
            .catch(error => this.setState({ error: true }));

          apiService
            .getCurrentProfileData()
            .then(response => {
              this.setState({
                currentUserId: response.user.id
              });
            })
            .catch(error => this.setState({ error: true }));

          apiService
            .getOrganizerInfo(response.organizer)
            .then(response => {
              this.setState({
                organizerName: response.name,
                organizerMembers: [response.user]
              });

              apiService
                .getOrganizerMembers(response.organizer)
                .then(response => {
                  response.map((member, index) =>
                    apiService
                      .getUserInfo(member.user)
                      .then(resp => {
                        this.setState(prevState => ({
                          organizerMembers: [
                            ...prevState.organizerMembers,
                            resp.user.id
                          ]
                        }));
                      })
                      .catch(error => this.setState({ error: true }))
                  );
                })
                .catch(error => this.setState({ error: true }));
            })
            .catch(error => this.setState({ error: true }));

          apiService
            .getOrganizerMembers(response.organizer)
            .then(response => {
              response.map((member, index) =>
                apiService
                  .getUserInfo(member.user)
                  .then(resp => {
                    this.setState(prevState => ({
                      organizerMembers: [
                        ...prevState.organizerMembers,
                        resp.user.id
                      ]
                    }));
                  })
                  .catch(error => this.setState({ error: true }))
              );
            })
            .catch(error => this.setState({ error: true }));
        })
        .catch(error =>
          this.setState({
            error: true
          })
        );
    }
    apiService
      .getQuizResults(this.props.match.params.quiz_id)
      .then(response => {
        this.setState({ results: response });
      })
      .catch(error => this.setState({ error: true }));
  }

  render() {
    const date = new Date(this.state.date);
    return (
      <div>
        <Navbar />
        <div className="container-fluid Main p-3 my-3 border">
          <div className="d-flex">
            <Calendar date={this.state.date} />
            <div className="flex-fill ml-3">
              <h1 className="display-2">{this.state.name}</h1>
              <h6>
                Lokacija:{" "}
                <a className="font-weight-normal">{this.state.location}</a>
              </h6>
              <h6>
                Liga:{" "}
                <a href={"/league/" + this.state.leagueId}>
                  {this.state.leagueName}
                </a>
              </h6>
              <h6>
                Organizator:{" "}
                <a href={"/organizer/" + this.state.organizer}>
                  {this.state.organizerName}
                </a>
              </h6>
              {date < new Date() && (
                <div className="d-flex mt-1">
                  <>
                    {this.state.organizerMembers.includes(
                      this.state.currentUserId
                    ) ? (
                      <>
                        <a
                          href={`/add-event-quiz-question/${this.props.match.params.quiz_id}`}
                          className="btn btn-dark mr-1 flex-fill"
                          role="button"
                        >
                          Dodaj pitanje
                        </a>
                        <a
                          href={`/add-quiz-results/${this.props.match.params.quiz_id}`}
                          className="btn btn-dark mx-1 flex-fill"
                          role="button"
                        >
                          Ažuriraj rezultate
                        </a>
                      </>
                    ) : (
                      <></>
                    )}
                    <a
                      href={`/solve-event-quiz/${this.props.match.params.quiz_id}`}
                      className="btn btn-dark ml-1 flex-fill"
                      role="button"
                    >
                      Riješi kviz
                    </a>
                  </>
                </div>
              )}
            </div>
          </div>

          {date > new Date() ? (
            <div>
              <div className="d-flex justify-content-between mt-4 mx-1">
                <h6 className="mt-2">
                  Broj ljudi po ekipi :{" "}
                  <a className="font-weight-normal">
                    {this.state.min_num_of_players} -{" "}
                    {this.state.max_num_of_players}
                  </a>
                </h6>
                <h6 className="mt-2">
                  Kapacitet kviza :{" "}
                  <a className="font-weight-normal">
                    {this.state.maxCapacity} ekipa
                  </a>
                </h6>
                <h6 className="mt-2">
                  Kotizacija :{" "}
                  <a className="font-weight-normal">{this.state.fee} kn</a>
                </h6>

                {this.state.organizerMembers.includes(
                  this.state.currentUserId
                ) ? (
                  <></>
                ) : (
                  <>
                    {this.state.has_quiz_entry == false ? (
                      <a
                        href={`/application-page/${this.props.match.params.quiz_id}`}
                        className="btn btn-primary"
                        role="button"
                      >
                        Prijavi se!
                      </a>
                    ) : (
                      <button
                        className="btn btn-primary col-sm-2"
                        role="button"
                        onClick={this.submitCheckout}
                      >
                        Odjavi se!
                      </button>
                    )}
                  </>
                )}
              </div>
              <div>
                <Information
                  concept={this.state.concept}
                  rules={this.state.rules}
                  prize={this.state.prize}
                />
              </div>
            </div>
          ) : (
            <div>
              <QuizRating quizId={this.props.match.params.quiz_id} />
              <h3>Rezultati</h3>
              <ol>
                {this.state.results &&
                  this.state.results.map((team_name, index) => (
                    <li>{team_name.name}</li>
                  ))}
              </ol>
            </div>
          )}
          <QuizComments quiz_id={this.props.match.params.quiz_id} />
        </div>
      </div>
    );
  }
}

const Calendar = props => {
  const date = new Date(props.date);
  const day = date.toLocaleDateString("hr-HR", { day: "numeric" });
  const month = date.toLocaleDateString("hr-HR", { month: "long" });
  const time = date.toLocaleTimeString("hr-HR", {
    hour: "numeric",
    minute: "numeric"
  });
  return (
    <div className="Border mx-0 col-sm-3 Calendar">
      <h1 className="Middle display-5">
        {day}
        <br />
        {month}
        <br />
        {time}
      </h1>
    </div>
  );
};
const Information = props => (
  <div className="mt-2">
    <h3>Koncept</h3>
    <p>{props.concept}</p>
    <h3>Pravila</h3>
    <p>{props.rules}</p>
    <h3>Nagrade</h3>
    <p>{props.prize}</p>
  </div>
);

export default Quiz;
