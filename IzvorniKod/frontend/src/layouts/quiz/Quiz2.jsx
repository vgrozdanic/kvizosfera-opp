import React from "react";
import { faCalendar, faClock, faMapMarker, faMoneyBillAlt, faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './quiz.css';
import APIService from '../../services/APIService';
import Navbar from '../../components/navbar/Navbar';
import QuizComments from '../../components/comments/QuizComments';
import QuizRating from '../../components/rating/QuizRating';
import { ModalHeader } from 'reactstrap';

const apiService = new APIService();

const dateOptions = {
  weekday: "long",
  year: "numeric",
  month: "long",
  day: "numeric"
}

const timeOptions = {
  hour12 : false,
  hour:  "2-digit",
  minute: "2-digit",
}

class Quiz extends React.Component {
  state = {
    error: false,
    organizer: undefined,
    name: undefined,
    date: undefined,
    location: undefined,
    maxCapacity: undefined,
    /* remainingCapacity: undefined, */
    /* image:
      "https://image.shutterstock.com/image-vector/quiz-thin-line-concept-vector-260nw-663203059.jpg", */
    concept: "concept",
    fee: 10,
    rules: "rules",
    prize: "prize",
    leagueName:undefined,
    leagueId: undefined,
    organizerName: undefined,
    currentUserId: false,
    organizerMembers: [],
    min_num_of_players: undefined,
    max_num_of_players: undefined,
    has_quiz_entry: undefined,
    success:undefined,
    results: [],
  };

  handleClick = () => {
    /* this.setState(state => ({
      remainingCapacity: state.remainingCapacity - 1
    })); */
  };

  handleChange = event =>
    this.setState({ [event.target.name]: event.target.value });

  submitCheckout = async event => {
    event.preventDefault();
    apiService
      .deleteApplication(
        this.props.match.params.quiz_id
      )
      .then(() => {
        window.location.reload();
        this.setState({ success: true });
      })
      .catch(() => this.setState({ error: true }));
  };

  componentDidMount() {
    if (this.props.match.params) {
      apiService
        .getQuizData(this.props.match.params.quiz_id)
        .then(response => {
          this.setState({
            date: response.event_date,
            organizer: response.organizer,
            name: response.name,
            location: response.location,
            maxCapacity: response.capacity,
            concept: response.concept,
            fee: response.fee,
            rules: response.rules,
            prize: response.prize,
            min_num_of_players: response.min_num_of_players,
            max_num_of_players: response.max_num_of_players,
            leagueId: response.league,
            has_quiz_entry: response.has_quiz_entry,
          });

          apiService
          .getLeagueInfo(response.league)
          .then(response => {
            this.setState({
              leagueName: response.name,
            });
          })
          .catch(error => this.setState({ error: true })
          );

          apiService
          .getCurrentProfileData()
          .then(response => {
            this.setState({
              currentUserId: response.user.id,
            });
          })
          .catch(error => this.setState({ error: true })
          );

          apiService
          .getOrganizerInfo(response.organizer)
          .then(response => {
            this.setState({
              organizerName: response.name,
              organizerMembers: [response.user],
            });

            apiService
            .getOrganizerMembers(response.organizer)
            .then((response) => {
              response.map((member, index) => (
                apiService
                  .getUserInfo(member.user)
                  .then((resp) => {
                    this.setState((prevState) => ({
                      organizerMembers: [...prevState.organizerMembers, resp.user.id]
                    }));
                  })
                  .catch((error) => this.setState({ error: true }))
              ));
            })
            .catch(error => this.setState({ error: true })
            );
          })
          .catch(error => this.setState({ error: true })
          );

          apiService
            .getOrganizerMembers(response.organizer)
            .then((response) => {
              response.map((member, index) => (
                apiService
                  .getUserInfo(member.user)
                  .then((resp) => {
                    this.setState((prevState) => ({
                      organizerMembers: [...prevState.organizerMembers, resp.user.id]
                    }));
                  })
                  .catch((error) => this.setState({ error: true }))
              ));
            })
            .catch(error => this.setState({ error: true })
            );
        })
        .catch(error =>
          this.setState({
            error: true
          })
        );
      }
        apiService
        .getQuizResults(this.props.match.params.quiz_id)
        .then((response) => {
            this.setState({ results: response });
        }).catch(error => this.setState({ error: true }));
  }

  render() {
    const date = new Date(this.state.date);
    return (
      <div>
        <Navbar />
        <div className="container-fluid Main p-3 my-3">
          <div className="text-weight-bold">
            <div className="card mb-1 border-primary">
              <h4 className="card-header bg-light">
                {this.state.name}
              </h4>
              <div className="card-body">
                <h6 class="card-subtitle mb-2">
                  Liga: <a href={'/league/' + this.state.leagueId}>{this.state.leagueName}</a>
                </h6>
                <h6 class="card-subtitle mb-2">
                  Organizator: <a href={'/organizer/' + this.state.organizer}>{this.state.organizerName}</a>
                </h6>
                <FontAwesomeIcon icon={faClock} className="mr-2" /><h7 class="card-subtitle font-weight-bold mb-2">{(new Date(this.state.date).toLocaleTimeString("hr-HR", timeOptions))} </h7>
                <FontAwesomeIcon icon={faMapMarker} className="mr-2 ml-2" /><h7 class="card-subtitle text-muted mb-2">{this.state.location} </h7>
                <FontAwesomeIcon icon={faCalendar} className="mr-2 ml-2" /><h7 class="card-subtitle text-muted mb-2">{(new Date(this.state.date).toLocaleDateString("hr-HR", dateOptions))} </h7>
              </div>
              <div className="card-footer bg-light text-right">
                {date < new Date() ?
                  <>
                    {this.state.organizerMembers.includes(this.state.currentUserId) ? 
                        <>
                          <a href={`/add-event-quiz-question/${this.props.match.params.quiz_id}`} className="btn btn-outline-dark btn-light mr-2" role="button">Dodaj pitanje</a>
                          <a href={`/add-quiz-results/${this.props.match.params.quiz_id}`} className="btn btn-outline-dark btn-light mr-2" role="button">Ažuriraj rezultate</a>
                        </>
                        :
                        <>
                        </>
                    }
                    <a href={`/solve-event-quiz/${this.props.match.params.quiz_id}`} className="btn btn-outline-primary btn-light col-sm-2" role="button">Riješi kviz</a>
                  </>
                  :
                  <>
                    {this.state.organizerMembers.includes(this.state.currentUserId) ?
                      <>
                      </>
                      :
                      <>
                        {this.state.has_quiz_entry==false ? 
                          (<a href={`/application-page/${this.props.match.params.quiz_id}`} className="btn btn-outline-primary btn-light col-sm-2 ml-2" role="button">Prijavi se!</a>)
                          :
                          (<button className="btn btn-outline-primary btn-light col-sm-2" role="button" onClick={this.submitCheckout}>Odjavi se!</button>)
                        }
                      </>
                    }
                  </>
                }
              </div>
            </div>  
            {/* <Calendar date={this.state.date} />
            <Picture image={this.state.image} /> */}
          </div>
          {/* <h1 className="display-2">{this.state.name}</h1>
          <Address location={this.state.location} /> */}

          {date > new Date() ? (
            <div>
              <div className="card mb-2 bg-light border-dark">
                <div className="card-body">
                  <h5 className="card-title">
                    <FontAwesomeIcon icon={faInfoCircle} className="mr-2" />Dodatne informacije
                  </h5>
                  <h6 class="card-subtitle mb-2">
                    Kotizacija: <a className="font-weight-normal">{this.state.fee} kn</a>
                  </h6>
                  <h6 class="card-subtitle mb-2">
                    Kapacitet kviza: <a className="font-weight-normal">{this.state.maxCapacity} ekipa</a>
                  </h6>
                  <h6 class="card-subtitle mb-3">
                    Broj članova u ekipi: <a className="font-weight-normal">{this.state.min_num_of_players} - {this.state.max_num_of_players}</a>
                  </h6>
                  <div className="card mb-2 border-dark">
                    <div className="card-body">
                      <h6 className="card-title">
                        Koncept
                      </h6>
                      <h7>
                        {this.state.concept}
                      </h7>
                    </div>
                  </div>
                  <div className="card mb-2 border-dark">
                    <div className="card-body">
                      <h6 className="card-title">
                        Pravila
                      </h6>
                      <h7>
                        {this.state.rules}
                      </h7>
                    </div>
                  </div>
                  <div className="card mb-2 border-dark">
                    <div className="card-body">
                      <h6 className="card-title">
                        Nagrade
                      </h6>
                      <h7>
                        {this.state.prize}
                      </h7>
                    </div>
                  </div>
                </div>
              </div>
              {/* <div className="flexbox-container">
                <RemainingSeats
                  remaining={this.state.remainingCapacity}
                  max={this.state.maxCapacity}/>
              <div className="flexbox-container">
                <MinMaxCapacity
                  min={this.state.min_num_of_players}
                  max={this.state.max_num_of_players}
                />
                <Fee fee={this.state.fee} />
                 {this.state.has_quiz_entry==false ? 
                (<a href={`/application-page/${this.props.match.params.quiz_id}`} className="btn btn-outline-dark col-sm-2" role="button">Prijavi se!</a>)
                 :
                (<button className="btn btn-outline-dark col-sm-2" role="button" onClick={this.submitCheckout}>Odjavi se!</button>)}
              </div> */}
              {/* <Information
                concept={this.state.concept}
                rules={this.state.rules}
                prize={this.state.prize}
              /> */}
            </div>
          ) : (
            <div>
              <QuizRating quizId={this.props.match.params.quiz_id}/>
              <h3>Rezultati</h3>
              <ol>
              {this.state.results &&
               this.state.results.map((team_name, index) => (
                  <li>
                 {team_name.name}
                 </li>
              ))}
              </ol>
            </div>
          )}
          <QuizComments quiz_id={this.props.match.params.quiz_id}/>
        </div>
      </div>
    );
  }
}

const Calendar = props => {
  const date = new Date(props.date);
  const day = date.toLocaleDateString("hr-HR", { day: "numeric" });
  const month = date.toLocaleDateString("hr-HR", { month: "long" });
  const time = date.toLocaleTimeString("hr-HR", {
    hour: "numeric",
    minute: "numeric"
  });
  return (
    <div className="Border">
      <h1 className="Middle">
        {day}
        <br />
        {month}
        <br />
        {time}
      </h1>
    </div>
  );
};
const Picture = props => (
  <img alt="Slika kviza" src={props.image} className="img-responsive Picture" />
);
const Address = props => <p>{props.location}</p>;
const Fee = props => <p className="col-sm-4">Kotizacija :{props.fee} kn</p>;
const MinMaxCapacity = props => (
  <p className="col-sm-6">
    Broj ljudi po ekipi : {props.min} - {props.max}
  </p>
);
const Information = props => (
  <div>
    <h3>Koncept</h3>
    <p>{props.concept}</p>
    <h3>Pravila</h3>
    <p>{props.rules}</p>
    <h3>Nagrade</h3>
    <p>{props.prize}</p>
  </div>
);

export default Quiz;
