import React from "react";
import APIService from "../services/APIService";

const apiService = new APIService();

class Login extends React.Component {
  state = {
    error: false,
    username: undefined,
    password: undefined,
    loggedIn: false
  };

  handleChange = event =>
    this.setState({ [event.target.name]: event.target.value });

  submitLogin = async event => {
    event.preventDefault();
    this.setState({ error: false, loggedIn: false });
    apiService
      .login(this.state.username, this.state.password)
      .then(() => {
        this.props.history.push("/");
        this.setState({ loggedIn: true });
      })
      .catch(error => this.setState({ error: true }));
  };

  componentDidMount() {
    apiService.logout();
  }

  render() {
    return (
      <div>
        <div className="container text-center">
          <h1>Dobrodošli u Kvizosferu</h1>
        </div>
        <div className="container col-sm-4">
          {/* Default form login */}
          <form
            className="text-center border border-light p-5"
            onSubmit={this.submitLogin}
          >
            <p className="h4 mb-4">Prijava</p>

            {/* Email */}
            <div className="row">
              Korisničko ime
              <input
                type="text"
                id="defaultLoginFormEmail"
                name="username"
                className="form-control mb-4"
                onChange={this.handleChange}
              />
            </div>

            {/* Password */}
            <div className="row">
              Lozinka
              <input
                type="password"
                id="defaultLoginFormPassword"
                name="password"
                className="form-control mb-4"
                onChange={this.handleChange}
              />
            </div>

            <div className="d-flex justify-content-around">
              <div>
                {/* Remember me */}
                <div className="custom-control custom-checkbox">
                  <input
                    type="checkbox"
                    className="custom-control-input"
                    id="defaultLoginFormRemember"
                  />
                  <label
                    className="custom-control-label"
                    htmlFor="defaultLoginFormRemember"
                  >
                    Zapamti me
                  </label>
                </div>
              </div>
              <div>
                {/* Forgot password */}
                <a href="/forgot">Zaboravili ste lozinku?</a>
              </div>
            </div>

            {/* Sign in button */}
            <button className="btn btn-info btn-block my-4" type="submit">
              Prijavi se
            </button>

            {/* Register */}
            <p>
              Niste registrirani?
              <a href="/register"> Registrirajte se</a>
            </p>
            {this.state.error && (
              <p className="text-danger">Pogrešno korisničko ime ili lozinka</p>
            )}
            {this.state.loggedIn && (
              <p className="text-success">Prijava uspješna!</p>
            )}
          </form>
        </div>
      </div>
    );
  }
}

export default Login;
