import React from "react";
import Navbar from "../../components/navbar/Navbar";
import "./explore.css";
import { Dropdown } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCalendar,
  faClock,
  faQuestionCircle,
  faMapMarker
} from "@fortawesome/free-solid-svg-icons";
import APIService from "../../services/APIService";

const apiService = new APIService();

const dateOptions = {
  weekday: "long",
  year: "numeric",
  month: "long",
  day: "numeric"
};

const timeOptions = {
  hour12: false,
  hour: "2-digit",
  minute: "2-digit"
};

class Explore extends React.Component {
  state = {
    error: false,
    quizEventsList: [],
    currentQuizEventsList: [],
    locationsList: undefined,
    categoriesList: undefined,
    organizers: [],
    organizerIds: [],
    has_quiz_entry: {},
    quizLocation: false,
    quizCategory: false,
    quizCategoryID: {1: "Kviz općeg znanja",
                     2: "Sportski kviz",
                     3: "Glazbeni kviz",
                     4: "Filmski kviz",
                     5: "Ostalo",
                     false: "Sve"
                    }
  };

  componentDidMount() {
    apiService
      .getQuizEvents()
      .then(response => {
        var locations = [];
        var categories = [];

        response.forEach(
          quizEvent => (
            apiService
              .getQuizData(quizEvent.id)
              .then(resp => {
                let id = quizEvent.id;
                let has_q_entry = this.state.has_quiz_entry;
                has_q_entry[id] = resp.has_quiz_entry;
                this.setState({ has_quiz_entry: has_q_entry });
              }),
            locations.push(quizEvent.location),
            categories.push(quizEvent.category)
          )
        )

        var uniqueLocations = [...new Set(locations)];
        var uniqueCategories = [...new Set(categories)];

        this.setState({
          quizEventsList: response,
          currentQuizEventsList: response,
          locationsList: uniqueLocations,
          categoriesList: uniqueCategories
        });
      })
      .catch(error => this.setState({ error: true }));

    apiService.getUserOrganizers()
      .then((response) => {
        this.setState({
          organizers: response
        })

        this.getOrganizerIds();
      })
      .catch(error => this.setState({ error: true }));
  }

  filterLocation = location => {
    this.setState({ quizLocation: location });
    this.filterQuizes(location, this.state.quizCategory);
  };

  filterCategory = category => {
    this.setState({ quizCategory: category });
    this.filterQuizes(this.state.quizLocation, category );
  };

  filterQuizes = (location, category) => {
    this.setState(state => {
      var filteredQuizEventsList;
      var eventList;
      if (location !== false){
        eventList = state.quizEventsList.filter(
          (quizEvent, index) => location === quizEvent.location
        );
      } else {
        eventList = state.quizEventsList;
      }
      if (category !== false){
        filteredQuizEventsList = eventList.filter(
          (quizEvent, index) => category === quizEvent.category
        );
      } else {
        filteredQuizEventsList = eventList;
      }
      return {
        currentQuizEventsList: filteredQuizEventsList
      };
    });
  };

  getOrganizerIds = () => {
    this.setState(state => {
      const organizerIds = [];
      state.organizers.map((organizer, index) => {
        organizerIds.push(organizer.id);
      })

      return {
        organizerIds: organizerIds
      };
    })
  };

  submitCheckout = async (event) => {
    event.preventDefault();
    apiService
      .deleteApplication(event.target.value)
      .then(() => {
        this.setState({ success: true });
      })
      .catch(() => this.setState({ error: true }));
    this.componentDidMount();

  };

  renderSwitch(param) {
    switch (param) {
      case 1:
        return (
          <li class="list-group-item list-group-item-warning py-2">
            <FontAwesomeIcon icon={faQuestionCircle} className="mr-2" />
            Kviz općeg znanja
          </li>
        );
      case 2:
        return (
          <li className="list-group-item list-group-item-success py-2">
            <FontAwesomeIcon icon={faQuestionCircle} className="mr-2" />
            Sportski kviz
          </li>
        );
      case 3:
        return (
          <li className="list-group-item list-group-item-info py-2">
            <FontAwesomeIcon icon={faQuestionCircle} className="mr-2" />
            Glazbeni kviz
          </li>
        );
      case 4:
        return (
          <li className="list-group-item list-group-item-danger py-2">
            <FontAwesomeIcon icon={faQuestionCircle} className="mr-2" />
            Filmski kviz
          </li>
        );
      case 5:
        return (
          <li className="list-group-item list-group-item-dark py-2">
            <FontAwesomeIcon icon={faQuestionCircle} className="mr-2" />
            Ostalo
          </li>
        );
      case false:
        return (
          <li className="list-group-item list-group-item-light py-2">
            <FontAwesomeIcon icon={faQuestionCircle} className="mr-2" />
            Sve
          </li>
        );
    }
  }

  render() {
    return (
      <div>
        <Navbar />
        <div className="container mt-3">
          <div className="col d-flex align-items-center justify-content-start pl-2 pr-2">
            <span className="font-weight-bold"> Lokacija: </span>
            <span className="ml-2 mr-4">
              <Dropdown>
                <Dropdown.Toggle variant="info" id="dropdown-basic">
                  {this.state.quizLocation ? `${this.state.quizLocation} ` : 'Sve '}
                </Dropdown.Toggle>

                <Dropdown.Menu>
                  <Dropdown.Item
                    onClick={() => this.filterLocation(false)}
                  >
                    Sve
                  </Dropdown.Item>
                  {this.state.locationsList &&
                    this.state.locationsList.map((location, index) => (
                      <Dropdown.Item
                        onClick={() => this.filterLocation(location)}
                      >
                        {location}
                      </Dropdown.Item>
                    ))}
                </Dropdown.Menu>
              </Dropdown>
            </span>
            <span className="font-weight-bold"> Tema: </span>
            <span className="ml-2">
              <Dropdown>
                <Dropdown.Toggle variant="info" id="dropdown-basic">
                  {this.state.quizCategory ? `${this.state.quizCategoryID[this.state.quizCategory]} ` : 'Sve '}
                </Dropdown.Toggle>

                <Dropdown.Menu>
                  <Dropdown.Item
                    onClick={() => this.filterCategory(false)}
                  >
                    {this.renderSwitch(false)}
                  </Dropdown.Item>
                  {this.state.categoriesList &&
                    this.state.categoriesList.map((category, index) => (
                      <Dropdown.Item
                        onClick={() => this.filterCategory(category)}
                      >
                        {this.renderSwitch(category)}
                      </Dropdown.Item>
                    ))}
                </Dropdown.Menu>
              </Dropdown>
            </span>
          </div>
        </div>
        <div className="container mt-3">
          <div className="card-deck mb-3 text-center">
            {this.state.currentQuizEventsList &&
              this.state.currentQuizEventsList
                .sort(function(a, b) {
                  let dateA = a.event_date;
                  let dateB = b.event_date;
                  return dateA > dateB ? 1 : dateA < dateB ? -1 : 0;
                })
                .map((quizEvent, index) => (
                  <div className="card mb-4 shadow-sm fixed-width">
                    <div className="card-header">
                      <a href={"/quiz/" + quizEvent.id}>
                        <h4 className="my-0 font-weight-normal font-weight-bold">
                          {quizEvent.name}
                        </h4>
                      </a>
                    </div>
                    <div className="card-body">
                      {this.renderSwitch(quizEvent.category)}
                      <h6 className="card-subtitle text-muted mt-2 mb-2">
                        <FontAwesomeIcon icon={faMapMarker} className="mr-2" />
                        {quizEvent.location}
                      </h6>
                      {quizEvent.league ? (
                        <a href={"/league/" + quizEvent.league.id}>
                          <h4 className="card-title mt-2">
                            {quizEvent.league.name}
                          </h4>
                        </a>
                      ) : (
                        <h4>Ne postoji liga</h4>
                      )}
                      <h6 className="card-subtitle text-muted mb-2">
                        <FontAwesomeIcon icon={faCalendar} className="mr-2" />
                        {new Date(quizEvent.event_date).toLocaleDateString(
                          "hr-HR",
                          dateOptions
                        )}
                      </h6>
                      <h6 className="card-subtitle text-muted mb-2">
                        <FontAwesomeIcon icon={faClock} className="mr-2" />
                        {new Date(quizEvent.event_date).toLocaleTimeString(
                          "hr-HR",
                          timeOptions
                        )}
                      </h6>
                      <table className="table table-bordered table-secondary">
                        <tbody>
                          <tr>
                            <td>Broj članova</td>
                            <td>
                              {quizEvent.min_num_of_players} -{" "}
                              {quizEvent.max_num_of_players}
                            </td>
                          </tr>
                          <tr>
                            <td>Kotizacija (po osobi)</td>
                            <td>{quizEvent.fee} kn</td>
                          </tr>
                        </tbody>
                      </table>
                      {this.state.organizerIds.includes(quizEvent.organizer) ?
                        <button className="btn btn-primary btn-block btn-lg mt-2 disabled">
                          Vi ste organizator
                        </button>
                        :
                        this.state.has_quiz_entry[quizEvent.id] ?
                        <button
                          className="btn btn-danger btn-block btn-lg active mt-2"
                          role="button"
                          onClick={this.submitCheckout}
                          value={quizEvent.id}
                        >
                          Odjavi se!
                        </button>
                        :
                        <a
                          href={"/application-page/" + quizEvent.id}
                          className="btn btn-primary btn-block btn-lg active mt-2"
                          role="button"
                        >
                          Prijavi se
                        </a>
                      }
                    </div>
                  </div>
                ))}
          </div>
        </div>
      </div>
    );
  }
}

export default Explore;
