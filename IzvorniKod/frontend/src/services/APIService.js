/* eslint-disable camelcase */
import axios from 'axios';

/**
 * Retrieves the user token from local storage
 */
const getToken = () => window.localStorage.getItem('token');

/**
 * Returns information about
 */
const authorization = () => {
  const token = getToken();
  if (token) {
    return `Bearer ${token}`;
  }
  return '';
};

const api = axios.create({
  baseURL: 'http://127.0.0.1:8000',
  headers: { Authorization: authorization() },
});

/**
 * Class used to send request to API
 */
export default class APIService {
  /**
   * Logout of user by deleting token from storage.
   */
  logout = () => {
    window.localStorage.removeItem('token');
  };

  /**
   * Cheks if user entered credentials for login
   * are valid.
   * @param {*} username User entered username
   * @param {*} password User entered password
   */
  login = async (username, password) => {
    const response = api
      .post('users/login/', {
        username,
        password,
      })
      .then((resp) => {
        this.checkResponseStatus(resp);
        const token = resp.data.access;
        this.setToken(token);
        return resp;
      });

    return response;
  };

  register = async (
    email,
    username,
    password,
    firstName,
    lastName,
    dob,
    sex,
    location,
  ) => {
    const response = api
      .post('users/register/', {
        email,
        username,
        password,
        first_name: firstName,
        last_name: lastName,
        dob,
        sex,
        location,
      })
      .then((resp) => {
        this.checkResponseStatus(resp);
        return resp;
      });

    return response;
  };

  /**
   * Gets profile data for current user.
   */
  getCurrentProfileData = async () => {
    const response = api.get("users/current/").then(resp => {
      this.checkResponseStatus(resp);
      return resp.data;
    });

    return response;
  };

  /**
   * Gets organizer data for current organizer.
   */
  getOrganizerData = async (organizerId) => {
    const path = `quiz/organizer/${organizerId}/`;
    const response = api.get(path).then((resp) => {
      this.checkResponseStatus(resp);
      return resp.data;
    });

    return response;
  };

  /**
   * Gets league data for current organizer.
   */
  getLeagueData = async (leagueId) => {
    const path = `quiz/league/${leagueId}/`;
    const response = api.get(path).then((resp) => {
      this.checkResponseStatus(resp);
      return resp.data;
    });

    return response;
  };

  /**
   * Creates organizer
   */
  createOrganizer = async (organizerName, rules, location) => {
    const response = api
      .post('/quiz/organizer/', {
        name: organizerName,
        rules,
        location,
      })
      .then((resp) => {
        this.checkResponseStatus(resp);
        return resp;
      });

    return response;
  };

  /**
   * POST request to save changed user data.
   */
  sendChangedUserData = async (
    first_name,
    last_name,
    location,
    email,
  ) => {
    const response = api
      .put('users/register/', {
        first_name,
        last_name,
        location,
        email,
        })
        .then((resp) => {
          this.checkResponseStatus(resp);
          return resp;
        });

      return response;
  };

  /**
   * POST request to save changed user data.
   */
  submitQuizResults = async (
    quiz_event,
    position,
    points,
    name,
  ) => {
    const response = api
      .post('quiz/quiz/result/', {
        quiz_event,
        position,
        points,
        name,
        })
        .then((resp) => {
          this.checkResponseStatus(resp);
          return resp;
        });

      return response;
  };

  /**
   * POST request to save changed organizer data.
   */
  editOrganizer = async (
    name,
    id,
    location,
  ) => {
    const response = api
      .put('quiz/organizer/', {
        name,
        id,
        location,
        })
        .then((resp) => {
          this.checkResponseStatus(resp);
          return resp;
        });

      return response;
  };

  /**
   * POST request to create new quiz event.
   */
  createQuizEvent = async (
    quizName,
    quizOrganizer,
    quizLeague,
    quizDate,
    quizLocation,
    quizCapacity,
    quizRangeOfPlayersMin,
    quizRangeOfPlayersMax,
    quizRegistrationFee,
    quizPrize,
    quizConcept,
    quizRules,
    quizCategory,
  ) => {
    const response = api
      .post('quiz/create-quiz/', {
        name: quizName,
        organizer: quizOrganizer,
        league: quizLeague,
        location: quizLocation,
        capacity: quizCapacity,
        fee: quizRegistrationFee,
        event_date: quizDate,
        min_num_of_players: quizRangeOfPlayersMin,
        max_num_of_players: quizRangeOfPlayersMax,
        prize: quizPrize,
        concept: quizConcept,
        rules: quizRules,
        category: quizCategory,
      })
      .then((resp) => {
        this.checkResponseStatus(resp);
        return resp;
      });

    return response;
  };

  /**
    * POST request to create a league.
    */
  createLeague = async (organizer,
    name,
    rules,
    concept,
    min_num_of_players,
    max_num_of_players,
    fee,
    location) => {
    const response = api
      .post('quiz/league/', {
        organizer,
        name,
        rules,
        concept,
        min_num_of_players,
        max_num_of_players,
        fee,
        location,
      })
      .then((resp) => {
        this.checkResponseStatus(resp);
        return resp;
      });

    return response;
  };

  /**
   * POST request to save quiz application.
   */
  sendApplication = async (
    name,
    num_of_team_members,
    quiz_event,
  ) => {
    const response = api
      .post('/quiz/entry/', {
        name,
        num_of_team_members,
        quiz_event,
      })
      .then((resp) => {
        this.checkResponseStatus(resp);
        return resp;
      });

    return response;
  };

  /**
   * POST request to delete quiz application.
   */
  deleteApplication = async (
    quiz_event,
  ) => {
    console.log("TU");
    console.log(quiz_event);
    const response = api
    .delete('quiz/entry/',{
      data: {quiz_event:quiz_event}
    })
      .then((resp) => {
        this.checkResponseStatus(resp);
        return resp;
      });

    return response;
  };

  /**
   * Gets profile data for user whose ID is represented by param userID.
   * @param {*} userID ID of an user
   */
  getProfileData = async (userID) => {
    const response = api.get(`users/profile/${userID}/`).then((resp) => {
      this.checkResponseStatus(resp);
      return resp.data;
    });

    return response;
  }

  /**
   * Returns list of all organizers where logged in user
   * is member
   */
  getUserOrganizers = async () => {
    const response = api.get('quiz/list/organizer/').then((resp) => {
      this.checkResponseStatus(resp);
      return resp.data;
    });

    return response;
  };

  /**
   * Gets quiz data.
   * @param quiz_id ID of a quiz
   */
  getQuizData = async (quiz_id) => {
    const response = api.get(`/quiz/info/${quiz_id}/`).then((resp) => {
      this.checkResponseStatus(resp);
      return resp.data;
    });

    return response;
  };

   /**
  * Gets quiz comments.
  * @param quiz_id ID of a quiz
  */
 getQuizComments = async (quiz_id) => {
   const response = api.get(`/quiz/quiz/comment/list/${quiz_id}/`).then((resp) => {
     this.checkResponseStatus(resp);
     return resp.data;
   });

   return response;
 };

 /**
   * POST request to add a comment to a quiz
   */
  addQuizComment = async (quiz, user, comment) => {
    const response = api
      .post('quiz/quiz/comment/', {
        quiz,
        user,
        comment,
      })
      .then((resp) => {
        this.checkResponseStatus(resp);
        return resp;
      });

    return response;
  };

    /**
   * DELETE request to delete comment
   */
  deleteQuizComment = async (id) => {
    const response = api
      .delete('quiz/quiz/comment/',{
        data: {id:id}
      })
      .then((resp) => {
        this.checkResponseStatus(resp);
        return resp;
      });

    return response;
  };

   /**
  * Gets organizer comments.
  * @param organizer_id ID of organizer
  */
 getOrganizerComments = async (organizer_id) => {
  const response = api.get(`quiz/organizer/comment/list/${organizer_id}/`).then((resp) => {
    this.checkResponseStatus(resp);
    return resp.data;
  });

  return response;
};

/**
  * POST request to add a comment to an organizer
  */
 addOrganizerComment = async (organizer, user, comment) => {
   const response = api
     .post('quiz/organizer/comment/', {
       organizer,
       user,
       comment,
     })
     .then((resp) => {
       this.checkResponseStatus(resp);
       return resp;
     });

   return response;
 };

   /**
  * DELETE request to delete OrganizerComment
  */
 deleteOrganizerComment = async (id) => {
   const response = api
     .delete('quiz/organizer/comment/',{
       data: {id:id}
     })
     .then((resp) => {
       this.checkResponseStatus(resp);
       return resp;
     });

   return response;
 };

  /**
   * Returns list of all upcoming quizzes for an user with userId
   */
  getUserUpcomingQuizzes = async (userId) => {
    const response = api.get(`quiz/list/quiz/upcoming/user/${userId}/`).then((resp) => {
      this.checkResponseStatus(resp);
      return resp.data;
    });

    return response;
  };

  /**
   * Returns list of all bygone quizzes for an user with userId
   */
  getUserBygoneQuizzes = async (userId) => {
    const response = api.get(`quiz/list/quiz/bygone/user/${userId}/`).then((resp) => {
      this.checkResponseStatus(resp);
      return resp.data;
    });

    return response;
  };

  /**
   * Returns list of all followers for an user with userId
   */
  getUserFollowers = async (userId) => {
    const response = api.get(`users/followers/user/${userId}/`).then((resp) => {
      this.checkResponseStatus(resp);
      return resp.data;
    });

    return response;
  };

  /*
   * Returns list of all leagues for chosen organizer where logged in user
   * is member
   */
  getOrganizerLeagues = async (organizerId) => {
    const path = `quiz/list/leagues/${organizerId}/`;
    const response = api.get(path).then((resp) => {
      this.checkResponseStatus(resp);
      return resp.data;
    });
    return response;
  };

  /**
   * Returns list of all following users for an user with userId
   */
  getUserFollowing = async (userId) => {
    const response = api.get(`users/follows/user/${userId}/`).then((resp) => {
      this.checkResponseStatus(resp);
      return resp.data;
    });

    return response;
  };

  /**
   * Returns list of all upcoming quizzes for an user with userId
   */
  getUserUpcomingQuizzes = async (userId) => {
    const response = api.get(`quiz/list/quiz/upcoming/user/${userId}/`).then(resp => {
      this.checkResponseStatus(resp);
      return resp.data;
    });

    return response;
  };

  /**
   * Returns list of all upcoming quizzes for a logged in user
   */
  getCurrentUserUpcomingQuizzes = async () => {
    const response = api.get(`quiz/list/quiz/upcoming/current/`).then(resp => {
      this.checkResponseStatus(resp);
      return resp.data;
    });

    return response;
  };

  /**
   * Returns list of all bygone quizzes for an user with userId
   */
  getUserBygoneQuizzes = async (userId) => {
    const response = api.get(`quiz/list/quiz/bygone/user/${userId}/`).then(resp => {
      this.checkResponseStatus(resp);
      return resp.data;
    });

    return response;
  };

  /**
   * Returns list of all bygone quizzes for a logged in user
   */
  getCurrentUserBygoneQuizzes = async () => {
    const response = api.get(`quiz/list/quiz/bygone/current/`).then(resp => {
      this.checkResponseStatus(resp);
      return resp.data;
    });

    return response;
  };

  /**
   * Returns list of all followers for an user with userId
   */
  getUserFollowers = async (userId) => {
    const response = api.get(`users/followers/user/${userId}/`).then(resp => {
      this.checkResponseStatus(resp);
      return resp.data;
    });

    return response;
  };

  /**
   * Returns list of all following users for an user with userId
   */
  getUserFollowing = async (userId) => {
    const response = api.get(`users/follows/user/${userId}/`).then(resp => {
      this.checkResponseStatus(resp);
      return resp.data;
    });

    return response;
  };

  /**
   * Returns list of all quiz events that haven't happened yet.
   */
  getQuizEvents = async () => {
    const response = api.get('quiz/list/quiz/upcoming/').then((resp) => {
      this.checkResponseStatus(resp);
      return resp.data;
    });

    return response;
  };


  /**
   * Returns list of all quiz events that have happened.
   */
  getBygoneQuizEvents = async () => {
    const response = api.get('quiz/list/quiz/bygone/').then((resp) => {
      this.checkResponseStatus(resp);
      return resp.data;
    });

    return response;
  };


  /**
   * Returns list of all registered users.
   */
  getAllUsers = async () => {
    const response = api.get('users/list/all/').then((resp) => {
      this.checkResponseStatus(resp);
      return resp.data;
    });

    return response;
  };

  /**
   * POST request to add user as a member to organizer.
   */
  addMemberToOrganizer = async (username, organizer) => {
    const response = api
      .post('quiz/organizer/member/', {
        user: {
          username,
        },
        organizer,
      })
      .then((resp) => {
        this.checkResponseStatus(resp);
        return resp;
      });

    return response;
  };

  /**
   * POST request to change user role.
   */
  changeUserRole = async (user, role) => {
    const response = api
      .put('users/role/', {
        user,
        role,
      })
      .then((resp) => {
        this.checkResponseStatus(resp);
        return resp;
      });

    return response;
  };

  /**
   * Gets average quiz rating.
   * @param quiz_id ID of a quiz
   */
  getAverageQuizRating = async (quiz_id) => {
    const response = api.get(`/quiz/quiz/grade/average/${quiz_id}/`).then((resp) => {
      this.checkResponseStatus(resp);
      return resp.data;
    });

    return response;
  }

  /**
   * POST request to add quiz rating from user.
   */
  addQuizRating = async (quiz, user, grade) => {
    const response = api
      .post('quiz/quiz/grade/', {
        quiz,
        user,
        grade,
      })
      .then((resp) => {
        this.checkResponseStatus(resp);
        return resp;
      });

    return response;
  }

    /**
   * Gets average organizer rating.
   * @param organizerID ID of an organizer
   */
  getAverageOrganizerRating = async (organizerID) => {
    const response = api.get(`quiz/organizer/grade/average/${organizerID}/`).then((resp) => {
      this.checkResponseStatus(resp);
      return resp.data;
    });

    return response;
  }

  /**
   * POST request to add organizer rating from user.
   */
  addOrganizerRating = async (organizer,user,grade) => {
    const response = api
      .post('quiz/organizer/grade/', {
        organizer,
        user,
        grade,
      })
      .then((resp) => {
        this.checkResponseStatus(resp);
        return resp;
      });

    return response;
  }

  /**
   * POST request to follow an user.
   */
  follow = async (userID) => {
    const response = api
      .post('users/follow/', {
        follows: userID,
      })
      .then((resp) => {
        this.checkResponseStatus(resp);
        return resp;
      });

    return response;
  };

  /**
   * POST request to add an question to trial quiz.
   */
  submitTrialQuizQuestion = async (organizer, question_number, question, answer) => {
    const response = api
      .post('quiz/question/trial/add/', {
        organizer: organizer,
        question_number: question_number,
        question: question,
        answer: answer,
      })
      .then((resp) => {
        this.checkResponseStatus(resp);
        return resp;
    });

    return response;
  };

  /**
   * POST request to add an question to quiz event.
   */
  submitEventQuizQuestion = async (quiz, question_number, question, answer) => {
    const response = api
      .post('quiz/question/add/', {
        quiz: quiz,
        question_number: question_number,
        question: question,
        answer: answer,
      })
      .then((resp) => {
        this.checkResponseStatus(resp);
        return resp;
    });

    return response;
  };

  /**
   * Returns list of all questions for trial quiz with param @organizerId
   */
  getTrialQuizQuestions = async (organizerId) => {
    const response = api.get(`quiz/questions/trial/list/${organizerId}/`).then((resp) => {
      this.checkResponseStatus(resp);
      return resp.data;
    });

    return response;
  };

  /**
   * Returns list of all questions for quiz event with param @quizId
   */
  getEventQuizQuestions = async (quizId) => {
    const response = api.get(`quiz/questions/list/${quizId}/`).then((resp) => {
      this.checkResponseStatus(resp);
      return resp.data;
    });

    return response;
  };

  /**
   * Returns list of all results for quiz event with param @quizId
   */
  getQuizResults = async (quizId) => {
    const response = api.get(`quiz/quiz/result/${quizId}/`).then((resp) => {
      this.checkResponseStatus(resp);
      return resp.data;
    });

    return response;
  };

  /**
   * POST request to send message to an user.
   */
  sendMessage = async (userID, msg) => {
    const response = api
      .post('users/msg/send/', {
        reciver: userID,
        message: msg,
      })
      .then((resp) => {
        this.checkResponseStatus(resp);
        return resp;
      });

    return response;
  };

  /**
   * Returns list of all message participants for current user
   */
  getMessageParticipants = async () => {
    const response = api.get('users/msg/participants/').then((resp) => {
      this.checkResponseStatus(resp);
      return resp.data;
    });

    return response;
  };

  /**
   * Returns list of all messages in a conversation with an user with userId
   */
  getConversationMessages = async (userId) => {
    const response = api.get(`users/msg/conversation/${userId}/`).then((resp) => {
      this.checkResponseStatus(resp);
      return resp.data;
    });

    return response;
  };

  /**
   * DELETE request to delete current user
   */
  deleteUser = async () => {
    const response = api
      .delete('users/current/')
      .then((resp) => {
        this.checkResponseStatus(resp);
        return resp;
      });

    return response;
  };

  /**
   * DELETE request to delete organizer profile
   */
  deleteOrganizer = async (id) => {
    const response = api
      .delete('quiz/organizer/',{
        data: {id:id}
      })
      .then((resp) => {
        this.checkResponseStatus(resp);
        return resp;
      });

    return response;
  };

  /**
   * Returns organizer info
   */
  getOrganizerInfo = async (organizerID) => {
    const response = api.get(`quiz/organizer/${organizerID}/`).then((resp) => {
      this.checkResponseStatus(resp);
      return resp.data;
    });
    return response;
  };
  /**
   * Returns organizer members
   */
  getOrganizerMembers = async (organizerID) => {
    const response = api.get(`quiz/list/organizer/${organizerID}/members/`).then((resp) => {
      this.checkResponseStatus(resp);
      return resp.data;
    });
    return response;
  };
  /**
   * Returns organizer past quizes
   */
  getOrganizerPastQuizes = async (organizerID) => {
    const response = api.get(`quiz/list/organizer/${organizerID}/past_quizes/`).then((resp) => {
      this.checkResponseStatus(resp);
      return resp.data;
    });
    return response;
  };
  /**
   * Returns organizer future quizes
   */
  getOrganizerFutureQuizes = async (organizerID) => {
    const response = api.get(`quiz/list/organizer/${organizerID}/future_quizes/`).then((resp) => {
      this.checkResponseStatus(resp);
      return resp.data;
    });
    return response;
  };
  /**
   * Returns organizer followers
   */
  getOrganizerFollowers = async (organizerID) => {
    const response = api.get(`users/followers/organizer/${organizerID}/`).then((resp) => {
      this.checkResponseStatus(resp);
      return resp.data;
    });
    return response;
  };
  /**
   * Follows organizer
   */
  followOrganizer = async (organizerID) => {
    const response = api.post('users/follow/organizer/', {
      organizer: organizerID
    }).then((resp) => {
      this.checkResponseStatus(resp);
      return resp.data;
    });
    return response;
  };

  /**
   * Get user info
   */
  getUserInfo = async (userID) => {
    const response = api.get(`users/profile/${userID}/`).then((resp) => {
      this.checkResponseStatus(resp);
      return resp.data;
    });
    return response;
  };

  /**
   * Get league info
   */
  getLeagueInfo = async (leagueID) => {
    const response = api.get(`quiz/league/${leagueID}/`).then((resp) => {
      this.checkResponseStatus(resp);
      return resp.data;
    });
    return response;
  };

  /**
   * Returns organizer leagues
   */
  getOrganizerLeagues = async (organizerID) => {
    const response = api.get(`quiz/list/leagues/${organizerID}/`).then((resp) => {
      this.checkResponseStatus(resp);
      return resp.data;
    });
    return response;
  };

  /**
   * POST request to unfollow an user.
   */
  unfollow = async (userID) => {
    const response = api
      .delete('users/unfollow/', {
        data: {follows: userID}
      })
      .then((resp) => {
        this.checkResponseStatus(resp);
        return resp;
      });

    return response;
  };

  /**
   * Unfollows organizer
   */
  unfollowOrganizer = async (organizerID) => {
    const response = api.delete('users/unfollow/organizer/', {
      data : {organizer: organizerID}
    }).then((resp) => {
      this.checkResponseStatus(resp);
      return resp.data;
    });
    return response;
  };

  /**
   * Checks if HTTP response status is in success code
   * range (200-299)
   * @param {*} response HTTP response to check
   */
  checkResponseStatus(response) {
    if (response.status >= 200 && response.status < 300) {
      if (response.status === 204) {
        // response code 204 returns no content
        return undefined;
      }
      return response;
    }
    const error = new Error(response.statusText);
    error.response = response;
    throw error;
  }

  /**
   * Checks if there is a saved token and if it is still
   * valid. Returns true if there is valid token.
   */
  loggedIn() {
    const token = getToken();
    return !!token;
  }

  /**
   * Saves user token to local storage
   * @param {string} accessToken
   */
  setToken(accessToken) {
    window.localStorage.setItem('token', accessToken);
  }

  /**
   * Removes user token to local storage
   * @param {string} accessToken
   */
  removeToken(accessToken) {
    window.localStorage.removeItem('token', accessToken);
  }
}
