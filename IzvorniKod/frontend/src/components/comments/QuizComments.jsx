import React from "react";
import APIService from "../../services/APIService";

const apiService = new APIService();

class QuizComments extends React.Component {
  state = {
    input: "",
    comments: [],
    error: false,
    userID: "",
    userRole: ""
  };

  componentDidMount() {
    apiService
      .getQuizComments(this.props.quiz_id)
      .then(response => {
        this.setState({
          comments: response
        });
      })
      .catch(error =>
        this.setState({
          error: true
        })
      );

    apiService
      .getCurrentProfileData()
      .then(response => {
        this.setState({
          userID: response.user.id,
          userRole: response.role
        });
      })
      .catch(error =>
        this.setState({
          error: true
        })
      );
  }

  handleSubmit = event => {
    if (this.state.input != "") {
      event.preventDefault();

      apiService
        .addQuizComment(this.props.quiz_id, this.state.userID, this.state.input)
        .then(() => {
          apiService
            .getQuizComments(this.props.quiz_id)
            .then(response => {
              this.setState({
                comments: response,
                input: ""
              });
            })
            .catch(error =>
              this.setState({
                error: true
              })
            );
        });
    }
  };

  handleChange = e => {
    this.setState({
      input: e.target.value
    });
  };

  deleteComment = i => {
    apiService
      .deleteQuizComment(i.id)
      .then(resp => {
        apiService.getQuizComments(this.props.quiz_id).then(response => {
          this.setState({
            comments: response
          });
        });
      })
      .catch(error => this.setState({ error: true }));
  };

  render() {
    const isError = this.state.error;

    const com = this.state.comments.map(i => (
      <li className="list-group-item" key={i.id}>
        <h4 className="list-group-item-headingo">{i.user.username}</h4>
        <p className="list-group-item-text">{i.comment}</p>

        <div className="d-flex justify-content-end">
          {(this.state.userID == i.user.id || this.state.userRole == 2 || this.state.userRole == 3) && (
            <div className="p-2">
              <button
                type="button"
                className="btn btn-outline-dark btn-sm"
                onClick={() => this.deleteComment(i)}
              >
                Obriši komentar
              </button>
            </div>
          )}
          <div className="p-2">
            <small className="h-100">
              {new Date(i.comment_date).toLocaleDateString("hr-HR", {
                day: "numeric",
                month: "long",
                year: "numeric",
                hour: "2-digit",
                minute: "2-digit"
              })}
            </small>
          </div>
        </div>
      </li>
    ));
    return (
      <div>
        {isError ? (
          <div>
            <h1 className="text-center">Došlo je do problema!</h1>
          </div>
        ) : (
          <div className="container">
            <div className="row">
              <h3>Komentari</h3>
              <textarea
                className="form-control w-100"
                rows="5"
                onChange={this.handleChange}
                value={this.state.input}
                placeholder="Vaš komentar"
              />
            </div>

            <div className="row justify-content-end">
              <button
                type="button"
                className="btn btn-outline-dark mx-1 my-2"
                onClick={this.handleSubmit}
              >
                Komentiraj
              </button>
            </div>

            <div className="row">
              <ul className="list-group w-100">{com}</ul>
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default QuizComments;
