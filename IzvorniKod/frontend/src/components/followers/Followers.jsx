import React, { useState } from 'react';
import { Modal, Button, ModalHeader, ModalTitle, ModalBody, ModalFooter } from 'reactstrap';
import APIService from '../../services/APIService';

const Followers = (props) => {
  const {
    buttonLabel,
    className
  } = props;

  const [modal, setModal] = useState(false);

  const toggle = () => setModal(!modal);

  return (
    <>
        {props.myProfile === true ? 
          <button type="button" className="btn btn-outline-primary ml-2" onClick={toggle}>Tko me prati?</button>
          :
          <>
            {props.sex === "M" ? 
              <button type="button" className="btn btn-outline-primary ml-2" onClick={toggle}>Tko ga prati?</button>
              :
              <button type="button" className="btn btn-outline-primary ml-2" onClick={toggle}>Tko ju prati?</button>
            }
          </>
        }
        <Modal isOpen={modal} toggle={toggle} className={className}>
        {props.myProfile === true ?
          <ModalHeader toggle={toggle}>Tko me prati?</ModalHeader>
          :
          <>
            {props.sex === "M" ?
                <ModalHeader toggle={toggle}>Tko ga prati?</ModalHeader>
                :
                <ModalHeader toggle={toggle}>Tko ju prati?</ModalHeader>
            }   
          </>
        }
        <ModalBody>
        {props.followers &&
          props.followers.map((user, index) => (
            <a href={'/profile/' + user.user.id}>{user.user.username}<br /></a>
        ))}
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={toggle}>Povratak</Button>
        </ModalFooter>
      </Modal>
    </>
  );
}

export default Followers;