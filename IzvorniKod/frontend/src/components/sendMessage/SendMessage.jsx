import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faComments, faCalendar, faClock } from '@fortawesome/free-solid-svg-icons';
import { Modal, Form, FormGroup, FormLabel, FormControl, ModalHeader, ModalTitle, ModalBody, ModalFooter, Button } from 'reactstrap';
import APIService from '../../services/APIService';

const apiService = new APIService();

const SendMessage = (props) => {

  const {
    buttonLabel,
    className
  } = props;

  const [modal, setModal] = useState(false);

  const toggle = () => setModal(!modal);

  const send = async (event) => {
    event.preventDefault();
    apiService.
      sendMessage(props.id, document.getElementById("msg-area").value)
      .then(() => {
            document.getElementById("msg-area").value = "";
      })
      .catch((error) => {
      });
  }

  return (
    <>
      <button type="button" className="btn btn-outline-primary" onClick={toggle}>
        <span>Poruka</span>
        <FontAwesomeIcon icon={faComments} className="ml-2" />
      </button>
      <Modal isOpen={modal} toggle={toggle} className={className}>
        <ModalHeader toggle={toggle}>Pošalji poruku korisniku {props.username}</ModalHeader>
            <form className="text-center border p-5" onSubmit={send}>
                <ModalBody>
                    <div className="row">
                        Tekst poruke:
                        <textarea rows="10" cols="150" name="msg" id="msg-area"></textarea>
                    </div>
                </ModalBody>
                <ModalFooter>
                    <Button color="secondary" onClick={toggle}>Odustani</Button>
                    <Button color="primary" type="submit">Pošalji</Button>
                </ModalFooter>
            </form>
     </Modal>
    </>
  );
}

export default SendMessage;