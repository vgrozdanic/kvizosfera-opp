import React from "react";
import Logo from "../../assets/kvizosfera_logo.png";
import "./navbar.css";
import Octicon, {
  Person,
  Bell,
  CommentDiscussion
} from "@primer/octicons-react";
import APIService from '../../services/APIService';

const apiService = new APIService();

class Navbar extends React.Component {
  logout = event => {
    event.preventDefault();
    window.localStorage.removeItem("token");
    window.location.replace("/login");
  };

  state = {
    error: false,
    currentUserRole: undefined,
  }

  componentDidMount() {
    apiService
    .getCurrentProfileData()
    .then((response) => {
      this.setState({
          currentUserRole: response.role
      });
    })
    .catch((error) => this.setState({ error: true }));
  }

  render() {
    return (
      <div>
        <nav className="navbar navbar-expand-sm quizColor tekstMargina ">
          <div className="col-sm-2">
            <a className="navbar-brand" href="/">
              <img src={Logo} className="img-fluid logo" alt="Logo" />
            </a>
          </div>
          <ul className="navbar-nav ml-4">
            <li className="nav-item">
              <a className="nav-link white-color" href="/explore">
                Pronađi kviz
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link white-color" href="/organize">
                Organiziraj kviz
              </a>
            </li>
            {this.state.currentUserRole === 3 ?
              <li className="nav-item">
                <a className="nav-link white-color" href="/change-user-role">
                  Uredi uloge korisnika
                </a>
              </li>
              :
              <div>

              </div>
            }
          </ul>
          <div className="form-inline my-2 my-lg-0 ml-auto"></div>
          <div className="ml-3 nav-item ikoneMargina">
            {/* <a className="white-color ml-2" href="/notifications">
              <Octicon size="medium" icon={Bell} />
            </a> */}
            <a className="white-color ml-2" href="/messages">
              <Octicon size="medium" icon={CommentDiscussion} />
            </a>
            <a className="caret white-color ml-2" href="/profile">
              <Octicon size="medium" icon={Person} />
            </a>
            <button onClick={this.logout} className="btn btnOdjava btn-outline-secondary ml-2 mb-3">
              Odjava
            </button>
          </div>
        </nav>
      </div>
    );
  }
}

export default Navbar;
