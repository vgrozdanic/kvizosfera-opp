import React, { useState } from 'react';
import { Modal, Button, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import APIService from '../../services/APIService';

const QuizResult = (props) => {
  const {
    buttonLabel,
    className
  } = props;

  const [modal, setModal] = useState(false);

  const toggle = () => setModal(!modal);

  return (
    <>
      <button type="button" className="btn btn-outline-dark btn-primary text-light mb-2 ml-5 mr-5" onClick={toggle}>Ocijeni odgovore!</button> 
      <Modal isOpen={modal} toggle={toggle} className={className}>
        <ModalHeader toggle={toggle}>Moj rezultat</ModalHeader>
        <ModalBody>
            {console.log(props)}
            <li className="list-group-item list-group-item-success font-weight-bold text-dark mt-1">Točno odgovoreno: {props.score}</li>
            <li className="list-group-item list-group-item-warning font-weight-bold text-dark mt-1">Ukupno pitanja: {props.questions}</li>
            <li className="list-group-item list-group-item-info font-weight-bold text-dark mt-1">Postotak riješenosti: {(props.score / props.questions * 100).toFixed(2)}%</li>
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={toggle}>OK</Button>
        </ModalFooter>
      </Modal>
    </>
  );
}

export default QuizResult;