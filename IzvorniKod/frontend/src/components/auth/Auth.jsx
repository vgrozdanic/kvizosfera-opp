import React from 'react';

export function requireAuthentication(Component) {
  return class AuthenticatedComponent extends React.Component {
    isAuthenticated() {
      // TODO (vgrozdanic): make better checking of token
      return window.localStorage.getItem('token') !== null;
    }

    render() {
      if (!this.isAuthenticated()) {
        this.props.history.push('/login');
      }

      return (
        <div>
          <Component {...this.props} />
        </div>
      );
    }
  };
}

export default requireAuthentication;
