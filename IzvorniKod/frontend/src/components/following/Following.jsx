import React, { useState } from 'react';
import { Modal, Button, ModalHeader, ModalTitle, ModalBody, ModalFooter } from 'reactstrap';
import APIService from '../../services/APIService';

const Following = (props) => {
  const {
    buttonLabel,
    className
  } = props;

  const [modal, setModal] = useState(false);

  const toggle = () => setModal(!modal);

  return (
    <>
      {props.myProfile ? 
        <button type="button" className="btn btn-outline-primary ml-2" onClick={toggle}>Koga pratim?</button> 
        :
        <button type="button" className="btn btn-outline-primary ml-2" onClick={toggle}>Koga prati?</button> 
      }
      <Modal isOpen={modal} toggle={toggle} className={className}>
        {props.myProfile ?
          <ModalHeader toggle={toggle}>Koga pratim?</ModalHeader>
          :
          <ModalHeader toggle={toggle}>Koga prati?</ModalHeader>
        }
        <ModalBody>
        {props.following &&
          props.following.map((user, index) => (
            <a href={'/profile/' + user.follows.id}>{user.follows.username}<br /></a>
        ))}
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={toggle}>Povratak</Button>
        </ModalFooter>
      </Modal>
    </>
  );
}

export default Following;