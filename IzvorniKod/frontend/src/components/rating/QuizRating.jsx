/* eslint-disable no-mixed-spaces-and-tabs */
/* eslint-disable no-tabs */
import React from 'react';
import APIService from '../../services/APIService';

const apiService = new APIService();

class QuizRating extends React.Component {
	state = {
	  quiz_id: undefined,
	  user: undefined,
	  grade: '1',				
	  averageGrade: '1',
	  error: undefined,
	  success: undefined,
	};

	handleChange = (event) => {
	  this.setState({ [event.target.name]: event.target.value} );
	}

	addRating = (event) => {
		event.preventDefault();
		this.setState({ error: false, success: false } );
		apiService
	    .addQuizRating(
	    	this.state.quiz_id,
	    	this.state.user,
	    	this.state.grade,
	    	)
	    .then(() => {
			apiService.getAverageQuizRating(this.props.quizId)
			.then((response) => {
				this.setState({
				averageGrade: response.avg_grade,
			});
		  	this.setState({ success: true });
			})
		.catch(() => {
			this.setState({ error: true });				
			});
		})
	    .catch(() => {
	    	this.setState({ error: true });				
		});
	}

	componentDidMount() {
		this.setState({
	    quiz_id: this.props.quizId,
		});
		apiService.getCurrentProfileData()
	    .then((response) => {
	    	this.setState({
	        user: response.user.id,
	    	});
	    })
	    .catch((error) => this.setState({ error: true }));
		apiService.getAverageQuizRating(this.props.quizId)
	    .then((response) => {
	    	this.setState({
	        averageGrade: response.avg_grade,
	    	});
	    }).catch((error) => this.setState({ error: true }));
	}

	render() {
		return (
  		<div className="row mt-2 ">
    		<div className="col mt-3"><h4>Ocijeni kviz:</h4></div>
    		<div className="col">
      			<select className="form-control mb-4 mt-2" name="grade" onChange={this.handleChange}>
        		<option value="1">1</option>
        		<option value="2">2</option>
        		<option value="3">3</option>
        		<option value="4">4</option>
        		<option value="5">5</option>
      			</select>
    		</div>
    		<div className="col text-left mt-2">
      			<button className="btn btn-outline-dark" type="button" onClick={this.addRating}>Ocijeni</button>
    		</div>
    		<div className="col">
      			<div className="row mt-3">
					{this.state.averageGrade && (<p>Prosječna ocjena: {this.state.averageGrade}/5 </p>)}
      			</div>
      			<div className="row">
        		{this.state.success && (<p className="text-success">Vaša ocjena:{this.state.grade}/5 </p>)}
				</div>
    		</div>
			{this.state.error && (<p className="text-danger">Pogreška</p>)}
  		</div>
	    );
	}
}
export default QuizRating;
