/* eslint-disable no-mixed-spaces-and-tabs */
/* eslint-disable no-tabs */
import React from 'react';
import APIService from '../../services/APIService';

const apiService = new APIService();

class OrganizerRating extends React.Component {
	state = {
	  organizer_id: undefined,
	  user: undefined,
	  grade: '1',				
	  averageGrade: '1',
	  error: undefined,
	  success: undefined,
	};

	handleChange = (event) => {
	  this.setState({ [event.target.name]: event.target.value} );
	}

	addRating = (event) => {
		event.preventDefault();
		console.log(this.state.organizer_id);
		console.log(this.state.user);
		console.log(this.state.grade);
		this.setState({ error: false, success: false } );
		apiService
	    .addOrganizerRating(
	    	this.state.organizer_id,
	    	this.state.user,
	    	this.state.grade,
	    	)
	    .then(() => {
			apiService.getAverageOrganizerRating(this.props.organizer_id) 
			.then((response) => {
				this.setState({
				averageGrade: response.avg_grade,
			});
		  	this.setState({ success: true });
			})
		.catch(() => {
			this.setState({ error: true });			
			});
		})
	    .catch(() => {
			this.setState({ error: true });		
		});
	}

	componentDidMount() {
		this.setState({
	    organizer_id: this.props.organizer_id,
		});
		apiService.getCurrentProfileData()
	    .then((response) => {
	    	this.setState({
	        user: response.user.id,
	    	});
	    })
	    .catch((error) => this.setState({ error: true }));
		apiService.getAverageOrganizerRating(this.props.organizer_id)
	    .then((response) => {
	    	this.setState({
	        averageGrade: response.avg_grade,
	    	});
	    }).catch((error) => this.setState({ error: true }));
	}

	render() {
		return (
  			<div>
              <div className="card mt-2">
                <div className="card-body">
                  <h5 className="card-title text-center ">Ocijeni organizatora</h5>
				  <select className="form-control mb-2" name="grade" onChange={this.handleChange}>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
				  </select>
				  <div className="card-text text-center">
				  	<button className="btn btn-outline-primary" type="button" onClick={this.addRating}>Ocijeni</button>
				  </div>
				  <p className="card-text text-center mt-1">Prosječna ocjena:{this.state.averageGrade && <a> {this.state.averageGrade}/5</a>}</p>
				  {this.state.success && (<p className="card-text text-success mt-1">Vaša ocjena: {this.state.grade}/5 </p>)}
				  {this.state.error && (<p className="card-text text-danger mt-1">Pogreška</p>)}
				</div>
              </div>
  			</div>
	    );
	}
}
export default OrganizerRating;
