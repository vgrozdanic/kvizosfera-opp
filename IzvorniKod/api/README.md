# BACKEND

### Starting API in debug mode

- To start API in debug mode first you will need to stop _api_service_ container by executing: `docker-compose stop api_service`. After you have stopped container, run it in debug mode: `docker-compose run --rm --service-ports api_service`

### Creating django superuser

- To create djagno superuser execute: `docker exec -it api_service python manage.py createsuperuser`
