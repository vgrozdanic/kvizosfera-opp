Django==2.2.6
djangorestframework==3.10.3
pytz==2019.3
sqlparse==0.3.0
psycopg2==2.8.3
djangorestframework_simplejwt==4.3.0
django-cors-headers==3.1.1
