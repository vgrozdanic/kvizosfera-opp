from rest_framework.views import APIView
from django.db.models import Q
from rest_framework.response import Response
from rest_framework import authentication, permissions
from django.contrib.auth.models import User
from rest_framework_simplejwt.views import TokenObtainPairView
from .serializers import (
    LoginSerializer,
    RegisterSerializer,
    UserProfileSerializer,
    FollowUserSerializer,
    FollowOrganizerSerializer,
    UserFollowersSerializer,
    UserFollowsSerializer,
    UserFollowsOrganizerSerializer,
    OrganizerFollowersSerializer,
    MessageParticipantsSerializer,
    MessageConversationSerializer,
    SendMessageSerializer,
    UpdateUserProfileSerializer,
    UpdateUserRoleSerializer
)
from django.shortcuts import (
    render,
    get_object_or_404
)
from rest_framework.response import Response
from rest_framework import status
# from .permissions import AllowOnlyPost
from .models import (
    CustomUser,
    FollowUser,
    FollowOrganizer,
    Message
)


class UserLogin(APIView):
    """
    Checks if user entered credentials are valid.
    If everythings is valid it returns JWT token, otherwise
    it returns error status code.
    """
    # permission_classes = [AllowOnlyPost]

    def post(self, request):
        serializer = LoginSerializer(data=request.data, context={'request': request})

        return Response(
            serializer.validate(data=request.data),
            status=status.HTTP_200_OK
            )


class UserRegister(APIView):
    """
    Checks if data is valid, if everything is ok, it creates new user.
    """

    def post(self, request):
        serializer = RegisterSerializer(data=request.data, context={'request': request})

        if serializer.is_valid():
            serializer.save()
            return Response({"msg": "Success"}, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request):
        """
        PUT request to update user information
        """
        if request.user.is_anonymous:
            return Response({"msg": "User is not logged in"}, status=status.HTTP_403_FORBIDDEN)

        user = request.user
        data = request.data
        data["id"] = user.id
        print(data)

        serializer = UpdateUserProfileSerializer(data=data, context={'request': request})

        if serializer.is_valid():
            serializer.save()
            return Response({"msg": "Success"}, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class CurrentUser(APIView):
    """
    Returns information about current user
    """

    def get(self, request):
        user = request.user
        profile = CustomUser.objects.get(user=user)
        serializer = UserProfileSerializer(profile)
        return Response(serializer.data)

    def delete(self, request):
        user = request.user
        if request.user.is_anonymous:
            return Response({"msg": "User is not logged in"}, status=status.HTTP_403_FORBIDDEN)

        CustomUser.objects.filter(user=user.id).delete()
        User.objects.filter(id=user.id).delete()

        return Response({"msg": "Success"}, status=status.HTTP_200_OK)


class GetUserProfile(APIView):
    """
    Returns information about requested user
    """

    def get(self, request, user_id):
        user = request.user
        profile = CustomUser.objects.get(user=user_id)
        serializer = UserProfileSerializer(profile)
        data = serializer.data
        data["following_user"] = False

        try:
            FollowUser.objects.get(user=user.id, follows=user_id)
            data["following_user"] = True
        except:
            # There is no such entry in table
            pass

        return Response(data)

class FollowUserView(APIView):
    """
    API view for following user.
    """
    def post(self, request):
        user = request.user
        data = request.data
        data["user"] = user.id

        serializer = FollowUserSerializer(data=data, context={'request': request})

        if serializer.is_valid():
            serializer.save()
            return Response({"msg": "Success"}, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class FollowOrganizerView(APIView):
    """
    API view for following organizer.
    """
    def post(self, request):
        user = request.user
        data = request.data
        data["user"] = user.id

        serializer = FollowOrganizerSerializer(data=data, context={'request': request})

        if serializer.is_valid():
            serializer.save()
            return Response({"msg": "Success"}, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class GetUserFollowers(APIView):
    """
    Returns list of all user followers
    """

    def get(self, request, user_id):
        profile = FollowUser.objects.filter(follows=user_id)
        serializer = UserFollowersSerializer(profile, many=True)
        return Response(serializer.data)

class GetUserFollows(APIView):
    """
    Returns list of all users that user follows followers
    """

    def get(self, request, user_id):
        profile = FollowUser.objects.filter(user=user_id)
        serializer = UserFollowsSerializer(profile, many=True)
        return Response(serializer.data)

class GetOrganizerFollowers(APIView):
    """
    Returns list of all organizer followers
    """

    def get(self, request, organizer_id):
        profile = FollowOrganizer.objects.filter(organizer=organizer_id)
        serializer = OrganizerFollowersSerializer(profile, many=True)
        return Response(serializer.data)

class GetUserFollowsOrganizer(APIView):
    """
    Returns list of all organizers that user follows followers
    """

    def get(self, request, user_id):
        profile = FollowOrganizer.objects.filter(user=user_id)
        serializer = UserFollowsOrganizerSerializer(profile, many=True)
        return Response(serializer.data)

class GetMsgParticipants(APIView):
    """
    Returns list of all participants in message conversations for current user
    """
    def get(self, request):
        if request.user.is_anonymous:
            return Response({"msg": "User is not logged in"}, status=status.HTTP_403_FORBIDDEN)

        user = request.user
        participants = Message.objects.filter(Q(sender=user) | Q(reciver=user)).order_by('-msg_date')
        serializer = MessageParticipantsSerializer(participants, many=True)

        # return only unique usernames as participants
        data = []
        list_of_usernames = {}
        for m in serializer.data:
            m_id = m["sender"]["id"]
            m_username = m["sender"]["username"]
            m_date = m["msg_date"]

            if m_id == user.id:
                m_id = m["reciver"]["id"]
                m_username = m["reciver"]["username"]

            if m_id in list_of_usernames:
                continue

            list_of_usernames[m_id] = m_username

            data.append({"username": m_username, "id": m_id, "msg_date": m_date})


        return Response(data)

class GetMsgConversation(APIView):
    """
    Returns list of all messages in conversations for current user and specific user
    """
    def get(self, request, user_id):
        if request.user.is_anonymous:
            return Response({"msg": "User is not logged in"}, status=status.HTTP_403_FORBIDDEN)

        user = request.user
        participants = Message.objects.filter(
            Q(sender=user, reciver=user_id) | Q(sender=user_id, reciver=user)
            ).order_by('msg_date')
        serializer = MessageConversationSerializer(participants, many=True)

        return Response(serializer.data)

class SendMessage(APIView):
    """
    API view for sending message.
    """
    def post(self, request):
        if request.user.is_anonymous:
            return Response({"msg": "User is not logged in"}, status=status.HTTP_403_FORBIDDEN)

        user = request.user
        data = request.data
        data["sender"] = user.id

        serializer = SendMessageSerializer(data=data, context={'request': request})

        if serializer.is_valid():
            serializer.save()
            return Response({"msg": "Success"}, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ChangeRoleView(APIView):
    """
    API view to change user role
    """

    def put(self, request):
        """
        PUT request to update user information about role
        """
        if request.user.is_anonymous:
            return Response({"msg": "User is not logged in"}, status=status.HTTP_403_FORBIDDEN)

        user = get_object_or_404(CustomUser, user=request.user.id)

        if user.role != 3:
            return Response({"msg": "User does not have rights"}, status=status.HTTP_403_FORBIDDEN)



        try:
            data = request.data
            user = data.get("user")
            role = data.get("role")

            CustomUser.objects.filter(user=user).update(
                role=role
            )
            return Response({"msg": "Success"}, status=status.HTTP_201_CREATED)
        except:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ListAllUsers(APIView):
    """
    API endpoint to list all users
    """
    def get(self, request):
        profile = CustomUser.objects.all()
        serializer = UserProfileSerializer(profile, many=True)
        return Response(serializer.data)

class UnfollowUserView(APIView):
    """
    API view for unfollowing organizer.
    """
    def delete(self, request):
        user = request.user
        if request.user.is_anonymous:
            return Response({"msg": "User is not logged in"}, status=status.HTTP_403_FORBIDDEN)

        FollowUser.objects.filter(user=request.user.id, follows=request.data['follows']).delete()

        return Response({"msg": "Success"}, status=status.HTTP_200_OK)

class UnfollowOrganizerView(APIView):
    """
    API view for unfollowing organizer.
    """
    def delete(self, request):
        user = request.user
        if request.user.is_anonymous:
            return Response({"msg": "User is not logged in"}, status=status.HTTP_403_FORBIDDEN)

        FollowOrganizer.objects.filter(user=request.user.id, organizer=request.data['organizer']).delete()

        return Response({"msg": "Success"}, status=status.HTTP_200_OK)
