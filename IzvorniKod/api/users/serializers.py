from rest_framework import serializers
from rest_framework_simplejwt.tokens import RefreshToken
from django.contrib.auth.hashers import check_password, make_password
from .models import (
    CustomUser,
    FollowUser,
    FollowOrganizer,
    Message
)
from quiz.models import Organizer
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from datetime import datetime

class LoginSerializer(serializers.Serializer):
    """
    User serializer with checks for login informations
    """            

    def generate_token(self, user):
        refresh = RefreshToken.for_user(user)
        return {
            "refresh": str(refresh),
            "access": str(refresh.access_token)
        }

    def validate(self, data):
        username = data.get('username')
        password = data.get('password')

        try:
            user = User.objects.get(username=username)
            if check_password(password, user.password):
                return self.generate_token(user)
            else:
                raise serializers.ValidationError({ "msg" : "Wrong username or password"})

        except ObjectDoesNotExist:
            raise serializers.ValidationError({ "msg" : "Wrong username or password"})


class RegisterSerializer(serializers.ModelSerializer):
    """
    Register data serializer. Checks data from reqeust if it fits
    table CustomUser rules in database.
    """
    username = serializers.CharField(source='user.username')
    email = serializers.CharField(source='user.email')
    password = serializers.CharField(source='user.password')

    class Meta:
        model = CustomUser
        fields = (
            'email',
            'username',
            'password',
            'first_name',
            'last_name',
            'dob',
            'sex',
            'location'
        )

    def validate(self, data):
        # TODO here all restrictions are written
        # TODO e.g. username restriction

        password = data.get('user')["password"]
        print(any(char.isdigit() for char in password))
        if len(password) < 6 or not any(char.isdigit() for char in password) or not any(char.isalpha() for char in password):
            raise serializers.ValidationError({ "msg" : "Password invalid"})

        dob = data.get('dob')
        present = datetime.now()
        if not dob < present.date():
            raise serializers.ValidationError({ "msg" : "Invalid date"})

        data["user"]["password"] = make_password(password)
        return data
    
    def create(self, data):
        userData = data.pop('user')
        email = userData['email']
        username = userData['username']
        password = userData['password']
        first_name = data.pop('first_name')
        last_name = data.pop('last_name')
        dob = data.pop('dob')
        sex = data.pop('sex')
        location = data.pop('location')

        baseUser = User.objects.create(
            email=email,
            username=username,
            password=password,
        )
        
        user = CustomUser.objects.create(
            user=baseUser,
            first_name=first_name,
            last_name=last_name,
            dob=dob,
            sex=sex,
            location=location
        )
        return user

class UpdateUserProfileSerializer(serializers.ModelSerializer):
    """
    Register data serializer. Checks data from reqeust if it fits
    table CustomUser rules in database.
    """
    email = serializers.CharField(source='user.email')
    id = serializers.CharField(source='user.id')

    class Meta:
        model = CustomUser
        fields = (
            'id',
            'email',
            'first_name',
            'last_name',
            'location'
        )

    def validate(self, data):
        return data
    
    def create(self, data):
        id = data["user"]['id']
        email = data["user"]['email']
        first_name = data.pop('first_name')
        last_name = data.pop('last_name')
        location = data.pop('location')

        baseUser = User.objects.filter(id=id).update(
            email=email
        )
        
        user = CustomUser.objects.filter(user__id=id).update(
            first_name=first_name,
            last_name=last_name,
            location=location
        )
        return user


class UserSerializer(serializers.ModelSerializer):
    """
    Django default user profile serializer
    """
    class Meta:
        model = User
        fields = ('id', 'username', 'email')


class UserProfileSerializer(serializers.ModelSerializer):
    """
    Custom user profile serializer
    """
    user = UserSerializer()

    class Meta:
        model = CustomUser
        fields = ('__all__')
        # fields = ('user', 'first_name', 'last_name')

class FollowUserSerializer(serializers.ModelSerializer):
    """
    Follow user data serializer. Checks data from reqeust if it fits
    table CustomUser rules in database.
    """
    # user = OrganizerUserSerializer()

    class Meta:
        model = FollowUser
        fields = (
            'follows',
            'user',
        )

    def validate(self, data):
        # TODO here all restrictions are written
        # TODO e.g. username restriction
        return data
    
    def create(self, data):
        user = data.pop('user')
        follows = data.pop('follows')
        
        followUser = FollowUser.objects.create(
            user=user,
            follows=follows
        )

        return followUser

class FollowOrganizerSerializer(serializers.ModelSerializer):
    """
    Follow organizer data serializer. Checks data from reqeust if it fits
    table CustomUser rules in database.
    """

    class Meta:
        model = FollowOrganizer
        fields = (
            'organizer',
            'user',
        )

    def validate(self, data):
        # TODO here all restrictions are written
        # TODO e.g. username restriction
        return data
    
    def create(self, data):
        user = data.pop('user')
        organizer = data.pop('organizer')
        
        followOrganizer = FollowOrganizer.objects.create(
            user=user,
            organizer=organizer
        )

        return followOrganizer

class FollowersOrganizerSerializer(serializers.ModelSerializer):
    """
    Organizer object used to get followers
    """
    
    class Meta:
        model = Organizer
        fields = ('__all__')

class UserFollowersSerializer(serializers.ModelSerializer):
    """
    User followers serializer to return all followers of specific user
    """
    user = UserSerializer()

    class Meta:
        model = FollowUser
        fields = ('user', 'follow_date')

class UserFollowsSerializer(serializers.ModelSerializer):
    """
    User follows serializer to return all user that specific user follows
    """
    follows = UserSerializer()

    class Meta:
        model = FollowUser
        fields = ('follows', 'follow_date')

class OrganizerFollowersSerializer(serializers.ModelSerializer):
    """
    Organizer followers serializer to return all followers of specific organizer
    """
    user = UserSerializer()

    class Meta:
        model = FollowOrganizer
        fields = ('user', 'follow_date')

class UserFollowsOrganizerSerializer(serializers.ModelSerializer):
    """
    User follows serializer to return all organizers that specific user follows
    """
    organizer = FollowersOrganizerSerializer()

    class Meta:
        model = FollowOrganizer
        fields = ('organizer', 'follow_date')

class MessageParticipantsSerializer(serializers.ModelSerializer):
    """
    User follows serializer to return all organizers that specific user follows
    """
    sender = UserSerializer()
    reciver = UserSerializer()

    class Meta:
        model = Message
        fields = ('sender', 'reciver', 'msg_date')

class MessageConversationSerializer(serializers.ModelSerializer):
    """
    User follows serializer to return all messages in conversation
    """
    sender = UserSerializer()
    reciver = UserSerializer()

    class Meta:
        model = Message
        fields = ('sender', 'reciver', 'msg_date', 'message')

class SendMessageSerializer(serializers.ModelSerializer):
    """
    User follows serializer to send message
    """

    class Meta:
        model = Message
        fields = ('sender', 'reciver', 'message')

    def validate(self, data):
        return data
    
    def create(self, data):
        sender = data.pop('sender')
        reciver = data.pop('reciver')
        message = data.pop('message')
        
        msg = Message.objects.create(
            sender=sender,
            reciver=reciver,
            message=message
        )

        return msg

class UpdateUserRoleSerializer(serializers.ModelSerializer):
    """
    Register data serializer. Checks data from reqeust if it fits
    table CustomUser rules in database.
    """

    class Meta:
        model = CustomUser
        fields = (
            'user',
            'role'
        )

    def validate(self, data):
        return data
    
    def create(self, data):
        user = data.get("user")
        role = data.get("role")
        
        user = CustomUser.objects.filter(user=user).update(
            role=role
        )
        return user
