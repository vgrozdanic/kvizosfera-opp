from django.urls import path, include
from users import views
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

urlpatterns = [
    path('login/', views.UserLogin.as_view(), name='login_view'),
    path('register/', views.UserRegister.as_view(), name='register_view'),
    path('current/', views.CurrentUser.as_view(), name='current_user_view'),
    path('profile/<int:user_id>/', views.GetUserProfile.as_view(), name='get_user_view'),
    path('follow/', views.FollowUserView.as_view(), name='follow_user_view'),
    path('follow/organizer/', views.FollowOrganizerView.as_view(), name='follow_organizer_view'),
    path('follows/organizer/<int:user_id>/', views.GetUserFollowsOrganizer.as_view(), name='follows_organizer_view'),
    path('followers/organizer/<int:organizer_id>/', views.GetOrganizerFollowers.as_view(), name='followers_organizer_view'),
    path('follows/user/<int:user_id>/', views.GetUserFollows.as_view(), name='follows_user_view'),
    path('followers/user/<int:user_id>/', views.GetUserFollowers.as_view(), name='followers_user_view'),
    path('msg/participants/', views.GetMsgParticipants.as_view(), name='msg_participants_view'),
    path('msg/conversation/<int:user_id>/', views.GetMsgConversation.as_view(), name='msg_conversation_view'),
    path('msg/send/', views.SendMessage.as_view(), name='send_msg_view'),
    path('role/', views.ChangeRoleView.as_view(), name='change_role_view'),
    path('list/all/', views.ListAllUsers.as_view(), name='list_all_users_view'),
    path('unfollow/', views.UnfollowUserView.as_view(), name='follow_user_view'),
    path('unfollow/organizer/', views.UnfollowOrganizerView.as_view(), name='follow_organizer_view')
]
