from django.db import models
from django.contrib.auth.hashers import check_password
from django.contrib.auth.models import User
from quiz.models import Organizer


# Create your models here.

class CustomUser(models.Model):
    """
    Model which represents user.
    """    
    REQUIRED_FIELDS = ('user',)

    user = models.OneToOneField(User, related_name='user', 
                               on_delete=models.CASCADE, default=None, primary_key=True)
    
    first_name = models.CharField(max_length=30, null=False)
    last_name = models.CharField(max_length=30, null=False)
    dob = models.DateField(null=False)
    creation_date = models.DateTimeField(auto_now_add=True)
    sex = models.CharField(max_length=2, null=True)
    picture = models.CharField(max_length=150)
    location = models.CharField(max_length=150)

    ROLES = [
        (1, "Obican korisik"),
        (2, "Moderator"),
        (3, "Administrator"),
    ]
    role = models.IntegerField(choices=ROLES,
                                    default=1,
                                    null=False)

    def __str__(self):
        return str(self.user.id) + " -> " + self.user.username


class FollowUser(models.Model):
    """
    Model which represents followers of user.
    """
    user = models.ForeignKey(User, related_name='follow_user_user', 
                               on_delete=models.CASCADE)
    follows = models.ForeignKey(User, related_name='follow_user_follows', 
                               on_delete=models.CASCADE)
    follow_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = [['user', 'follows']]

class FollowOrganizer(models.Model):
    """
    Model which represents followers of organizer.
    """
    user = models.ForeignKey(User, related_name='follow_organizer_user', 
                               on_delete=models.CASCADE)
    organizer = models.ForeignKey(Organizer, related_name='follow_organizer_organizer', 
                               on_delete=models.CASCADE)
    follow_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = [['user', 'organizer']]

class Message(models.Model):
    """
    Model which represents message
    """

    sender = models.ForeignKey(User, related_name='message_sender', 
                               on_delete=models.CASCADE)
    reciver = models.ForeignKey(User, related_name='message_reciver', 
                               on_delete=models.CASCADE)
    msg_date = models.DateTimeField(auto_now_add=True)                               
    message =  models.TextField(null=False)