from django.contrib import admin
from .models import (
    CustomUser,
    FollowUser,
    FollowOrganizer,
    Message
)

admin.site.register(CustomUser)
admin.site.register(FollowUser)
admin.site.register(FollowOrganizer)
admin.site.register(Message)
