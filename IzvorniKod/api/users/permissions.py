from rest_framework import permissions

class AllowOnlyPost(permissions.BasePermission):        
    """
    Custom permission class for allowing anyone to use login and register
    API endpoints. Only 'POST' method is allowed.
    TODO security testing for other POST request
    """

    def has_permission(self, request, view):
         # allow all POST requests
        if request.method == 'POST':
            return True

        if request.method == 'GET':
            # TODO (vgrozdanic) find better solution
            return True
            
        return False