from rest_framework import serializers
from .models import (
    Organizer,
    QuizEvent,
    QuizLeague,
    QuizEventEntry,
    OrganizerMember,
    GradeOrganizer,
    GradeQuiz,
    QuizComment,
    OrganizerComment,
    QuizQuestion,
    TrialQuizQuestion,
    QuizResult
)
from django.contrib.auth.models import User
from datetime import datetime

class OrganizerUserSerializer(serializers.ModelSerializer):
    """
    Django default user profile serializer
    """
    class Meta:
        model = User
        fields = ('id')

class OrganizerSerializer(serializers.ModelSerializer):
    """
    Organizer data serializer. Checks data from reqeust if it fits
    table CustomUser rules in database.
    """
    # user = OrganizerUserSerializer()

    class Meta:
        model = Organizer
        fields = (
            'user',
            'name',
            'location'
        )

    def validate(self, data):
        return data

    def create(self, data):
        user = data.pop('user')
        name = data.pop('name')
        location = data.pop('location')


        org = Organizer.objects.create(
            user=user,
            name=name,
            location=location
        )

        return org

class UpdateOrganizerSerializer(serializers.ModelSerializer):
    """
    Organizer data serializer. Checks data from reqeust if it fits
    table CustomUser rules in database and updates current info.
    """
    # user = OrganizerUserSerializer()

    class Meta:
        model = Organizer
        fields = (
            'id',
            'name',
            'location'
        )

    def validate(self, data):
        return data

    def update(self, instance, data):
        name = data.pop('name')
        location = data.pop('location')

        instance.name = name
        instance.location = location
        instance.save()
        return instance

class GetOrganizerSerializer(serializers.ModelSerializer):
    """
    Organizer data serializer. Beutifies data for return of
    organizer requested by id.
    """

    class Meta:
        model = Organizer
        fields = (
            'user',
            'name',
            'location',
            'creation_date',
            'picture'
        )

class ListUserOrganizersSerializer(serializers.ModelSerializer):
    """
    Organizer data serializer. Lists all organizers for specific user.
    """

    class Meta:
        model = Organizer
        fields = (
            'user',
            'id',
            'name',
            'location',
            'creation_date',
            'picture'
        )



class GetQuizEventSerializer(serializers.ModelSerializer):
    """
    Quiz event data serializer. Used for returning info about quiz
    """
    # user = OrganizerUserSerializer()

    class Meta:
        model = QuizEvent
        fields = (
            'league',
            'user',
            'organizer',
            'name',
            'event_date',
            'location',
            'capacity',
            'concept',
            'fee',
            'rules',
            'prize',
            'min_num_of_players',
            'max_num_of_players',
            'category'
        )



class CreateQuizEventEntrySerializer(serializers.ModelSerializer):
    """
    Quiz event entry data serializer. Checks data from reqeust if it fits
    table QuizEventEntry rules in database.
    """

    class Meta:
        model = QuizEventEntry
        fields = (
            'user',
            'name',
            'num_of_team_members',
            'quiz_event'
        )

    def validate(self, data):
        return data

    def create(self, data):
        user = data.pop('user')
        name = data.pop('name')
        num_of_team_members = data.pop('num_of_team_members')
        quiz_event = data.pop('quiz_event')

        entry = QuizEventEntry.objects.create(
            user=user,
            name=name,
            num_of_team_members=num_of_team_members,
            quiz_event=quiz_event
        )

        return entry

class CreateQuizEventSerializer(serializers.ModelSerializer):
    """
    Quiz event data serializer. Checks data from reqeust if it fits
    table QuizEvent rules in database.
    """
    # user = OrganizerUserSerializer()

    class Meta:
        model = QuizEvent
        fields = (
            'league',
            'user',
            'organizer',
            'name',
            'event_date',
            'location',
            'capacity',
            'concept',
            'fee',
            'rules',
            'prize',
            'min_num_of_players',
            'max_num_of_players',
            'category'
        )

    def validate(self, data):
        min_num_of_players = data.get('min_num_of_players')
        max_num_of_players = data.get('max_num_of_players')
        if min_num_of_players > max_num_of_players:
            raise serializers.ValidationError({ "msg" : "Indalid number of players"})
        
        if data.get("fee") < 0:
            raise serializers.ValidationError({ "msg" : "Indalid fee"})

        if data.get("capacity") < 1:
            raise serializers.ValidationError({ "msg" : "Indalid capacity"})

        present = datetime.now()
        if not data.get('event_date').date() > present.date():
            raise serializers.ValidationError({ "msg" : "Invalid event date"})
        
        return data

    def create(self, data):
        league = data.get('league')
        organizer = data.pop('organizer')
        user = data.pop('user')
        name = data.pop('name')
        event_date = data.pop('event_date')
        capacity = data.pop('capacity')
        rules = data.pop('rules')
        concept = data.pop('concept')
        min_num_of_players = data.pop('min_num_of_players')
        max_num_of_players = data.pop('max_num_of_players')
        fee = data.pop('fee')
        prize = data.pop('prize')
        location = data.pop('location')
        category = data.pop('category')
        print(category)

        event = QuizEvent.objects.create(
            league=league,
            user=user,
            organizer=organizer,
            name=name,
            event_date=event_date,
            rules=rules,
            concept=concept,
            capacity=capacity,
            min_num_of_players=min_num_of_players,
            max_num_of_players=max_num_of_players,
            fee=fee,
            prize=prize,
            location=location,
            category=category
        )

        return event

class QuizLeagueSerializer(serializers.ModelSerializer):
    """
    Quiz league data serializer. Checks data from reqeust if it fits
    table QuizLeague rules in database.
    """

    class Meta:
        model = QuizLeague
        fields = (
            'organizer',
            'name',
            'rules',
            'concept',
            'min_num_of_players',
            'max_num_of_players',
            'fee',
            'location'
        )

    def validate(self, data):
        return data

    def create(self, data):
        organizer = data.pop('organizer')
        name = data.pop('name')
        rules = data.pop('rules')
        concept = data.pop('concept')
        min_num_of_players = data.pop('min_num_of_players')
        max_num_of_players = data.pop('max_num_of_players')
        fee = data.pop('fee')
        location = data.pop('location')


        league = QuizLeague.objects.create(
            organizer=organizer,
            name=name,
            rules=rules,
            concept=concept,
            min_num_of_players=min_num_of_players,
            max_num_of_players=max_num_of_players,
            fee=fee,
            location=location
        )

        return league

class ListQuizLeagueSerializer(serializers.ModelSerializer):
    """
    Quiz league data serializer. List info about quiz leagues.
    """

    class Meta:
        model = QuizLeague
        fields = (
            'id',
            'name',
            'rules',
            'concept',
            'min_num_of_players',
            'max_num_of_players',
            'fee',
            'location'
        )


class AddOrganizerMemberSerializer(serializers.ModelSerializer):
    """
    Organizer member serializer for adding new member
    """

    class Meta:
        model = OrganizerMember
        fields = (
            'organizer',
            'user'
        )

    def validate(self, data):
        return data

    def create(self, data):
        organizer = data.pop('organizer')
        user = data.pop('user')


        league = OrganizerMember.objects.create(
            organizer=organizer,
            user=user
        )

        return league

class QuizLeagueForQEListSerializer(serializers.ModelSerializer):
    """
    Serialzer user in GetListOfQuizEvnetsSerializer for getting
    league name
    """
    class Meta:
        model = QuizLeague
        fields = (
            'id',
            'name',
        )

class GetListOfQuizEvnetsSerializer(serializers.ModelSerializer):
    """
    Quiz league data serializer. List info about quiz leagues.
    """

    league = QuizLeagueForQEListSerializer()
    class Meta:
        model = QuizEvent
        fields = (
            '__all__'
        )

class GetListOfQuizEntriesSerializer(serializers.ModelSerializer):
    """
    Quiz league data serializer. List info about quiz leagues.
    """
    quiz_event = GetListOfQuizEvnetsSerializer()
    class Meta:
        model = QuizEventEntry
        fields = (
            '__all__'
        )

class GradeOrganizerSerializer(serializers.ModelSerializer):
    """
    Grade organizer serializer for grading organizer
    """

    class Meta:
        model = GradeOrganizer
        fields = (
            'organizer',
            'user',
            'grade'
        )

    def validate(self, data):
        return data

    def create(self, data):
        organizer = data.pop('organizer')
        user = data.pop('user')
        grade = data.pop('grade')

        gradeOrganizer, created = GradeOrganizer.objects.update_or_create(
            organizer=organizer,
            user=user,
            defaults={
                "grade":grade
            }
        )

        return gradeOrganizer

class GradeQuizSerializer(serializers.ModelSerializer):
    """
    Grade quiz serializer for grading organizer
    """

    class Meta:
        model = GradeQuiz
        fields = (
            'user',
            'quiz',
            'grade'
        )

    def validate(self, data):
        return data

    def create(self, data):
        quiz = data.pop('quiz')
        user = data.pop('user')
        grade = data.pop('grade')

        gradeQuiz, created = GradeQuiz.objects.update_or_create(
            quiz=quiz,
            user=user,
            defaults={
                "grade":grade
            }
        )

        return gradeQuiz

class QuizCommentSerializer(serializers.ModelSerializer):
    """
    Quiz comment serializer
    """

    class Meta:
        model = QuizComment
        fields = ('quiz', 'user', 'comment')

    def validate(self, data):
        return data

    def create(self, data):
        quiz = data.pop('quiz')
        user = data.pop('user')
        comment = data.pop('comment')



        qcomment = QuizComment.objects.create(
            quiz=quiz,
            user=user,
            comment=comment
        )

        return qcomment

class UserForGetQuizCommentSerializer(serializers.ModelSerializer):
    """
    User Serializer to get info for user about quiz comments
    """
    class Meta:
        model = User
        fields = ('username', 'id')

class GetQuizCommentSerializer(serializers.ModelSerializer):
    """
    Serializer to get info about organizer comments
    """
    user = UserForGetQuizCommentSerializer()
    class Meta:
        model = QuizComment
        fields = ('__all__')

class OrganizerCommentSerializer(serializers.ModelSerializer):
    """
    Organizer comment serializer
    """

    class Meta:
        model = OrganizerComment
        fields = ('organizer', 'user', 'comment')

    def validate(self, data):
        return data

    def create(self, data):
        organizer = data.pop('organizer')
        user = data.pop('user')
        comment = data.pop('comment')



        ocomment = OrganizerComment.objects.create(
            organizer=organizer,
            user=user,
            comment=comment
        )

        return ocomment

class GetOrganizerCommentSerializer(serializers.ModelSerializer):
    """
    Serializer to get info about organizer comments
    """
    user = UserForGetQuizCommentSerializer()
    class Meta:
        model = OrganizerComment
        fields = ('__all__')

class QuizQuestionSerializer(serializers.ModelSerializer):
    """
    Serializer for quiz question
    """
    class Meta:
        model = QuizQuestion
        fields = (
            'quiz',
            'question_number',
            'question',
            'answer'
        )

    def validate(self, data):
        return data

    def create(self, data):
        quiz = data.pop('quiz')
        question_number = data.pop('question_number')
        question = data.pop('question')
        answer = data.pop('answer')

        question = QuizQuestion.objects.create(
            quiz=quiz,
            question_number=question_number,
            question=question,
            answer=answer
        )

        return question

class TrialQuizQuestionSerializer(serializers.ModelSerializer):
    """
    Serializer for trail quiz question
    """
    class Meta:
        model = TrialQuizQuestion
        fields = (
            'organizer',
            'question_number',
            'question',
            'answer'
        )

    def validate(self, data):
        return data

    def create(self, data):
        organizer = data.pop('organizer')
        question_number = data.pop('question_number')
        question = data.pop('question')
        answer = data.pop('answer')

        question = TrialQuizQuestion.objects.create(
            organizer=organizer,
            question_number=question_number,
            question=question,
            answer=answer
        )

        return question

class GetListOfOrganizerMembersSerializer(serializers.ModelSerializer):
    """
    Quiz organizer members serializer.
    """
    class Meta:
        model = OrganizerMember
        fields = (
            '__all__'
        )
class QuizResultSerializer(serializers.ModelSerializer):
    """
    Serializer for quiz result
    """
    class Meta:
        model = QuizResult
        fields = (
            'quiz_event',
            'position',
            'name',
            'points'
        )

    def validate(self, data):
        return data

    def create(self, data):
        quiz_event = data.pop('quiz_event')
        position = data.pop('position')
        name = data.pop('name')
        points = data.pop('points')

        result = QuizResult.objects.create(
            quiz_event=quiz_event,
            position=position,
            name=name,
            points=points
        )

        return result

class GetLeagueSerializer(serializers.ModelSerializer):
    """
    Serializer for getting information about league
    """
    class Meta:
        model = QuizLeague
        fields = ('__all__')