# Generated by Django 2.2.6 on 2020-01-02 17:14

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('quiz', '0013_quizcomment'),
    ]

    operations = [
        migrations.CreateModel(
            name='OrganizerComment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('comment', models.TextField()),
                ('comment_date', models.DateTimeField(auto_now_add=True)),
                ('organizer', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, related_name='organizer_comment_quiz', to='quiz.Organizer')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='organizer_comment_user', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
