from django.shortcuts import (
    render,
    get_object_or_404
)
from users.models import CustomUser
from django.db.models import Avg
from rest_framework.views import APIView
from .serializers import (
    OrganizerSerializer,
    CreateQuizEventSerializer,
    GetOrganizerSerializer,
    ListUserOrganizersSerializer,
    QuizLeagueSerializer,
    ListQuizLeagueSerializer,
    GetQuizEventSerializer,
    CreateQuizEventEntrySerializer,
    AddOrganizerMemberSerializer,
    GetListOfQuizEvnetsSerializer,
    GetListOfQuizEntriesSerializer,
    GradeOrganizerSerializer,
    GradeQuizSerializer,
    QuizCommentSerializer,
    GetQuizCommentSerializer,
    OrganizerCommentSerializer,
    GetOrganizerCommentSerializer,
    UpdateOrganizerSerializer,
    QuizQuestionSerializer,
    TrialQuizQuestionSerializer,
    GetListOfOrganizerMembersSerializer,
    QuizResultSerializer,
    GetLeagueSerializer
)
from rest_framework.response import Response
from rest_framework import status
from .models import (
    Organizer,
    QuizLeague,
    QuizEvent,
    QuizEventEntry,
    GradeOrganizer,
    GradeQuiz,
    QuizComment,
    OrganizerComment,
    QuizQuestion,
    TrialQuizQuestion,
    OrganizerMember,
    QuizResult
)

from users.models import FollowOrganizer
from django.contrib.auth.models import User
import datetime

class QuizOrganizer(APIView):
    """
    Checks if data for organizer is valid.
    """

    def post(self, request):
        user = request.user
        data = request.data
        data["user"] = user.id
        serializer = OrganizerSerializer(data=data, context={'request': request})

        if serializer.is_valid():
            serializer.save()
            return Response({"msg": "Success"}, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, organizer_id):
        user = request.user
        if request.user.is_anonymous:
            return Response({"msg": "User is not logged in"}, status=status.HTTP_403_FORBIDDEN)

        organizer = Organizer.objects.get(id=organizer_id)
        serializer = GetOrganizerSerializer(organizer)
        data = serializer.data

        data["following_organizer"] = False
        try:
            if len(FollowOrganizer.objects.filter(user=user.id, organizer=organizer_id)) == 0:
                raise Exception()
            data["following_organizer"] = True
        except:
            pass
            

        data["is_organizer"] = False
        if data["user"] == user.id:
            data["is_organizer"] = True

        return Response(data)

    def put(self, request):
        """
        Method for updating info about organizer
        """
        user = request.user
        data = request.data
        data["user"] = user.id
        org = get_object_or_404(Organizer, id=request.data["id"], user=user.id)

        #TODO (vgrozdanic) Add checking if user is allowed to edit organizer

        serializer = UpdateOrganizerSerializer(org, data=data, context={'request': request})

        if serializer.is_valid():
            serializer.save()
            return Response({"msg": "Success"}, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request):
        user = request.user
        if request.user.is_anonymous:
            return Response({"msg": "User is not logged in"}, status=status.HTTP_403_FORBIDDEN)

        id = request.data["id"]
        if id is None:
            return Response({"msg": "ID is misssing"}, status=status.HTTP_400_BAD_REQUEST)

        try:
            Organizer.objects.get(user=user.id, id=id)
        except:
            return Response({"msg": "User is not member of organizer"}, status=status.HTTP_403_FORBIDDEN)

        Organizer.objects.filter(id=id).delete()

        return Response({"msg": "Success"}, status=status.HTTP_200_OK)

class CreateQuizEvent(APIView):
    """
    API view for creating quiz event.
    """
    def post(self, request):
        user = request.user
        data = request.data
        data["user"] = user.id
        serializer = CreateQuizEventSerializer(data=data, context={'request': request})

        if serializer.is_valid():
            serializer.save()
            return Response({"msg": "Success"}, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class CreateQuizEventEntry(APIView):
    """
    API view for creating quiz event entry.
    """
    def post(self, request):
        user = request.user
        data = request.data
        data["user"] = user.id
        serializer = CreateQuizEventEntrySerializer(data=data, context={'request': request})

        if serializer.is_valid():
            serializer.save()
            return Response({"msg": "Success"}, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request):
        user = request.user
        if request.user.is_anonymous:
            return Response({"msg": "User is not logged in"}, status=status.HTTP_403_FORBIDDEN)

        quiz_event = request.data["quiz_event"]
        if quiz_event is None:
            return Response({"msg": "Quiz event ID is misssing"}, status=status.HTTP_400_BAD_REQUEST)

        try:
            if len(QuizEventEntry.objects.filter(user=user.id, quiz_event=quiz_event)) == 0:
                raise Exception()
        except:
            return Response({"msg": "There is no quiz event entry for this user and quiz"}, status=status.HTTP_403_FORBIDDEN)

        QuizEventEntry.objects.filter(quiz_event=quiz_event, user=user.id).delete()

        return Response({"msg": "Success"}, status=status.HTTP_200_OK)

class GetQuizEvent(APIView):
    """
    API view for getting info about quiz event.
    """
    def get(self, request, quiz_id):
        user = request.user
        quiz = QuizEvent.objects.get(id=quiz_id)
        serializer = GetQuizEventSerializer(quiz)
        data = serializer.data

        data["has_quiz_entry"] = False
        try:
            if len(QuizEventEntry.objects.filter(quiz_event=quiz_id, user=user.id)) > 0:
                data["has_quiz_entry"] = True
        except:
            # There is no quiz event entry
            pass

        return Response(data)



class GetOrganizerLeagues(APIView):
    """
    Lists all leagues for specific organizer.
    """
    def get(self, request, organizer_id):
        organizers = QuizLeague.objects.filter(organizer=organizer_id)
        serializer = ListQuizLeagueSerializer(organizers, many=True)

        return Response(serializer.data)


class GetUserOrganizers(APIView):
    """
    Lists all organizers for specific user.
    """
    def get(self, request):
        user = request.user
        organizers = Organizer.objects.filter(user=user)
        serializer = ListUserOrganizersSerializer(organizers, many=True)

        return Response(serializer.data)

class QuizLeagueView(APIView):
    """
    API view for creating quiz league.
    """
    def post(self, request):
        user = request.user
        data = request.data
        data["user"] = user.id
        serializer = QuizLeagueSerializer(data=data, context={'request': request})

        if serializer.is_valid():
            serializer.save()
            return Response({"msg": "Success"}, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AddOrganizerMember(APIView):
    """
    API view for adding member to organizer.
    """
    def post(self, request):
        user = request.data["user"]["username"]
        data = request.data

        try:
            userObject = User.objects.get(username=user)
            data["user"] = userObject.id
        except:
            return Response({"msg": "User does not exists"}, status=status.HTTP_400_BAD_REQUEST)

        serializer = AddOrganizerMemberSerializer(data=data, context={'request': request})

        if serializer.is_valid():
            serializer.save()
            return Response({"msg": "Success"}, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class GetListOfUpcomingQuizEvnets(APIView):
    """
    Lists all upcoming quizes.
    """
    def get(self, request):
        today = datetime.datetime.now()
        quizes = QuizEvent.objects.filter(event_date__gte=today)
        serializer = GetListOfQuizEvnetsSerializer(quizes, many=True)

        return Response(serializer.data)

class GetListOfBygoneQuizEvnets(APIView):
    """
    Lists all bygone quizes.
    """
    def get(self, request):
        today = datetime.datetime.now()
        quizes = QuizEvent.objects.filter(event_date__lt=today)
        serializer = GetListOfQuizEvnetsSerializer(quizes, many=True)

        return Response(serializer.data)

class GetListOfBygoneCurrentUserQuizEvnets(APIView):
    """
    Lists all current user bygone quizes.
    """
    def get(self, request):
        user = request.user
        today = datetime.datetime.now()
        quizes = QuizEventEntry.objects.filter(quiz_event__event_date__lt=today, user=user)
        serializer = GetListOfQuizEntriesSerializer(quizes, many=True)

        return Response(serializer.data)

class GetListOfUpcomingCurrentUserQuizEvnets(APIView):
    """
    Lists all current user upcoming quizes.
    """
    def get(self, request):
        user = request.user
        today = datetime.datetime.now()
        quizes = QuizEventEntry.objects.filter(quiz_event__event_date__gte=today, user=user)
        serializer = GetListOfQuizEntriesSerializer(quizes, many=True)

        return Response(serializer.data)


class GetListOfBygoneUserQuizEvnets(APIView):
    """
    Lists all specific user bygone quizes.
    """
    def get(self, request, user_id):
        today = datetime.datetime.now()
        quizes = QuizEventEntry.objects.filter(quiz_event__event_date__lt=today, user=user_id)
        serializer = GetListOfQuizEntriesSerializer(quizes, many=True)

        return Response(serializer.data)

class GetListOfUpcomingUserQuizEvnets(APIView):
    """
    Lists all specific user upcoming quizes.
    """
    def get(self, request, user_id):
        today = datetime.datetime.now()
        quizes = QuizEventEntry.objects.filter(quiz_event__event_date__gte=today, user=user_id)
        serializer = GetListOfQuizEntriesSerializer(quizes, many=True)

        return Response(serializer.data)


class GradeOrganizerView(APIView):
    """
    API view for grading organizer.
    """
    def post(self, request):
        user = request.user
        data = request.data

        try:
            userObject = User.objects.get(username=user)
            data["user"] = userObject.id
        except:
            return Response({"msg": "User does not exists"}, status=status.HTTP_400_BAD_REQUEST)

        serializer = GradeOrganizerSerializer(data=data, context={'request': request})

        if serializer.is_valid():
            serializer.save()
            return Response({"msg": "Success"}, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class GetAverageGradeOrganizer(APIView):
    """
    Returns average grade for organizer
    """
    def get(self, request, organizer_id):
        grade = GradeOrganizer.objects.filter(organizer=organizer_id).aggregate(Avg('grade'))
        data = {"avg_grade" : grade["grade__avg"]}

        return Response(data, status=status.HTTP_200_OK)


class GradeQuizView(APIView):
    """
    API view for grading quiz.
    """
    def post(self, request):
        user = request.user
        data = request.data

        try:
            userObject = User.objects.get(username=user)
            data["user"] = userObject.id
        except:
            return Response({"msg": "User does not exists"}, status=status.HTTP_400_BAD_REQUEST)

        serializer = GradeQuizSerializer(data=data, context={'request': request})

        if serializer.is_valid():
            serializer.save()
            return Response({"msg": "Success"}, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class GetAverageGradeQuiz(APIView):
    """
    Returns average grade for quiz
    """
    def get(self, request, quiz_id):
        grade = GradeQuiz.objects.filter(quiz=quiz_id).aggregate(Avg('grade'))
        data = {"avg_grade" : grade["grade__avg"]}

        return Response(data, status=status.HTTP_200_OK)

class QuizCommentView(APIView):
    """
    API view for commenting on quiz.
    """
    def post(self, request):
        user = request.user
        data = request.data

        try:
            userObject = User.objects.get(username=user)
            data["user"] = userObject.id
        except:
            return Response({"msg": "User does not exists"}, status=status.HTTP_400_BAD_REQUEST)

        serializer = QuizCommentSerializer(data=data, context={'request': request})

        if serializer.is_valid():
            serializer.save()
            return Response({"msg": "Success"}, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request):
        user = request.user
        if request.user.is_anonymous:
            return Response({"msg": "User is not logged in"}, status=status.HTTP_403_FORBIDDEN)

        id = request.data["id"]
        if id is None:
            return Response({"msg": "ID is misssing"}, status=status.HTTP_400_BAD_REQUEST)

        try:
            custom_user = CustomUser.objects.get(user=user.id)
            if len(QuizComment.objects.filter(user=user.id, id=id)) == 0 and custom_user.role < 2:
                raise Exception()
        except:
            return Response({"msg": "User has not right to delete!"}, status=status.HTTP_403_FORBIDDEN)

        QuizComment.objects.filter(id=id).delete()

        return Response({"msg": "Success"}, status=status.HTTP_200_OK)

class GetQuizComments(APIView):
    """
    Returns all comments for quiz
    """
    def get(self, request, quiz_id):
        comments = QuizComment.objects.filter(quiz=quiz_id)
        serializer = GetQuizCommentSerializer(comments, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)

class OrganizerCommentView(APIView):
    """
    API view for commenting on organizer.
    """
    def post(self, request):
        user = request.user
        data = request.data

        try:
            userObject = User.objects.get(username=user)
            data["user"] = userObject.id
        except:
            return Response({"msg": "User does not exists"}, status=status.HTTP_400_BAD_REQUEST)

        serializer = OrganizerCommentSerializer(data=data, context={'request': request})

        if serializer.is_valid():
            serializer.save()
            return Response({"msg": "Success"}, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request):
        user = request.user
        if request.user.is_anonymous:
            return Response({"msg": "User is not logged in"}, status=status.HTTP_403_FORBIDDEN)

        id = request.data["id"]
        if id is None:
            return Response({"msg": "ID is misssing"}, status=status.HTTP_400_BAD_REQUEST)

        try:
            custom_user = CustomUser.objects.get(user=user.id)
            if len(OrganizerComment.objects.filter(user=user.id, id=id)) == 0 and custom_user.role != 2:
                raise Exception()
        except:
            return Response({"msg": "User has not right to delete!"}, status=status.HTTP_403_FORBIDDEN)

        OrganizerComment.objects.filter(id=id).delete()

        return Response({"msg": "Success"}, status=status.HTTP_200_OK)

class GetOrganizerComments(APIView):
    """
    Returns all comments for organizer
    """
    def get(self, request, organizer_id):
        comments = OrganizerComment.objects.filter(organizer=organizer_id)
        serializer = GetOrganizerCommentSerializer(comments, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)

class CreateQuizQuestionView(APIView):
    """
    API view for creating quiz question.
    """
    def post(self, request):
        serializer = QuizQuestionSerializer(data=request.data, context={'request': request})

        if serializer.is_valid():
            serializer.save()
            return Response({"msg": "Success"}, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class CreateTrialQuizQuestionView(APIView):
    """
    API view for creating trial quiz question.
    """
    def post(self, request):
        serializer = TrialQuizQuestionSerializer(data=request.data, context={'request': request})

        if serializer.is_valid():
            serializer.save()
            return Response({"msg": "Success"}, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class GetQuizQuestions(APIView):
    """
    Returns all quiz questions for quiz
    """
    def get(self, request, quiz_event_id):
        questions = QuizQuestion.objects.filter(quiz=quiz_event_id)
        serializer = QuizQuestionSerializer(questions, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)

class GetTrialQuizQuestions(APIView):
    """
    Returns all trial quiz questions for quiz
    """
    def get(self, request, organizer_id):
        questions = TrialQuizQuestion.objects.filter(organizer=organizer_id)
        serializer = TrialQuizQuestionSerializer(questions, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)

class GetOrganizerPastQuizes(APIView):
    """
    API view for getting organizer past quizes.
    """
    def get(self, request, organizer_id):
        today = datetime.datetime.now()
        quizes = QuizEvent.objects.filter(organizer=organizer_id, event_date__lt=today)
        serializer = GetListOfQuizEvnetsSerializer(quizes, many=True)
        return Response(serializer.data)
class GetOrganizerFutureQuizes(APIView):
    """
    API view for getting organizer future quizes.
    """
    def get(self, request, organizer_id):
        today = datetime.datetime.now()
        quizes = QuizEvent.objects.filter(organizer=organizer_id, event_date__gte=today)
        serializer = GetListOfQuizEvnetsSerializer(quizes, many=True)
        return Response(serializer.data)
class GetOrganizerMembers(APIView):
    """
    API view for getting organizer members.
    """
    def get(self, request, organizer_id):
        members = OrganizerMember.objects.filter(organizer=organizer_id)
        serializer = GetListOfOrganizerMembersSerializer(members, many=True)
        return Response(serializer.data)

class QuizResultView(APIView):
    """
    View for quiz results.
    """
    def post(self, request):
        serializer = QuizResultSerializer(data=request.data, context={'request': request})

        if serializer.is_valid():
            serializer.save()
            return Response({"msg": "Success"}, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class GetQuizResult(APIView):
    """
    Returns all quiz results
    """
    def get(self, request, quiz_id):
        results = QuizResult.objects.filter(quiz_event=quiz_id).order_by("position")
        serializer = QuizResultSerializer(results, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)

class GetLeagueInfo(APIView):
    """
    Returns info about quiz league
    """
    def get(self, request, league_id):
        league = QuizLeague.objects.get(id=league_id)
        serializer = GetLeagueSerializer(league)

        return Response(serializer.data, status=status.HTTP_200_OK)
