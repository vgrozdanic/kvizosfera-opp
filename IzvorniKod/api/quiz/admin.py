from django.contrib import admin
from .models import (
    Organizer, 
    QuizEvent,
    QuizLeague,
    OrganizerMember,
    QuizEventEntry,
    GradeOrganizer,
    GradeQuiz,
    QuizComment,
    OrganizerComment,
    QuizQuestion,
    TrialQuizQuestion,
    QuizResult
)

admin.site.register(Organizer)
admin.site.register(OrganizerMember)
admin.site.register(QuizEvent)
admin.site.register(QuizLeague)
admin.site.register(QuizEventEntry)
admin.site.register(GradeOrganizer)
admin.site.register(GradeQuiz)
admin.site.register(QuizComment)
admin.site.register(OrganizerComment)
admin.site.register(QuizQuestion)
admin.site.register(TrialQuizQuestion)
admin.site.register(QuizResult)
