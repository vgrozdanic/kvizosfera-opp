from django.db import models
from django.contrib.auth.models import User

class Organizer(models.Model):
    """
    Model which represents quiz organizer.
    """    

    user = models.ForeignKey(User, related_name='organizer_user', 
                               on_delete=models.CASCADE, default=None)
    
    name = models.CharField(max_length=60, null=False, unique=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    picture = models.CharField(max_length=150)
    location = models.CharField(max_length=150)

    class Meta:
        unique_together = [['user', 'name']]

    def __str__(self):
        return str(self.id) + " -> " + self.name

class OrganizerMember(models.Model):
    """
    Model which represents members of quiz organizer.
    Members are User models.
    """
    user = models.ForeignKey(User, related_name='oranizer_member_user', 
                               on_delete=models.CASCADE, default=None)
    organizer = models.ForeignKey(Organizer, related_name='organizer_member_organizer', 
                               on_delete=models.CASCADE, default=None)
    class Meta:
        unique_together = (("user", "organizer"),)

class QuizLeague(models.Model):
    """
    Model which represents quiz league. League is created by organizer.
    """
    organizer = models.ForeignKey(Organizer, related_name='quiz_league_organizer', 
                               on_delete=models.CASCADE, default=None) 

    name = models.CharField(max_length=60, null=False, unique=True)
    rules = models.CharField(max_length=4096, null=False)
    concept = models.CharField(max_length=4096)
    creation_date = models.DateTimeField(auto_now_add=True)
    min_num_of_players = models.IntegerField()
    max_num_of_players = models.IntegerField()
    fee = models.IntegerField(default=0)
    location = models.CharField(max_length=150)
    picture = models.CharField(max_length=150)

    class Meta:
        unique_together = [['organizer', 'name']]

class QuizEvent(models.Model):
    """
    Model which represents quiz event.
    """
    league = models.ForeignKey(QuizLeague, related_name='quiz_event_league', 
                               on_delete=models.CASCADE, default=None, null=True)
    user = models.ForeignKey(User, related_name='quiz_event_user', 
                               on_delete=models.CASCADE, default=None)
    organizer = models.ForeignKey(Organizer, related_name='quiz_event_organizer', 
                               on_delete=models.CASCADE, default=None)
    creation_date = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=60, null=False)
    event_date = models.DateTimeField(null=False)
    location = models.CharField(max_length=150)
    capacity = models.IntegerField()
    concept = models.CharField(max_length=4096)
    fee = models.IntegerField(default=0)
    rules = models.CharField(max_length=4096)
    prize = models.CharField(max_length=4096)
    min_num_of_players = models.IntegerField()
    max_num_of_players = models.IntegerField()

    CATEGORIES = [
        (1, "Kviz opceg znanja"),
        (2, "Sportski kviz"),
        (3, "Glazbeni kviz"),
        (4, "Filmski kviz"),
        (5, "Ostalo"),
    ]
    category = models.IntegerField(choices=CATEGORIES,
                                    default=1,
                                    null=False)
    class Meta:
        unique_together = [['organizer', 'name']]

class QuizEventEntry(models.Model):
    """
    Model which represents quiz event entry.
    """
    user = models.ForeignKey(User, related_name='quiz_event_entry_user', 
                               on_delete=models.CASCADE)
    name = models.CharField(max_length=60, null=False)
    num_of_team_members = models.IntegerField()
    application_date = models.DateTimeField(auto_now_add=True)
    quiz_event = models.ForeignKey(QuizEvent, related_name='quiz_event_entry_quiz_event', 
                               on_delete=models.CASCADE)

    class Meta:
        unique_together = [['user', 'quiz_event']]

class GradeOrganizer(models.Model):
    """
    Model which represents grade for organizer.
    """
    user = models.ForeignKey(User, related_name='grade_organizer_user', 
                               on_delete=models.CASCADE)
    organizer = models.ForeignKey(Organizer, related_name='grade_organizer_organizer', 
                               on_delete=models.CASCADE, default=None)
    grade = models.IntegerField()
    grade_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = [['user', 'organizer']]

class GradeQuiz(models.Model):
    """
    Model which represents grade for organizer.
    """
    user = models.ForeignKey(User, related_name='grade_quiz_user', 
                               on_delete=models.CASCADE)
    quiz = models.ForeignKey(QuizEvent, related_name='grade_quiz_quiz', 
                               on_delete=models.CASCADE, default=None)
    grade = models.IntegerField()
    grade_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = [['user', 'quiz']]

class QuizComment(models.Model):
    """
    Model which represents quiz comment
    """
    user = models.ForeignKey(User, related_name='quiz_comment_user', 
                               on_delete=models.CASCADE)
    quiz = models.ForeignKey(QuizEvent, related_name='quiz_comment_quiz', 
                               on_delete=models.CASCADE, default=None)
    comment = models.TextField()
    comment_date = models.DateTimeField(auto_now_add=True)

class OrganizerComment(models.Model):
    """
    Model which represents organizer comment
    """
    user = models.ForeignKey(User, related_name='organizer_comment_user', 
                               on_delete=models.CASCADE)
    organizer = models.ForeignKey(Organizer, related_name='organizer_comment_quiz', 
                               on_delete=models.CASCADE, default=None)
    comment = models.TextField()
    comment_date = models.DateTimeField(auto_now_add=True)

class QuizQuestion(models.Model):
    """
    Model which represents quiz question
    """
    quiz = models.ForeignKey(QuizEvent, related_name='quiz_question_quiz_event', 
                               on_delete=models.CASCADE)
    question_number = models.IntegerField()
    question = models.TextField()
    answer = models.TextField()

class TrialQuizQuestion(models.Model):
    """
    Model which represents trial quiz question
    """
    organizer = models.ForeignKey(Organizer, related_name='trial_quiz_question_organizer', 
                               on_delete=models.CASCADE)
    question_number = models.IntegerField()
    question = models.TextField()
    answer = models.TextField()

class QuizResult(models.Model):
    """
    Model which represents quiz event result
    """
    quiz_event = models.ForeignKey(QuizEvent, related_name='quiz_result_quiz_event', 
                               on_delete=models.CASCADE)
    position = models.IntegerField()
    name = models.TextField()
    points = models.IntegerField()